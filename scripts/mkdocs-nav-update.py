"""
This script processes the mkdocs.yaml document.
It searches for pages which appear several times in the "nav" tree,
and therefore create problems in the generated static page navigation
(showing as "None"). For these duplicates a symlink with a slightly
different filename is created, thus tricking mkdocs into thinking
they would be different pages, however while having the same content.
"""

import yaml
import os


def rename(file_name: str, seen_values: set):
    splitted = file_name.split("_")
    suffix = splitted[-1].replace(".md", "")
    name = "_".join(splitted[:-1])
    if suffix.isdecimal():
        i = int(suffix)
    else:
        name = file_name
        i = 0
    is_taken = True
    while is_taken:
        i += 1
        file_name_new = name + "_" + str(i) + ".md"
        is_taken = file_name_new in seen_values
    return file_name_new


def replace_nav_recursive(item, key, parent, seen_values: set):
    if isinstance(item, str):
        if item not in seen_values:
            seen_values.add(item)
        else:
            item_renamed = rename(item, seen_values)
            seen_values.add(item_renamed)
            parent[key] = item_renamed
            os.symlink(item, "docs/" + item_renamed)
    elif isinstance(item, dict):
        for key, value in item.items():
            replace_nav_recursive(value, key, item, seen_values)
    elif isinstance(item, list):
        for i, value in enumerate(item):
            replace_nav_recursive(value, i, item, seen_values)


file_name = "mkdocs.yml"
with open(file_name) as file:
    mkdocs_config = yaml.safe_load(file)

nav = mkdocs_config["nav"]
seen_values = set()

for i, x in enumerate(nav):
    replace_nav_recursive(x, i, nav, seen_values)

# Now write YAML with the updated nav back into the file
with open(file_name, "wt") as file:
    yaml.safe_dump(mkdocs_config, file)
