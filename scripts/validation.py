import os
import pkg_resources
import traceback

import frontmatter
from linkml.validator import Validator
from linkml.validator.plugins import JsonschemaValidationPlugin
from linkml.validator.plugins import (
    JsonschemaValidationPlugin,
    RecommendedSlotsPlugin,
)
from pydantic import ValidationError

schema_dir = pkg_resources.resource_filename("n4e_kh_schema_py", ".")


def validate_lhb_metadata(abs_path, rel_path) -> list:
    print("Will validate: " + rel_path)
    validation_results = []
    with open(abs_path) as file:
        content = file.read()
    post = frontmatter.loads(content)
    metadata = post.metadata

    # Note: From Python 3.9 onwards should use instead:
    # import importlib.resources as pkg_resources
    schema = "n4eschema-linkml.yaml"
    # the LinkML schema has a relative reference to the other schema file
    # containing the Enum definition.
    # For the parsing library to work correctly unfortunately
    # we have to change the working dir to the dir of the schema
    # NOTE: Important to handle the dir change outside of the function
    os.chdir(schema_dir)
    validator = Validator(
        schema=schema,
        validation_plugins=[
            JsonschemaValidationPlugin(closed=True),
            RecommendedSlotsPlugin(),
        ],
    )

    # to make the manual metadata curation easier, allow single values or array
    # for the following fields
    remove = []
    for k, v in metadata.items():
        if k in [
            "about",
            "name",
            "description",
            "license",
            "additionalType",
            "inLanguage",
            "isPartOf",
            "subjectArea",
            "identifier",
            "sameAs"
        ]:
            if v is None:
                remove.append(k)
            elif not isinstance(v, list):
                metadata[k] = [v]
        if k == "version":
            metadata[k] = str(v)
    for k in remove:
        metadata.pop(k)

    authors = metadata["author"]
    # LinkML validation currently cannot deal well with nested objects,
    # so make sure that the author field is validated with the Person schema
    if isinstance(authors, list):
        for i, author in enumerate(authors):
            # LinkML expects an identifier
            author["id"] = "tmp"
            # same as above
            if not isinstance(author["name"], list):
                author["name"] = [author["name"]]
            try:
                validator.validate(author, "Person").results
            except ValidationError as e:
                for x in e.errors():
                    validation_results.append(
                        {
                            "Article": rel_path,
                            "severity": x["input"]["severity"],
                            "error": x["input"]["message"],
                        }
                    )
                # replace the entry in the metadata dict because was already validated
            authors[i] = "tmpId"

    metadata["id"] = "tmpId"
    try:
        validator.validate(metadata, "LHBArticle")
    except ValidationError as e:
        for x in e.errors():
            validation_results.append(
                {
                    "Article": path,
                    "severity": x["input"]["severity"],
                    "error": x["input"]["message"],
                }
            )
    return validation_results


if __name__ == "__main__":
    paths = []
    for file_name in os.listdir("docs"):
        if file_name.endswith(".md"):
            path = os.path.join("docs", file_name)
            abs_path = os.path.abspath(path)
            paths.append((abs_path, path))
    # NOTE: because the validate_lhb_metadata changes the current
    # working dir we need to get the absolute paths of the handbook
    # files before running the function
    validation_errors = []
    for abs_path, path in paths:
        try:
            errors = validate_lhb_metadata(abs_path, path)
            validation_errors.extend(errors)
        except Exception as e:
            validation_errors.append(
                {
                    "Article": path,
                    "severity": "Runtime error",
                    "error": traceback.format_exc(),
                }
            )
    if len(validation_errors) > 0:
        for e in validation_errors:
            if e["severity"] == "Runtime error":
                print({"Article": e["Article"], "severtiy": "Runtime error:"})
                print(e["error"])
            else:
                print(e)
        err_msg = (
            "LHB Metadata validation unsuccessful. Found "
            + f"{len(validation_errors)} problems, see above."
        )
        # To ensure that the message is really logged also in the CI pipeline
        # print it double
        print(err_msg)
        raise ValueError(err_msg)
    else:
        print("Validation: no problems in article's metadata found")
