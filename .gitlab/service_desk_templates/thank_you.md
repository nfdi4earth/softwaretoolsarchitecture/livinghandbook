Thank you for contacting the NFDI4Earth Living Handbook Editors. Your mail is recorded as issue %{ISSUE_ID}. We will get back to you soon with further information or questions. 

If you want to provide any additional information or commments related to your query, please reply to this mail. 

Best regards, 
The NFDI4Earth Living Handbook Editors
