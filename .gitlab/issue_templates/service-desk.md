### Initial steps: 

1.  Please add the appropriate labels to the issue to indicate its type
2.  Assign the persons that should handle this issue (this also means yourself, if you are the one handling it)
3.  Notify the managing editor/Coordinator of M3.3 to check whether the author accepted the author agreement (they will invite them, if they did not). 
4.  If necessary, ask the issue creator for further information. If you don't fully understand the problem, others likely won't. 
