---
name: How does a FAQ for the NFDI4Earth Living Handbook looks like?
# The actual FAQ

description: 
# Can be omitted for short answers. Otherwise, please provide a short summary of the answer or which aspects are particularly highlighted in it (up to 300 characters) 

author: 
  - name: User Support Network # must not be altered or removed

language: ENG
# The language of the article. Accepted values are:  
# ENG for English
# DEU for German

collection: 
  - FAQ.md # must not be altered or removed
  - collection1.md
# If the FAQ should be included also in topical collections, provide here their filenames as they appear in https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/tree/main/docs. 
# Alternatively, the titles of the collections. The editor will replace it with the collections' file names. 

subtype: FAQ # must not be altered or removed

subject: 
  - unesco:concept3117 # must not be altered or removed
  - dfgfo:313-02
# A list of controlled terms describing the subject of the FAQ, given as their identifiers. Acceptable are any terms from: 
# DFG subject ontology: https://terminology.tib.eu/ts/ontologies/dfgfo/terms
# UNESCO thesaurus: https://vocabularies.unesco.org/browser/thesaurus/en/

keywords:
  - keyword1
  - keyword2
# An optional list of terms to describe the FAQ's topic more precisely. 

target_role: 
  - data collector
  - policy maker
# A list of the main roles of persons within research data management that is addressed by the article. 
# data collector: The person responsible for collecting, generating, re-using or contributing to research data and documents contexts and processes that relates to data handling. 
# data owner: Typically the principal researcher of the project or research group leader who is responsible for establishing who has access to the data, and reducing or mitigating the risk of any mishaps; e.g. improper storage facility, non-compliance with specific guidelines. 
# data depositor: Either the data creator or data owner and is the person who will perform the deposit of the data to a data preservation facility, like a repository or data archive. They perform activities such as uploading data after it has been generated, making it suitable for archiving, creating metadata and data documentation and submitting the data to the facility, so it can be preserved for the long-term. 
# data user: Someone who is interested in accessing and interacting with data for verification or (re)using data to evaluate, analyse, model and generally handle then to create insights and knowledge. access to research data for re-use or verification. 
# data curator: A person who ensures availability and reliability of data. They review and harmonise metadata of datasets submitted for storage and ensure that they are generally compliant with relevant policies before publication. They further monitor access to the data, support data owners in fulfilling data access requests and possess deeper technical knowledge about the backend of the repository or storage system. 
# data librarian: An information professional responsible for developing technical expertise on practical solutions of data management, archiving and dissemination and on data mining and visualization. 
# data steward: A supporter who advises researchers (or otherwise involved) persons on data management, privacy and information security aspects and can understand the meaning and applications of the data. They support: data creators on quality assurance, storage, policy compliance; data depositors on data selection, metadata and data documentation creation, preserving and publishing; data custodians on data curation, access controls, FAIR data management 
# data advocate: A person promoting the proper use of research data and technology and providing assistance or guidance in doing so. 
# data provider: A person or organization that stores and provides datasets to others.
# service provider: An organization that provides services related to the processing and management of data, such as, high-performance computing facilities or web-based maps.
# research Software Engineer: A person working with researchers to gain an understanding of the problems they face, and then develops, maintains and extends software to provide the answers. 
# policy maker: In research data management, policy makers strive to design effective, technology-neutral, forward-looking and coherent data governance policies across sectors.
# general public: All persons that do not belong to any of the other groups. 

identifier: 
# If the FAQ has already a unique identifier such as a DOI, provide it here as URL (e.g., https://doi.org/10.1001/12345). 

version: 0.1
# The version of the FAQ; will be updated by the editor. 

---

# How does a FAQ for the NFDI4Earth Living Handbook looks like?
FAQs provide consise and general responses to questions repeatedly asked to the NFDI4Earth User Support Network. They are regular articles in the Living Handbook that follow a more constrained way of how the information is presented and the article is described by its metadata. Being regular articles in the technical implementation means that they can use all features available in the Living Handbook such as figures, videos, and interactive content. See the general template's section [Media and other linked/embedded content](example.md#media-and-other-linkedembedded-content) for details on how they can be included. 

## Metadata
The metadata of the FAQ are placed at the very top and is separated from the article text by `---`. Please fill it in/replace the examples according to the explanation provided for each metadatum. Afterwards, you can delete any line starting with `#`. Please be aware that any entries followed by `# must not be altered or removed` is common to all FAQs. 

## Structure of an FAQ
The actual FAQ is the title of the article. The answer to the FAQ consists of at least one paragraph providing a general answer to it. In addition, subsequent paragraphs can elaborate on specific aspects of this answer, provide further information or provide a more nuanced approach, taking e.g., specificities of different scientific fields into account. 

## References
Although references are likely to be unnecessary for FAQs, please provide at least when referring to specific resources such as publications and resources having a DOI (e.g., on Zenodo). The respective bibliographic information should be provided at the end of the article in a way that clearly identifies to which in-text reference it belongs to. If the resource has a DOI, the DOI suffices. If not, bibliographic information should be provided in bibtex formatting. 

## Additional resources

* [Author guidelines](LHB_author-guidelines.md)
