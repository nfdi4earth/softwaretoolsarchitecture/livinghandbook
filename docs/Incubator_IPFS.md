---
name: IPFS Pinning Service for Open Climate Research Data

description: Description of the NFDI4Earth Incubator project. 

author: 
  - name: Marco Kulüke
    orcidId: https://orcid.org/0000-0003-0611-2567
  - name: Stephan Kindermann
    orcidId: https://orcid.org/0000-0001-9335-1093
  - name: Tobias Kölling

inLanguage: ENG

about:
  - external nfdi4earth
  - tools and techniques
  - fair
  - workflow


isPartOf: 
  - N4E_Incubators.md
  - Collection_AccessReuse.md

additionalType: article

subjectArea: 
  - dfgfo:313
  - unesco:concept522
  - unesco:mt2.35
  - unesco:concept5434
  - unesco:concept17128

keywords:
  - decentralised file storage
  - IPFS
  - Pinning Service
  - FAIR
  - Open Science
  - Climate

audience: 
  - data depositor
  - data librarian

version: 1.0

identifier: https://doi.org/10.5281/zenodo.7646356


license: CC-BY-4.0

---

# IPFS Pinning Service for Open Climate Research Data

<a id="med1"></a>

![Screenshot of the web interface.](img/Incubator_IPFS_media1.png "Screenshot of the web interface.")

&nbsp; 

## Abstract
We explored the use of the IPFS for scientific data with a focus on climate data. We set up an IPFS node running on a cloud instance at the German Climate Computing Center where selected scientists can pin their data and make them accessible to the public via the IPFS infrastructure. IPFS is a good choice for climate data, because the open network architecture strengthens open science efforts and enables FAIR data processing workflows. Data within the IPFS is freely accessible to scientists regardless of their location and offers fast access rates to large files. In addition, data within the IPFS is immutable, which ensures that the content of a content identifier does not change over time. Due to the recent development of the IPFS, the project outcomes are novel data science developments for the earth system science and are potentially relevant building blocks to be included in the earth system science community. 

## Outcome and Trends
We set up an IPFS node running on a virtual machine at the German Climate Computing Center where selected researchers can pin their data and make them accessible to the public via the IPFS infrastructure. The IPFS proved to be a good choice for climate data, because the open network architecture strengthens open science efforts and enables FAIR data processing workflows. Data within the IPFS is freely accessible to researchers regardless of their location and offers fast access rates to large files. In addition, data within the IPFS is immutable, which ensures that the content of a content identifier does not change over time.  

As of now, the IPFS project continues to evolve and improve. In recent years, the project has seen significant growth in terms of adoption, developer community, and funding. Recent developments include improvements to IPFS performance and usability, including the release of IPFS Desktop, a user-friendly interface for accessing IPFS files. The IPFS project also continues to see widespread adoption in a variety of industries and use cases, from blockchain and cryptocurrency to healthcare and content distribution, which makes it attractive for interdisciplinary research. Looking forward, the project outcomes are novel data science developments for the earth system science and are potentially relevant building blocks to be included in the earth system science community.

## Resources

* Live version ([Binder](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fnfdi4earth%2Fpilotsincubatorlab%2Fincubator%2Fipfs-pinning-service/HEAD))
* Code repository ([GitLab](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/ipfs-pinning-service) | [Zenodo](https://doi.org/10.5281/zenodo.7646356))
* [Science Talk](https://www.youtube-nocookie.com/embed/scByz8dYzio)
* [Final Report](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/ipfs-pinning-service/-/blob/master/project-description.pdf)
* [Tutorial](https://www.youtube-nocookie.com/embed/jDrBFgUi7Oc)
* [Jupyter Notebook Tutorial](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/ipfs-pinning-service/-/blob/master/tutorial.ipynb)


