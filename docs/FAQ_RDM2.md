---
name: What are the FAIR principles? 

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth
  - fair

isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - unesco:concept7387
  - unesco:concept7396
  - unesco:concept522

keywords:
  - FAIR principles
  - interoperability
  - findability
  - discovery
  - reusability
  - accessibility

audience: 
  - data depositor
  - data owner
  - data steward
  - general public
  - data advocate

version: 1.0

license: CC-BY-4.0

---

# What are the FAIR principles? 

The FAIR principles ([Wilkinson et al. 2016](https://doi.org/10.1038/sdata.2016.18)) are a set of guiding principles designed to enhance the discoverability, accessibility, interoperability, and reusability of digital resources, particularly in the context of scientific data and research. The acronym "FAIR" stands for:

**Findable**: The first step in (re)using data and digital resources is to find them, both for humans and machines. This involves providing metadata (descriptive information) that includes context, provenance, and other relevant details in a standardized format. Metadata should be assigned a globally unique and persistent identifier and registered or indexed in searchable resources.

**Accessible**: Data and digital resources should be accessible that means that both humans and machines receive instructions on how to obtain the data. It should be noted that FAIR does not equate to open, Data can be restricted and still be FAIR. However, if access is allowed, data should be retrievable without the need for specialised protocols and access is appropriately controlled and managed.

**Interoperable**: Data and digital resources should be structured and described in a way that enables their integration and interoperability with other data and resources. This involves using common data standards, formats, and protocols, so that different systems can work together effectively.

**Reusable**: Data and digital resources should be designed and documented in a way that allows others to easily reuse and build upon them. This includes providing clear and comprehensive documentation, licensing information, and other relevant context to facilitate proper understanding and utilization.

The FAIR principles were introduced to address the challenges of data sharing and integration, especially in scientific research, where vast amounts of data and digital resources are generated but often remain siloed or difficult to use effectively. By adhering to the FAIR principles, data producers and curators aim to make their resources more discoverable, accessible, and usable by the broader research community, leading to increased collaboration and the acceleration of scientific advancements.

Further information on FAIR principles: 

* [Go-FAIR initiative: FAIR Principles](https://www.go-fair.org/fair-principles/) 
* FAIRSFAIR: How to be FAIR with your data. A teaching and training handbook for higher education institutions ([Engelhardt et al. 2022](https://doi.org/10.5281/zenodo.6674301)) 
* [OpenAIRE: How to Make your data fair](https://www.openaire.eu/how-to-make-your-data-fair)