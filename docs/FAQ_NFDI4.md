---
name: Where does the content in the NFDI4Earth come from?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth


isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - dfgfo:34
  - unesco:mt2.35

keywords:
  - NFDI4Earth

audience: 
  - general public

version: 1.0

license: CC-BY-4.0

---

# Where does the content in the NFDI4Earth come from?

On the one hand, content is provided by the participating institutions and the Earth System Science Community. On the other hand, information on repositories, services, etc. is automatically collected from various sources such as [re3data](https://www.re3data.org).
