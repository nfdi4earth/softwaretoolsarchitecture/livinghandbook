---
name: Graph Based Visual Search Engine (VESA)

description: A visual data search application, where data from different repositories are merged by a knowledge graph. The result is then shown via an interactive and filtering-based visualization dashboard. 


author: 
  - name: Dr. Pawandeep Kaur Betz
    orcidId: https://orcid.org/0000-0002-3073-326X
  - name: Dr. Tobias Hecking 
    orcidId: https://orcid.org/0000-0003-0833-7989 

inLanguage: ENG


about:
  - external nfdi4earth
  - tools and techniques

isPartOf: 
  - N4E_Pilots.md
  - LHB_ENG.md

subjectArea: 
  - dfgfo:409-05

keywords:
  - Data Search
  - Knowledge Graph
  - Visual Search 

audience: 
  - data user
  - general public


identifier: 

version: 0.1

license: CC-BY-4.0

---

# Graph Based Visual Search Engine (VESA)

_**Contribution to the community**: VESA is developed to enhance data findability from multiple data sources. The backend of VESA is a knowledge graph that merges metadata from different data repositories. The frontend dashboard provides an intuitive and interactive way to explore the metadata and search the datasets available through the knowledge graph_.

The FAIR data principles advocate for making scientific and research datasets findable. Yet, the sheer volume and diversity of these datasets present significant challenges. Despite advancements in data search technologies, techniques for presenting search results are still traditional and inadequate, often returning extraneous results. To address these issues, we developed a knowledge graph-based visual search application called VESA (Visualization Enabled Search Application). At its core, it uses a knowledge graph to seamlessly connect metadata from two different repositories, enabling users to search from a common platform.

The backend of VESA is built and configured for the Earth System Scientists (ESS) connecting part of the PANGAEA and DLR Earth Observation data repositories. Its frontend uses various chart widgets to present search results in four different perspectives: a map-based view (to visualize the location where the datasets were observed or collected), a list-based view (to show results relevant to a selected topic, e.g. temperature), a line chart (to visualize the year and month when the datasets were observed or collected) and a chord diagram (to show collaboration links between researchers of a community). This approach enhances the discovery process and uncovers intricate connections, facilitating deeper insights into the datasets explored and more informed decision-making ([Media 1](#video1)). For instance, the exploration of the dataset and its filtering through different widgets, can assist in knowing the quality of the metadata content.

<a id="video1"></a>

![](videos/vesa_teaser_video.mp4 "") 

## Outcomes
- [Software repository](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/pilots/graph-based-visual-search-engine)  
- [Prototype on the Web](https://vesa.webapps.nfdi4earth.de/) 
