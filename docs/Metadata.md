---
name: 'Introduction to the Collection: Metadata - Information about research data'

description: Metadata provide information about research data. These information are as well treated as data, hence, the term 'metadata'. The proper annotation of research data with metadata is a mandatory part of creating FAIR research data.

author: 
  - name: Michael Finkel
    orcidId: http://orcid.org/0000-0002-5268-5203
  - name: Dominik C. Hezel
    orcidId: http://orcid.org/0000-0002-5059-2281

inLanguage: ENG

about:
  - internal nfdi4earth
  - metadata


isPartOf: 
  - Collection_ObserveCollectCreateEvaluate.md

additionalType: collection

subjectArea: 
  - dfgfo:313
  - dfgfo:314
  - dfgfo:315
  - dfgfo:316
  - dfgfo:317
  - dfgfo:318
  - dfgfo:409-06
  - unesco:concept7398

keywords:
  - research data management
  - metadata
  - taxonomy

version: 0.1

audience:
  - data collector
  - data depositor
  - data steward
  - data user
  - data advocate
  - data curator
  - data provider

license: CC-BY-4.0

---

# Metadata

This is an introductory article to the **Collection of articles on Metadata**.  This collection comprises contributions  on all aspects and issues relevant to the creation of rich and robust metadata.  

## Metadata - Information about research data <br> (or: The data about the data)

Metadata is information that is added to (research) data to provide context and information about the (research) data itself. Metadata is essential to understand, manage and utilise data effectively. Typically, metadata is a set of attributes or properties (e.g., author, date, measurement parameters) that offers insights into the content, structure and characteristics of a dataset, document, file or any other information resource. These attributes can be grouped into different categories such as:

- Summary: What is the dataset about?
- Provenance: Who created it?
- Context: How was the dataset collected or generated and why does it exist?
- Content: What does the dataset include?
- Usability: How can the dataset be used?
- Temporal and Spatial Coverage: What is the dataset’s time frame? What is the spatial coverage of the dataset?
- Privacy: Is there any restricted content, why is it private, and how can it be accessed?

Metadata may be created automatically or manually. The detailedness (i.e., [granularity](Metadata.md#granularity-of-metadata)) of metadata varies depending on the type and purpose of the data. Metadata should at least contain relevant basic information about provenance and content (e.g., according to the ["Dublin Core"](https://www.dublincore.org/specifications/dublin-core/dces/) standard set), but hundreds of metadata attributes may be used for a detailed description of an individual single dataset.

Metadata and [metadata standards](Metadata.md#metadata-standards-in-earth-system-sciences) differ among disciplines and domains, may evolve over time along with the progression of technology and research fields, and are usually specific to the type of (research) data that is described.

## The importance of metadata

The [appropriate annotation of research data](Metadata.md#appropriate-metadata) with metadata is mandatory when creating FAIR research data. Proper metadata need to provide all information required to comprehend how the data were created and how it is to be understood, thereby ensuring its findability and reusability. Metadata that follows given standards for vocabularies, schemas, etc. ensures interoperability between databases and systems. 

Missing metadata limit the value of research data to the person who generated these, for as long as she or he can memorise how the data were measured, modelled, collected, etc. Any other person will not benefit, either because the research data won't be found, or because the research data won't be understood good enough for making use of it in the context of further research. Hence, metadata are a mandatory part of research data, and undoubtedly as equally important as the data themselve. Or, as [Jason Scott](https://x.com/textfiles/status/119403173436850176?s=20) coined it: "Metadata is a love note to the future."

## Types of metadata

Metadata may be discriminated into *descriptive*, *structural*, and *administrative* metadata. *Descriptive metadata* describe the contents of a data set. *Structural metadata* provide information about the organisation of a data set. *Administrative metadata* contain technical information about a data set.

## Appropriate metadata

Metadata should be *rich* and *robust*. *Rich* means, metadata need to be complete, accurate, and detailed to allow others to find, interoperate and reuse the data. *Robust* refers to e.g., continuity, consistency, curation and maintenance, practicability, uniqueness, avoidance of misinterpretation. Specific requirements for *rich* and *robust* depend on the type of data, available standards and protocols, the resepective discpline or community, as well as the use case. Based on the [FAIR data principles](https://force11.org/info/the-fair-data-principles/), descriptions, criteria and indicators for *rich* and *robust* were proposed, e.g., by the [GoFAIR Initiative](https://www.go-fair.org/fair-principles/) and the Research Data Alliance FAIR data maturity model Working Group, as part of the specification and guidelines towards a [FAIR Data Maturity Model](https://doi.org/10.15497/RDA00050).

## Granularity of metadata 

The more detailed metadata are, the more *granular* they are. A higher granularity allows a more precise search and analysis of the data. Efficient and effective reuse of data requires finding and accessing data at various levels of granularity. The ideal level of granularity for metadata needs to be defined for the particular data type and usage context. The [Research Data Alliance Data Granularity Workgroup](https://www.rd-alliance.org/groups/data-granularity-wg), for example, explores key questions and collects and shares information on how to best find appropriate data granularity, thereby providing guidance to help data professionals to determine the best level of granularity for user discovery, access, interoperability and citability. 

## Using vocabularies for metadata naming

To ensure the creation of consistent metadata that can be accurately and quickly searched and retrieved across various databases, controlled vocabularies should be used for

- the naming of metadata items, and
- defined lists of accepted terms used to populate descriptive metadata items (e.g., [@Hedden2010]).

The use of vocabularies is a prerequisite to overcome ontological and semantic discrepancies when data are synthesized across repositories (e.g., [@Piasecki2009]).

## Metadata standards (in Earth System Sciences)

Metadata standards facilitate not only technological, but also semantic interoperability between infrastructures such as research data repositories. Several general, i.e., cross-domain as well as domain-specific standards exist for the Earth System Sciences. The broad term *metadata standard* includes standards that may or may not be technology-agnostic, i.e., some standards have a formal specification (e.g., in the form of an XSD or an RDFS schema), others have none. However, even an approved metadata schema can be flawed or is only used by a few. Similarly, an unapproved, but widely used metadata schema might act as *quasi-*standard in a given domain. There might even exist several metadata standards for the same type of data (e.g., geospatial metadata standards - see information at [FGDC.gov](https://www.fgdc.gov/metadata/geospatial-metadata-standards)). A catalog of metadata standards has been issued by the Research Data Alliance: [Metadata Standards Catalog](https://rdamsc.bath.ac.uk/).

## Acknowledgement
The writing of this article was inspired by several webpages – many thanks to: 

* [Research Data Alliance](https://www.rd-alliance.org), 
* [Helmholtz Metadata Collaboration (HMC)](https://helmholtz-metadaten.de/en), 
* [esri](https://desktop.arcgis.com/en/arcmap/latest/manage-data/metadata/metadata-standards-and-styles.htm), 
* [opendatasoft](https://www.opendatasoft.com/en/resources/guide-metadata/), and 
* [StreamSets](https://streamsets.com/learn/metadata-management/).

## References
