---
name: The GeoFRESH online platform

description: Description of the NFDI4Earth Pilot project. 

author: 
  - name: Sami Domisch
    orcidId: https://orcid.org/0000-0002-8127-9335
  - name: Vanessa Bremerich
    orcidId: https://orcid.org/0000-0002-7657-1534
  - name: Yusdiel Torres-Cambas
    orcidId: https://orcid.org/0000-0003-2312-2329
  - name: Afroditi Grigoropoulou
    orcidId: https://orcid.org/0000-0002-7884-097X
  - name: Jaime R. Garcia Marquez
    orcidId: https://orcid.org/0000-0002-1998-5669
  - name: Giuseppe Amatulli
    orcidId: https://orcid.org/0000-0002-8341-2830
  - name: Luc De Meester
    orcidId: https://orcid.org/0000-0001-5433-6843
  - name: Hans-Peter Grossart
    orcidId: https://orcid.org/0000-0002-9141-0325
  - name: Mark Gessner
    orcidId: https://orcid.org/0000-0003-2516-7416
  - name: Thomas Mehner
    orcidId: https://orcid.org/0000-0002-3619-165X
  - name: Rita Adrian
    orcidId: https://orcid.org/0000-0002-6318-7189

inLanguage: ENG

about:
  - external nfdi4earth
  - dataset
  - tools and techniques
  - workflow

isPartOf: 
  - N4E_Pilots.md
  - Collection_ObserveCollectCreateEvaluate.md
  - Collection_ProcessAnalyse.md

additionalType: technical_note

subjectArea: 
  - dfgfo:315-02
  - dfgfo:318-01
  - unesco:concept4583
  - unesco:concept7741
  - unesco:concept8387
  - unesco:concept3652
  - unesco:concept8389
  - unesco:concept1878
  - unesco:concept188
  - unesco:concept11433
  - unesco:concept197
  - unesco:concept14174

keywords:
  - stream
  - river
  - network
  - routing
  - Hydrography90m
  - climate
  - topography
  - land cover
  - soil
  - PostgreSQL
  - R
  - Shiny
  - GDAL/OGR
  - GRASS GIS

audience: 
  - data user
  - data librarian

version: 1.0

identifier: https://doi.org/10.5281/zenodo.7888389


license: CC-BY-4.0

---

# The GeoFRESH online platform 

**Getting freshwater spatiotemporal data on track: towards the interoperability of freshwater-specific data**

Spatiotemporal earth system data on freshwaters are poorly organized at present and their rich potential for research and environmental management is barely exploited. Part of this deficiency relates to the spatial structure of river networks, in particular, which requires a specialized workflow for earth system data integration and use. To address this challenge, we developed the GeoFRESH online platform, available at <http://geofresh.org/>. GeoFRESH provides the integration, processing, management and visualization of standardized spatiotemporal earth system data related to freshwaters on the global scale. Users can (1) upload point data; (2) automatically assign the points to a new fine-scale hydrographical network; (3) map the points to the corresponding upstream catchment; (4) annotate the points with site-specific and upstream environmental information based on a suite of 104 layers capturing topography [@DOI:10.5194-essd-14-4525-2022], climate [@DOI:10.1038-sdata.2017.122], landcover [@ESA-Landcover2017] and soil characteristics [@DOI:10.1371-journal.pone.0169748]; (5) calculate network distances among points; and (6) download the compiled data ready for downstream analyses. GeoFRESH capitalizes on open-source PostgreSQL, PostGIS and pgRouting software, building on a new database comprising the global ‘Hydrography90m’ stream network at 90 m resolution [@DOI:10.1038-sdata.2017.122], comprising a total of 726 million unique stream segments. The platform is readily expandable thanks to its modular structure and serves as a key element in supporting freshwater science and management relying on high-resolution geospatial analyses. GeoFRESH thus contributes significantly to FAIR data management and exchange by enabling globally standardized stream network queries and offering a powerful tool to stakeholders engaged in water research, sustainable water management and the monitoring of freshwater ecosystem services alike.

![GeoFRESH logo](img/Pilot_GeoFRESH_logo.png "GeoFRESH logo.")

## Funding statement
This work has been funded by the German Research Foundation (DFG) through the project NFDI4Earth (TA1 M1.1, DFG project no. 460036893, <https://www.nfdi4earth.de/>) within the German National Research Data Infrastructure (NFDI, <https://www.nfdi.de/>). 

## Resources
* [GeoFRESH website](http://geofresh.org/)
* Pilot report ([Zenodo](https://doi.org/10.5281/zenodo.7888389))
* Code repository ([Zenodo](https://doi.org/10.5281/zenodo.7888430) | [ GitHub](https://github.com/glowabio/geofresh))
* [hydrographr R-package](https://github.com/glowabio/hydrographr) that is planned to be linked to the platform 

## References
