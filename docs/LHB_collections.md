---
name: Collections
description: Collections are used to structure the content of the Living Handbook. 

author: 
  - name: Thomas Rose
    orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

isPartOf: LHB_ENG.md

additionalType: technical_note

subjectArea: 
  - unesco:concept4224


audience: 
  - general public

version: 1.0

license: CC-BY-4.0

---

# Collections

Collections are articles of the Living Handbook that structure its content. They consist of a short abstract or summary about the topic, followed by a list of the articles that belong to this collection. The list is automatically created from the metadata of the articles assigned to this collection.  

Collections aim to help readers in gaining a better overview on a specific topic by providing a list of relevant articles. Consequently, not all articles are part of collections. Collections can belong to one or more collections, and articles can be part of more than one collection. As a result, a hierarchical network is built from the topics represented in the Living Handbook ([Media 1](#med1)).

<a id="med1"></a>

![Structure of Living Handbook Collections](img/collections.png "Schematic visualisation of how collections structure the content of the Living Handbook.")
