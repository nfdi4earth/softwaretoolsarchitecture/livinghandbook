---
name: The NFDI4Earth Label - Introduction

description: This article explains the motivation and the approach of the NFDI4Earth Community Label. The Label is awarded to research data repositories in the Earth Sciences that have demonstrated their standards compliance, technical interoperability, and governance.

author:
  - name: Tim Schäfer
    orcidId: https://orcid.org/0000-0002-3683-8070
  - name: Ronny Gey
    orcidId: https://orcid.org/0000-0003-1028-1670

inLanguage: ENG

about:
  - internal nfdi4earth
  - publication
  - data service
  - tools and techniques
  - licensing options


isPartOf:
  - LHB_ENG.md
  # - Collection_StoreManage.md

additionalType: user_guide

subjectArea:
  - dfgfo:409-06
  - dfgfo:34
  - unesco:concept4888
  - unesco:concept17162

#Information Systems, Process and Knowledge Management; geosciences; Data centre; digital platform

keywords:
  - NFDI4Earth Label
  - data repository
  - research data management
  - certification
  - assessment
  - geosciences


audience:
  - data librarian
  - data curator
  - policy maker
  - service provider
  - data provider

# identifier:

version: 0.1

license: CC-BY-4.0
---


# Introduction to the NFDI4Earth Community Label

The NFDI4Earth Community Label project aims to assess and subsequently improve the level of interoperability and trustworthiness of research data repositories in the Earth System Sciences (ESS). It provides clear guidelines for repositories, transparent evaluation criteria to assess the degree to which infrastructures conform to the guidelines, and a graphical badge to showcase the efforts and capabilities of certified repositories.

![NFDI4Earth Community label](https://img.shields.io/badge/NFDI4Earth%20Label-Certified-green)

## Motivation

The landscape of research data infrastructures in the ESS is very diverse and contains a [large number of data repositories](https://onestop4all.nfdi4earth.de/search?resourcetype=Repository+%26+Archive&sort=) and metadata aggregators of different sizes and from different sub-disciplines. These data providers use a variety of interfaces, metadata exchange protocols, and metadata schemes to facilitate FAIR access to research data.

However, the degree to which standards are used and the exact choice of standards often differ between repositories, which makes it challenging to provide crucial services across infrastructures. Examples of such services required by scientists include searching across many repositories, including the ability to filter by geographic location and time, and thus the prerequisite for being able to combine data sets from different sources and sub-disciplines to answer new research questions. Scientists also need a means to identify trustworthy and suitable repositories, but information on research data repositories is often incomplete or out-of-date in public registries.

Clear guidelines for repositories can help here, proposing a set of standards that the ESS community has agreed on. This would be useful in combination with easily available information about which repositories implement these guidelines, for example through certification, a label or a graphical badge. However, most currently available certifications are not specific to the ESS and are hard to obtain for smaller data repositories. NFDI4Earth is therefore developing the NFDI4Earth label to enable repository operators to establish compliance with common standards and to signal this to their users via a badge.

## Approach

To assess and subsequently improve the level of interoperability and trustworthiness of research data repositories in the ESS, we have developed a semi-automated process for the evaluation of research data repositories that consists of:

- The mandatory registration of the repository at [re3data.org](https://re3data.org), which provides a unique identifier and ensures the public availability of metadata on the repository itself in machine-readable form.
- A set of guidelines for repositories, each one related to a [FAIR principle](https://www.go-fair.org/fair-principles/), which define common standards that should be implemented by all repositories in the ESS to facilitate interoperability and increase trustworthiness.
- Specific evaluation metrics that can be used to programmatically evaluate repositories and measure the degree to which a repository conforms to the guidelines.
- A semi-automated assessment procedure, that consists of:
  1. Automated retrieval of information on the repository from re3data, combined with
  2. The automated FAIR assessment of a random sample of datasets stored in the repository using the [F-UJI tool](https://f-uji.net), and
  3. A self-evaluation form to be filled out by repository representatives that queries information on the repository that is not available from re3data.
- A transparent workflow that will be used to guide repository representatives through the process of applying for, obtaining, and renewing the Label.

## Progress

We have held several events to take into account the interests of the user community and repository providers. In October 2023, a **label forum** took place that was aimed at data repository operators and at which we discussed the workload and benefits for repository operators interested in the label. In March 2024, another **workshop** took place at the NFDI4Earth working meeting in Frankfurt, where we discussed our approach in detail with representatives of the consortium. The NFDI4Earth user community, in turn, was addressed in May 2024 during the [NFDI4Earth Plenary](https://www.nfdi4earth.de/nfdi4earth-plenary-2024) in Dresden.

The following reports have been published as part of the project work on the NFDI4Earth label: [The NFDI4Earth Label - Concept Overview (NFDI4Earth White Paper)](https://doi.org/10.5281/zenodo.13711458) and the [NFDI4Earth Label Status Report (NFDI4Earth Milestone MS3.2.1)](https://doi.org/10.5281/zenodo.13711419).

