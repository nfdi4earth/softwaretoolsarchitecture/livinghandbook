---
name: Become a helpdesk expert

description: NFDI4Earth aims to a network that supports FAIR and open research data management practices in the Earth System Sciences. Here, we provide an overview of steps to become an expert in our user support network.

author:

  - name: Hela Mehrtens
    orcidId: https://orcid.org/0000-0002-4526-2472
  - name: Klaus Getzlaff
    orcidId: https://orcid.org/0000-0002-0347-7838

inLanguage: ENG

about:
  - internal nfdi4earth  

isPartOf:
  - Collection_ObserveCollectCreateEvaluate.md

additionalType: article

subjectArea:

keywords:
  - showcase
  - helpdesk
  - user support
  - FAIR principles

version: 0.1

license: CC-BY-4.0
---

# Become a helpdesk expert

NFDI4Earth is a community-driven initiative aiming to provide researchers with FAIR, coherent, and open access to relevant Earth System resources. Our NFDI4Earth helpdesk supports researchers in their research data management activities, such as finding a proper repository for research outcomes or answering questions on data formats and processing. Our helpdesk experts are located in institutes all over Germany and offer their specific ESS expertise. Considering the breadth of the ESS discipline and the diversity of requests, we are looking for volunteers who would share their expertise and support other researchers by becoming an NFDI4Earth helpdesk expert.

To become an NFDI4Earth helpdesk expert, you can follow these steps:

## 1. Understand the NFDI4Earth goals and requirements

* __[Mission](https://www.nfdi4earth.de/about-us)__: Familiarize yourself with the mission and goals of NFDI4Earth. This will help you ensure that your support aligns with our objectives. 

## 2. Tell us more about how you can support

* __Expertise__: Describe the expertise you could offer the ESS community to facilitate research data management. This could be narrowed to a specific ESS sub-discipline, like geochemistry, or an RDM topic, e.g., data management planning. 
* __Availability__: Provide your availability for answering requests.

## 3. Contact the NFDI4Earth Helpdesk
* Please provide your questions or interest and information about your support to the NFDI4Earth Helpdesk:  [helpdesk@nfdi4earth.de](mailto:helpdesk@nfdi4earth.de).
 
## 4. Get contacted by the helpdesk to answer support questions 
* Your name, expertise and contact email will be included in our internal expert list and tickets related to your expertise will be forwarded to you. Your answers will be included in the ticket and visible for the other agents and the customer.

<!--

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences).

-->


