---
name:  What is the difference between storing, publishing and long-term storage of/archiving research data?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth
  - publication
  - storage
  - sharing data


isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - unesco:concept2768
  - unesco:concept7398


keywords:
  - data archiving
  - data publication
  - data storage

audience: 
  - data depositor
  - data steward
  - data provider
  - data curator

version: 1.0

license: CC-BY-4.0

---

#  What is the difference between storing, publishing and long-term storage of/archiving research data?

Storing, publishing, and long-term storage/archiving of research data are distinct concepts within the realm of managing and preserving research-related information. Effective research data management involves considering these different concepts as stages of the lifecycle of data.

## Storing Research Data

Research data is stored during the act of collecting and processing data. This might involve using data management systems, cloud storage, local servers, external storage, or other digital repositories. The main purpose is to keep data secure and accessible during the active phase of a research project.

## Long-Term Storage/Archiving of Research Data

Processed or raw data can be archived as long as it is of archival value, which is defined by the researcher/data owner. This process includes adhering to data preservation standards, metadata documentation, and safeguarding against technological obsolescence. Long-term storage often relies on dedicated data archives, repositories, or libraries that specialize in maintaining and curating research data for the long haul. The main purpose is preserving research data over the long term, ensuring its continued accessibility and usability for future researchers.

## Publishing Research Data

This stage entails making the data publicly available, often alongside research findings, in a way that allows other researchers, scholars, and the broader community to access and reuse the data. Publishing research data often involves specialized data repositories, institutional databases, or as supplementary material alongside publications. The main purpose is to make research data publicly findable for others.

Further information on the Research Data Lifecycle on [forschungsdaten.info](https://forschungsdaten.info/themen/informieren-und-planen/datenlebenszyklus/) (in German).