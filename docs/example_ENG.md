---
name: Template article
# The title of the article.

description: 
# A brief summary of the article, max. 300 characters incl. spaces.

author: 
  - name: Jane Doe
    orcidId: https://orcid.org/0000-0002-1584-4316
  - name: John Doe
    orcidId: https://orcid.org/0000-0001-2345-6789
# The author(s) of the article. The name must be given, the ORCID should be given. As many authors as necessary can be listed. 

inLanguage: ENG
# The language of the article. Accepted values are:  
# ENG for English
# DEU for German

about:
  - internal nfdi4earth
  - publication


isPartOf: 
  - LHB_ENG.md
  - Collection1.md
  - Collection2.md
# The filenames of the NFDI4Earth Living Handbook collection(s), the article belongs to, as they appear in https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/tree/main/docs. 
# Alternatively, the titles of the collections. The editor will replace it with the collections' file names. 

additionalType: user_guide
# The type of the article. Acceptable terms are: 
# article: The most generic category. Should only be used if none of the others fit. 
# best_practice: A procedure of how a defined problem described in the article can be solved which is a (de-facto) standard or widely accepted in the community
# workflow: A step-by-step description of a procedure
# technical_note: A short article presenting a new software tool, method, technology. 
# recommended_article: a short review of a publication.
# discussion/review: An article that summarises/compares or (critically) discusses e.g., the state of understanding, different approaches or methodologies of a topic. 
# user_guide: Manuals on how to use e.g., software products. 
# isPartOf: A NFDI4Earth Living Handbook collection article.
# FAQ: Can only used by the NFDI4Earth User Support Network, which define the FAQs. 
# topical_entry: Can only used by the NFDI4Earth Living Handbook Editorial Board. Identifies the articles featured in the topical entry points. 
# call_participate: Can only used by the NFDI4Earth Living Handbook Editorial Board. Identifies the articles featured in the "How to participate" section on of the OS4A Portal start page. 

subjectArea: 
  - dfgfo:313-02
  - unesco:concept160
  - unesco:mt2.35
# A list of controlled terms describing the subject of the article, given as their identifiers. Acceptable are any terms from: 
# DFG subject ontology: https://terminology.tib.eu/ts/ontologies/dfgfo/terms
# UNESCO thesaurus: https://vocabularies.unesco.org/browser/thesaurus/en/

keywords:
  - keyword1
  - keyword2
# A list of terms to describe the article's topic more precisely. 

audience: 
  - data collector
  - policy maker
# A list of the main roles of persons within research data management that is addressed by the article. 
# data collector: The person responsible for collecting, generating, re-using or contributing to research data and documents contexts and processes that relates to data handling. 
# data owner: Typically the principal researcher of the project or research group leader who is responsible for establishing who has access to the data, and reducing or mitigating the risk of any mishaps; e.g. improper storage facility, non-compliance with specific guidelines. 
# data depositor: Either the data creator or data owner and is the person who will perform the deposit of the data to a data preservation facility, like a repository or data archive. They perform activities such as uploading data after it has been generated, making it suitable for archiving, creating metadata and data documentation and submitting the data to the facility, so it can be preserved for the long-term. 
# data user: Someone who is interested in accessing and interacting with data for verification or (re)using data to evaluate, analyse, model and generally handle them to create insights and knowledge. 
# data curator: A person who ensures availability and reliability of data. They review and harmonise metadata of datasets submitted for storage and ensure that they are generally compliant with relevant policies before publication. They further monitor access to the data, support data owners in fulfilling data access requests and possess deeper technical knowledge about the backend of the repository or storage system. 
# data librarian: An information professional responsible for developing technical expertise on practical solutions of data management, archiving and dissemination and on data mining and visualization. 
# data steward: A supporter who advises researchers (or otherwise involved) persons on data management, privacy and information security aspects and can understand the meaning and applications of the data. They support: data creators on quality assurance, storage, policy compliance; data depositors on data selection, metadata and data documentation creation, preserving and publishing; data custodians on data curation, access controls, FAIR data management 
# data advocate: A person promoting the proper use of research data and technology and providing assistance or guidance in doing so. 
# data provider: A person or organization that stores and provides datasets to others.
# service provider: An organization that provides services related to the processing and management of data, such as, high-performance computing facilities or web-based maps.
# research software engineer: A person working with researchers to gain an understanding of the problems they face, and then develops, maintains and extends software to provide the answers. 
# policy maker: In research data management, policy makers strive to design effective, technology-neutral, forward-looking and coherent data governance policies across sectors.
# general public: All persons that do not belong to any of the other groups. 

identifier: 
# If the article has already a unique identifier such as an DOI, provide it here as URL (e.g., https://doi.org/10.1001/12345). 

version: 0.1
# The version of the article; will be updated by the editor. 

license: CC-BY-4.0
# The license of the article. Must not be altered. 

---

# Template article

This article provides you with the most important information on how to write an article for the NFDI4Earth Living Handbook. 

It covers the metadata describing Living Handbook articles, the recommended structure of articles, and how to deal with figures and other linked or embedded content and references. Articles in the Living Handbook are written in Markdown. Therefore, this article also provides some instructions on how to do text-formatting and inclusion of external content is done in Markdown. Although submission of articles in Markdown is strongly encouraged, we also provide this [template in the docx format](pdf/template_article_ENG.docx) as alternative. Please note that instructions below vary between the documents if they are specific for the respective file format. 

## Metadata
To view the metadata, please read the [raw version of the article](https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/blob/main/docs/example_ENG.md). In the raw version, the top part of this document, enclosed in `---`, contains the metadata associated with the articles, i.e., information that describes your article. Please fill it in/replace the examples according to the explanation provided for each metadatum. Afterwards, you can delete any line starting with `#`. 

## Structure of NFDI4Earth Living Handbook articles
Except for some specific types of articles, there is no specific outline for Living Handbook articles. Instead, the content should be arranged in a way that suits the topic best. To ease comprehensibility of the content, we strongly recommend following a "spiral out" approach. This means that the core of the topic is explained first, then *all* its main aspects are briefly developed before detailed accounts for some or all of them are given. Structuring content this way allows readers unfamiliar with the content to acquire general knowledge before encountering expert knowledge. 

If a topic is very complex, it might be better to split it up into multiple articles and develop the different aspects in detail there, while the "main" article provides a summary/review style perspective of the topic and refers to them. 

## Media and other linked/embedded content
Media and other content can significantly enhance the comprehensibility on an article. Therefore, any type of media and other non-text content can be included in an article. This also includes interactive content such as widgets, 3D models, maps or Jupyter Notebooks. Please find in the section [Writing in Markdown](#writing-with-markdown) a list of how different media can be included when preparing the article in markdown. If the type of content you want to include is not listed there, please get in touch with the editors via <nfdi4earth-livinghandbook@tu-dresden.de> to check how it can made possible. 

## References and Links
References are important for supporting the content of an article. In addition, they provide important entry points for an in-depth study of the topic beyond the article. Therefore, all content must be supported with exemplary suitable references. Open access publications/resources should be preferred. There is no need for providing an exhaustive reference list for the article, but essential references should be cited such as given in the following example:

*The FAIR principles ([@wilkinson_etal2016]) are a set of guiding principles designed to enhance the discoverability, accessibility, interoperability, and reusability of digital resources, particularly in the context of scientific data and research.*

whereas the given reference refers to a corresponding entry in the list of references, which is compiled in file references.bib. The references are then automatically listed at the bottom of the article in subsection *##References* (add an empty subsection to do so).

Please also consider adding an "Additional resources" section to facilitate additional research on the topic by the readers. 

Links to other articles in the NFDI4Earth Living Handbook may also be used (in this way, e.g., "Go to [FAQ_RDM2.md](FAQ_RDM2.md) to learn more about the FAIR principles.").

## Writing with Markdown
The NFDI4Earth Living Handbook is written in Markdown, which ensures a high degree of flexibility on a technical level while having a high degree of readability as raw text. The general Markdown syntax is well explained [in this Cheat Sheet](https://www.markdownguide.org/cheat-sheet/). In addition, the NFDI4Earth Living Handbook supports some additional syntax. 

### Including media/external content

* `![Alt text](LINK "Caption in double quotes")` places an image at the location LINK with its caption placed under it and the Alt text being accessible to screen readers (it will also be displayed, if the link is broken). Numbering will be done automatially. 
* `[![Alt text](https://img.youtube.com/vi/YOUTUBE-ID/0.jpg "Caption in double quotes")](https://www.youtube.com/watch?v=YOUTUBE-ID)` includes the Youtube video identified by its YOUTUBE-ID. Analogous to images, the caption will be displayed below the video and the Alt text should contain a description/summary of the video for screen readers. 
* Raw HTML code can be included, allowing to, e.g., copy-paste snippets provided via the "Embed" button. 

## Additional resources

* [Author guidelines](LHB_author-guidelines_ENG.md)
* [Editorial Workflow](LHB_editorial-workflow_ENG.md)

## References
