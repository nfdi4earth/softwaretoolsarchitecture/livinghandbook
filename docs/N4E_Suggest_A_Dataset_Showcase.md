---
name: Suggest your Dataset as Showcase

description: NFDI4Earth aims to share information on high-quality datasets that support FAIR and open research data management practices in the Earth System Sciences. Here, we provide an overview of steps to suggest your community dataset as showcase.

author:
  - name: Christin Henzen
    orcidId: https://orcid.org/0000-0002-5181-4368

inLanguage: ENG

about:
  - internal nfdi4earth  

isPartOf:
  - Collection_ObserveCollectCreateEvaluate.md

additionalType: article

subjectArea:

keywords:
  - showcase
  - FAIR dataset

version: 0.1

license: CC-BY-4.0
---


# Suggest your dataset as showcase

NFDI4Earth is a community-driven initiative aiming to provide researchers with FAIR, coherent, and open access to relevant Earth System resources, such as data. NFDI4Earth Dataset Showcases highlight existing initiatives or high-quality and relevant datasets towards FAIRness and openness and raise their visibility within the community.  

Sharing your expertise and offering high-quality community datasets as resources or best-practice solutions is key to supporting the researchers. To suggest an NFDI4Earth Dataset Showcase, you can follow these steps:

## 1. Understand the NFDI4Earth goals and requirements

* __[Mission](https://www.nfdi4earth.de/about-us)__: Familiarize yourself with the mission and goals of NFDI4Earth. This will help you ensure that your contribution aligns with our objectives.
* __[NFDI4Earth Dataset Showcases](https://onestop4all.nfdi4earth.de/howtoentry/data)__: Review existing showcases to learn more about addressed data-driven topics.

## 2. Tell us more about your showcase by preparing the following information

* __Title__: Provide a concise title allowing the community to identify your main contributions
* __Authors__: List author names and their [OrcIDs](https://orcid.org/), if available
* __Short description__: Provide 1-2 sentences as teaser text for your showcase
* __Thumbnail figure__: Provide an illustrative figure with an open license to underpin the teaser text
* __Description__: Describe your showcase. You can add links and references, if needed.
* __High-resolution figure__: Provide a high-resolution figure that shows your data and facilitates understanding the description
* __Contributions to the community__: Clearly describe how your dataset contributes to standards, standardisation of workflows, or implementing the FAIR principles in a couple of sentences. Moreover, describe contributions to the quality assurance of data, if suitable. State where your dataset is reused, e.g., by naming a specific community, project, or initiative. Provide information about other benefits/impacts of your data.
* __Outcomes__: Provide links to your outcomes
* __Subdiscipline__: Add relevant subdisciplines according to the [DFG classification](https://www.dfg.de/en/research-funding/proposal-funding-process/interdisciplinarity/subject-area-structure)
* __Keywords__: Provide a list of keywords that facilitates search and discovery


## 3. Contact the NFDI4Earth Helpdesk

* Please provide your questions or interest and information to the NFDI4Earth Helpdesk: [helpdesk@nfdi4earth.de](mailto:helpdesk@nfdi4earth.de). We will coordinate follow-up activities with the related NFDI4Earth working groups.

![NFDI4Earth](img/NFDI4Earth.jpg)
<!--

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences).

-->

