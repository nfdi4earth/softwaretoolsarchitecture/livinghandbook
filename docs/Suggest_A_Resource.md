---
name: Suggest your resource

description: NFDI4Earth aims to share information on high-quality resources that support FAIR and open research data management in the Earth System Sciences. Here, we provide an overview of steps to suggest your resource.

author:
  - name: Christin Henzen
    orcidId: https://orcid.org/0000-0002-5181-4368

inLanguage: ENG

about:
  - internal nfdi4earth  

isPartOf:
  - Collection_ObserveCollectCreateEvaluate.md

additionalType: article

subjectArea:

keywords:
  - metadata
  - services
  - repository
  - dataset
  - showcase
  - tools
  - software
  - learning materials

version: 0.1

license: CC-BY-4.0
---

# Suggest your resource

NFDI4Earth is a community-driven initiative aiming to provide researchers with FAIR, coherent, and open access to relevant Earth System resources.

Sharing your expertise and offering high-quality resources is key to supporting the researchers. In NFDI4Earth you are invited to contribute the following resources by following the steps in the linked articles: 

* your [service](N4E_Suggest_A_Service.md)
* your [repository](N4E_Suggest_A_Repository.md)
* Your [open educational resource](N4E_Suggest_an_OER.md)
* your [software or tool in a showcase](N4E_Suggest_A_Tool_Showcase.md) 
* your [dataset in a showcase](N4E_Suggest_A_Dataset_Showcase.md). 
  * _Please note that your dataset should be published in a repository first._ 

Our [NFDI4Earth Helpdesk](mailto:helpdesk@nfdi4earth.de) is happy to support you in related steps, e.g., in selecting a repository that fits your data and needs.

![NFDI4Earth](img/NFDI4Earth.jpg)

<!--

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences).

-->
