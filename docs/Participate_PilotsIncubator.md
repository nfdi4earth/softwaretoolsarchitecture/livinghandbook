---
name: Solve community needs and apply for funding

description: Information about which opportunities the NFDI4Earth provides for the community to obtain funding for small projects. 

author: 
  - name: Kolja Nenoff
#    orcidId: https://orcid.org/0000-0003-4017-2467
  - name: Farzaneh Sadeghi
    orcidId: https://orcid.org/0000-0002-7920-4289
  - name: Udo Feuerhake
    orcidId: https://orcid.org/0000-0003-0781-5395

inLanguage: ENG

about:
  - internal nfdi4earth
  - resources

additionalType: call_participate

subjectArea: 
  - dfgfo:34
  - unesco:mt2.35
  - unesco:concept522
  - unesco:concept7387
  - unesco:concept2214

keywords:
  - NFDI4Earth
  - funding 

audience: 
  - data user
  - data collector
  - data steward
  - data librarian
  - service provider


version: 1.0

license: CC-BY-4.0

---

# Solve community needs and apply for funding

NFDI4Earth engages with the community ‒ be it student, professional, service provider or top scientist. Our measures guarantee that the development of NFDI4Earth is entirely **driven by the community’s needs and requirements**. It takes into consideration that the uptake of FAIR principles happens at different speeds and needs different solutions across Earth System Science sub disciplines. 

We support researchers of all ESS domains with flex funds to implement their RDM ideas. The hosting institutions need to be or become participant of the NFDI4Earth consortium to receive the funding. Projects are selected in open calls. Calls will be announced here and on the NFDI4Earth homepage. 

We offer 3 different funding opportunities: 


## Incubators 

The objective of this task is to steer the **exploration of new, potentially relevant building blocks** to be included in NFDI4Earth and related NFDIs. The Incubator Lab also serves as a forum where novel requirements can be formulated and trends presented in terms of a user consultation process. In this way, **scouting for new trends and opportunities** is achieved. The measure lead must oversee and monitor that compliance rules concerning the software and infrastructural developments are fulfilled while at the same time **innovative blue sky developments** should also be encouraged. 

**Duration/Funding**: 3-6 months 1 FTE, corresponding to 17.500€ up to 35.000€ 

**Next Call**: Autumn 2024

**Previous Projects**: <https://www.nfdi4earth.de/2participate/incubator-lab>, [this article](N4E_Incubators.md), and <https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator>

**Contact**: <nfdi4earth-incubators@tu-dresden.de>

## Educational Pilots

The purpose of Educational Pilots is to develop Open Educational Resources that cater to the specific needs of the Earth System Sciences community. These resources are focused on advanced topics and methodologies to address gaps in current ESS education. The initiative aims to produce materials that are beneficial for both self-learning and academic use.

**Duration/Funding**: Funding and duration vary by project needs. 

**Next Call**: We accept proposals on an ongoing basis, which means that innovative ideas can be submitted continuously. 

**Previous Projects**: <https://www.nfdi4earth.de/?view=article&id=314&catid=15> 

**Contact**: farzaneh.sadeghi@hs-bochum.de


## Earth System Science Pilots 

Pilot projects implement innovative solutions to challenges that earth system researchers face in their research data management workflows. The tools can target specific **interoperability issues** for and between particular Earth System Science subdomains and leverage existing technologies. We aim at supporting **solutions that engage a broad user community** and can potentially be transferred to other domains. 
 
**Duration/Funding**: 12 months, 1 FTE corresponding to ~ 71.000€ 

**Next Call**: Last cohort is currently running

**Previous Projects**: <https://www.nfdi4earth.de/2participate/pilots> and [this article](N4E_Pilots.md)

**Contact**: <nfdi4earth-pilots@tu-dresden.de>