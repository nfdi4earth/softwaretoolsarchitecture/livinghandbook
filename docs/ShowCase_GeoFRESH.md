---
name: GeoFresh – Connecting Rivers and Lakes FAIRly 

description: The GeoFRESH platform provides integration, processing, management and visualisation of standardized spatiotemporal data related to freshwaters. 

author: 
  - name: Sami Domisch
    orcidId: https://orcid.org/0000-0002-8127-9335
  - name: Vanessa Bremerich
    orcidId: https://orcid.org/0000-0002-7657-1534
  - name: Yusdiel Torres-Cambas
    orcidId: https://orcid.org/0000-0003-2312-2329
  - name: Afroditi Grigoropoulou
    orcidId: https://orcid.org/0000-0002-7884-097X
  - name: Jaime R. Garcia Marquez
    orcidId: https://orcid.org/0000-0002-1998-5669
  - name: Giuseppe Amatulli
    orcidId: https://orcid.org/0000-0002-8341-2830
  - name: Luc De Meester
    orcidId: https://orcid.org/0000-0001-5433-6843
  - name: Hans-Peter Grossart
    orcidId: https://orcid.org/0000-0002-9141-0325
  - name: Mark Gessner
    orcidId: https://orcid.org/0000-0003-2516-7416
  - name: Thomas Mehner
    orcidId: https://orcid.org/0000-0002-3619-165X
  - name: Rita Adrian
    orcidId: https://orcid.org/0000-0002-6318-7189

inLanguage: ENG

about:
  - external nfdi4earth
  - dataset
  - tools and techniques
  - workflow

isPartOf: 
  - N4E_Pilots.md
  - Collection_ObserveCollectCreateEvaluate.md
  - Collection_ProcessAnalyse.md

additionalType: technical_note

subjectArea: 
  - dfgfo:315-02
  - dfgfo:318-01
  - unesco:concept4583
  - unesco:concept7741
  - unesco:concept8387
  - unesco:concept3652
  - unesco:concept8389
  - unesco:concept1878
  - unesco:concept188
  - unesco:concept11433
  - unesco:concept197
  - unesco:concept14174

keywords:
  - stream
  - river
  - network
  - routing
  - Hydrography90m
  - climate
  - topography
  - land cover
  - soil
  - PostgreSQL
  - R
  - Shiny
  - GDAL/OGR
  - GRASS GIS

audience: 
  - data user
  - data librarian

version: 1.0

identifier: https://doi.org/10.5281/zenodo.7888389


license: CC-BY-4.0

---

# GeoFresh – Connecting Rivers and Lakes FAIRly 

![GeoFRESH logo](img/Pilot_GeoFRESH_logo.png "GeoFRESH logo.")

_**Contributions to the community:** GeoFRESH is a vital tool for FAIR data management in freshwater research, offering globally standardized stream network queries. It supports water research, sustainable management, and ecosystem monitoring, primarily facilitating freshwater ecologists focused on rivers, with plans to extend to lakes. GeoFRESH simplifies scalable geospatial data processing for users unfamiliar with command-line GIS programming. Its architecture is reused in the [AquaINFRA](https://aquainfra.eu/) project and the GeoFRESH solutions integrate with NFDI initiatives like NFDI4Biodiversity. Beyond data retrieval, GeoFRESH increases the visibility of freshwater geospatial workflows, addressing critical aspects like longitudinal network connectivity._

Spatiotemporal Earth system data on freshwaters are poorly organized at present and their rich potential for research and environmental management is barely exploited. Part of this deficiency relates to the spatial structure of freshwater habitats such as rivers, and currently being extended to lakes. Both require specialized workflows for an efficient Earth system data integration to address the longitudinal and lateral connectivity.

To address this challenge, we developed the [GeoFRESH online platform](https://geofresh.org/) within a [pilot project of the NFDI4Earth](https://zenodo.org/records/7888389). GeoFRESH provides the integration, processing, management and visualization of standardized spatiotemporal Earth system data related to freshwaters on the global scale. Users can (1) upload point data; (2) automatically assign the points to a new fine-scale hydrographical network; (3) map the points to the corresponding upstream catchment; (4) annotate the points with site-specific and upstream environmental information based on a suite of 104 layers capturing topography, climate, landcover and soil characteristics; (5) calculate network distances among points; and (6) download the compiled data for downstream analyses. 

The functionality of the platform is currently also extended also to lakes: users will have the possibility to identify river-lake intersections, compute upstream contributing areas of lakes and aggregate environmental information across lake catchments.

![GeoFRESH](img/geofresh_webpage.png "GeoFRESH.")


## Funding statement

- NFDI4Earth pilot project GeoFRESH ([@domisch_etal_2023])(2022-23)
- [NFDI4Earth pilot project Connecting rivers and lakes FAIRly](https://www.nfdi4earth.de/images/364_ConnectRiversLakes.pdf) (2023-24)
- [NFDI4Biodiversity Use Case hydrographr](https://glowabio.org/project/hydrographr/) 
- [Horizon Europe project](https://glowabio.org/project/hydrographr/) “Infrastructure for marine and inland water research” (AquaINFRA), 2023-27 


## Outcomes
  - The FAIR dataset is openly available:
    - [Dataset DOI](https://doi.org/10.18728/igb-fred-762.1)
    - Scientific publication: [@essd-14-4525-2022]
  - The FAIR software is openly available:
    - [Software repository](https://github.com/glowabio/hydrographr)
    - Scientific publication: [@Schuerz_etal_2023]
  - The Web-based solution is available: 
    - [Web platform](https://geofresh.org/) 
    - Scientific publication: [@domisch_etal_taylorfrancis_2024]
	
## References


