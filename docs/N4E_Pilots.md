---
name: NFDI4Earth Pilots

description: This article provides information about the NFDI4Earth Pilots and a list of all articles related to them. 

author: 
  - name: Kolja Nenoff
    orcidId: https://orcid.org/0009-0001-1773-0056

inLanguage: ENG

about:
  - internal nfdi4earth
  - resources


isPartOf: 
  - NFDI4Earth.md

additionalType: collection

subjectArea: 
  - dfgfo:34
  - unesco:mt2.35
  - unesco:concept522
  - unesco:concept7387
  - unesco:concept2214

keywords:
  - NFDI4Earth

audience: 
  - data user
  - data collector
  - data steward
  - data librarian
  - service provider

version: 1.0

license: CC-BY-4.0

---

# The NFDI4Earth Pilots

**Earth System Science Pilots** are one of NFDI4Earth’s most important measures to engage with the community. Pilot projects are small agile projects with a duration of around 12 months. Researchers from the Earth System Science community can submit their ideas to open calls. In those calls for pilots, we are looking for innovative solutions to challenges that earth system researchers face in their research data management. The proposed tools can target specific interoperability issues for and between particular Earth System Science subdomains and leverage existing technologies. We aim at supporting solutions that engage a broad user community and can potentially be transferred to other domains. The most promising ideas are funded for a year and are to be integrated into the NFDI4Earth after their completion.

## Calls
A first call has been launched in 2020 and the first cohort of pilots ran from April 2022 until March 2023. A second call was launched in February 2023. In 2025 the third Cohort has started. 
## Projects

The projects of the **first cohort** and **second cohort** the stem from various disciplines from Geophysics, Landscape Ecology to Climate Modelling. They have worked on diverse data challenges like metadata standardization, the integration of analysis tools or the development of interactive exploration tools. Pilot results have reached different levels of maturity by the completion of the one-year project phase. They have produced tools from prototypes to ready-to-use solutions and triggered discussions and processes on standards and RDM practices in their domains. As expected by NFDI4Earth pilot projects, the vast majority of the projects has shown to be committed to FAIR and open software, data and platforms and developed their solutions accordingly.  Depending on the subdomain the pilots faced specific challenges, being of rather technological nature (software integration) or more based on the structure, connectedness and habits of the sub-community.

The **third cohort** projects aim to strengthen collaboration across multiple Earth System Science (ESS) subdisciplines and beyond. They provide ready-to-use implementations fostering interdisciplinary research.

The pilot projects provide valuable insights into the diverse Earth System Sciene sub-domains which support the overall strategy of NFDI4Earth to combine technological progress with community work.

  

Pilots have summarized their outputs and results in short articles that are gathered in this collection. For more detailed information on the individual projects please refer to the links provided in the articles.

