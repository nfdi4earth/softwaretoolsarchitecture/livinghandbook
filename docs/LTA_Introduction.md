---
name: Introduction to digital long-term preservation

description: This article serves as an introduction to the topic of digital long-term preservation of ESS-data. It gives an overview about the basic principles of digital long-term preservation as well as associated challenges and strategies.

author: 
  - name: Peter Valena
    orcidId: https://orcid.org/0000-0003-2673-2974
  - name: Markus Schmalzl
    orcidId: https://orcid.org/0009-0005-7547-2699

inLanguage: ENG

about:
  - internal nfdi4earth
  - storage
  - resources


isPartOf: 
  - Collection_Archive.md

additionalType: article

subjectArea: 
  - unesco:concept492
  - unesco:concept2780
  - unesco:mt2.35

keywords:
  - NFDI4Earth
  - LTA
  - long-term archiving
  - digital long-term preservation

audience: 
  - data collector
  - policy maker
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 0.1

---

# Introduction to digital long-term preservation

Digital long-term preservation can be defined as "a series of managed activities necessary to ensure continued access to digital materials for as long as necessary" [@DigitalPreservationCoalition.2015{Glossary}]. In accordance with the "Guidelines for Safeguarding Good Research Practice" from the DFG, a retention period for research data of ten years has been established in many scientific disciplines [@deutsche_forschungsgemeinschaft_2022_6472827]{Guideline 17}. However, research data and especially ESS data can be and often is of great relevance long beyond the ten-year retention period and needs to be preserved accordingly. 

Digital long-term archiving aims at ensuring the (re)usability and comprehensibility of digital data over an undefined period of time through securing the

* **Authenticity**: "The digital material is what it purports to be. In the case of electronic records, it refers to the trustworthiness of the electronic record as a record. In the case of "born digital" and digitized materials, it refers to the fact that whatever is being cited is the same as it was when it was first created unless the accompanying metadata indicates any changes. Confidence in the authenticity of digital materials over time is particularly crucial owing to the ease with which alterations can be made." [@DigitalPreservationCoalition.2015{Glossary}]
* **Integrity**: "Internal consistency or lack of corruption of digital objects. Integrity can be compromised by hardware errors even when digital objects are not touched, or by software or human errors when they are transferred or processed." [@coretrustseal_standards_and_certificatio_2022_7051125]
* **Access**: "Access is assumed to mean continued, ongoing usability of a digital resource, retaining all qualities of authenticity, accuracy and functionality deemed to be essential for the purposes the digital material was created and/or acquired for." [@DigitalPreservationCoalition.2015{Glossary}]
* **Interpretability**: The ability to understand data on a semantic level. Long-term interpretability of digital data requires sufficient descriptive metadata on a contextual and content-related level about the digital object. 

of digital data over time. Regarding the conceptual structure of a digital archive the Open Archival Information System (OAIS) Reference Model [@TheConsultiveCommitteeforSpaceDataSystems.2012][^6] has established itself as a de facto standard in the archival community.

[^6]: This text is identical to the standard ISO 14721:2012.

## Challenges

* **Digital obsolescence**: Digital data requires a software environment to render and present its content. Continuous and rapid technological change in hard- and software environments, as well as file formats, poses a major challenge to ensuring a continuous comprehensible access to the content of digital data.
* **Documentation**: Digital data needs to be adequately documented and described with metadata on several levels in order to ensure its independent long-term comprehensibility. In addition to technical specifications (e.g. regarding file format, software, etc.), information on creation context and content, all steps undertaken over time for long-term preservation must also be documented.
* **Issue of scale and appraisal criteria**: The volume, diversity and complexity of newly created digital data, the diversity of file formats used, as well as the size of individual files, has been continuously growing, posing an issue of scale on several levels. Not all digital data needs to or even can be preserved indefinitely, thus criteria for the appraisal of digital data are required to determine its archival value and the intended timeframe of preservation.
* **Organizational issues**: Long-term archiving of digital data requires a continuous curation with corresponding financial and human resource allocation. Responsibilities within an organization (or in the case of collaborations with partner organizations) need to be clearly defined and assigned as well as preservation planning policies implemented, taking possible organizational changes into account.

## Strategies
**Bitstream preservation** is a strategy to preserve digital data (the exact bit string of a data file) as it was at the point of ingest. It is a basic requirement for digital long-term preservation, but it does not ensure access to the content of digital data over time. 

There are two main strategies for digital long-term preservation:

* The **migration strategy** ensures continuous access to the content and interpretability of digital data and its usability through migrating data to newer file formats, once the currently used is in danger of becoming obsolete. This requires ongoing monitoring regarding technological changes in order to determine the point of time actions have to be undertaken. In addition, documentary material on the original file format and systems as well as the definition of significant properties is necessary. Despite the possibility of information loss during the migration process, this strategy is considered so far as the most feasible one.
* The **emulation strategy** on the other hand keeps the original data as it is and aims at ensuring continuous access to it through (re)creating the original software environment on to newer hardware environments and operating systems. This approach has proven difficult to implement so far due to technical and legal challenges.

## Additional resources

* [Digital Preservation Handbook](https://www.dpconline.org/handbook)
* [Recommendations Consultation of the EOSC Association's Long Term Data Preservation Task Force](https://doi.org/10.5281/zenodo.10014698 )
* ["Long-term archiving" on forschungsdaten.info](https://forschungsdaten.info/themen/veroeffentlichen-und-archivieren/langzeitarchivierung/) (in German)
* [KOST (Koordinationsstelle für die dauerhafte Archivierung elektronischer Unterlagen)](https://kost-ceco.ch/cms/willkommen.html) (in German, French or Italian)
* [Pages on digital preservation of the National Archives and Records Administration (NARA)](https://www.archives.gov/preservation/digital-preservation)
* [Competence network long-term archiving nestor](http://langzeitarchivierung.de/)
* [UNESCO PERSIST Programme](https://unescopersist.org/)

## References

