---
name: Interoperability and Reusability of Geoscientific Laboratory Data

description: Description of the NFDI4Earth Pilot project. 

author: 
  - name: Matthias Halisch
    orcidId: https://orcid.org/0000-0002-7002-7191
  - name: Sven Nordsiek

inLanguage: ENG

about:
  - external nfdi4earth
  - dataset
  - data model
  - tools and techniques



isPartOf: 
  - N4E_Pilots.md
  - Collection_AccessReuse.md

additionalType: technical_note

subjectArea: 
  - dfgfo:3
  - unesco:concept501
  - unesco:concept1308
  - unesco:mt2.35

keywords:
  - laboratory
  - documentation
  - laboratory database

audience: 
  - data owner
  - data librarian
  - data curator

version: 1.0

identifier: https://doi.org/10.5281/zenodo.8063264


license: CC-BY-4.0

---

# Interoperability and Reusability of Geoscientific Laboratory Data: A conceptual model for a highly flexible database

A generally applicable database for geoscientific laboratory data is still missing. The variety of both methods and data and the complexity of laboratory studies impede the creation of an appropriate database. With this pilot project, we aimed at the development of a geoscientific laboratory database. We developed a model for a flexible and extensible database applicable to geoscientific laboratory data ([Media 1](#med1)). This model can work as a basis for the construction of geoscientific laboratory databases. Beside the technical implementation of the database model, the distribution and acceptance of the database model within the relevant communities will be future challenges. 

<a id="med1"></a>

![Simplified data model map: The flow of data (light green arrows) and the links between different sets of metadata (dark blue arrows) visualize the relationships that exist between metadata of a single laboratory study](img/Pilot_InteropReuseLabData_media1.png "Simplified data model map: The flow of data (light green arrows) and the links between different sets of metadata (dark blue arrows) visualize the relationships that exist between metadata of a single laboratory study.")

## Resources
* Roadmap / project report ([@halisch_2023])

## References
