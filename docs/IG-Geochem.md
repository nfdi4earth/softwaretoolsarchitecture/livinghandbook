---
name: Interest Group Metadata Standards for Geochemical Data
description: The NFDI4Earth Interest Group Metadata Standards for Geochemical Data

author: 
  - name:  Elfrun Lehmann
    orcidId: http://orcid.org/0000-0002-1943-3100

inLanguage: ENG

about:
  - external nfdi4earth
  - metadata


isPartOf: 
  - N4E_IG.md
  - Collection_AccessReuse.md
  - Collection_ObserveCollectCreateEvaluate.md
  - Collection_PlanDesign.md

additionalType: article

subjectArea: 
  - dfgfo:316-01
  - unesco:concept5039
  - unesco:concept3799

keywords:
  - Standards
  - interest group
  - NFDI4Earth

audience: 
  - data collector
  - data user
  - data depositor
  - data curator
  - data steward

version: 1.0

license: CC-BY-4.0

---

# Interest Group Metadata Standards for Geochemical Data

Funding organizations and scientific journals now emphasize following widely accepted metadata standards to align with FAIR data principles when storing data in repositories. The primary objective is to facilitate the reuse of existing data and the integration of data from diverse research areas to generate novel data products. In geochemistry and related fields, the diversity of data and metadata on geological samples and materials reflects the broad range of methods and data presentation. As a result, various standards of community-specific data description exist now.

The expansion of advanced analytical methods in geoscience research over the last two decades was not mirrored by a comparable evolution of metadata formats in data repositories necessary for effective data reuse. Editors of scientific journals in geochemistry had previously issued recommendations for metadata descriptions; however, a substantial number of these recommendations have not been met over the past two decades. Recent initiatives such as [OneGeochemistry](https://onegeochemistry.github.io/) and [NFDI4Earth](https://nfdi4earth.de/) provide an opportunity to assemble increased support for data reporting standards. Given the current increase in data and greater diversity in methods and data presentation, establishing mutual standards for different types of geochemical data is more urgent than ever. This Interest Group aims to channel efforts to improve these matters both, on a national and international level.

Sub-fields of geochemistry such as hydrogeochemistry, cosmochemistry, biogeochemistry, and other related scientific fields (e.g., archaeology/archaeometry, material sciences) use a range of geochemical data. Recognizing the diverse applications in which such data is used, the Interest Group proposes to initially focus on specific topics. Our discussions have identified possible areas that might be picked up by interested focus groups,

1. Developing ontologies and metadata for mass fractions of elements in rock samples and related inorganic material (meteorites, minerals, dust grains (and artifacts));
2. Developing ontologies and metadata for mass fractions of elements in water and suspended material (hydrogeochemistry);
3. Developing ontologies and metadata for isotopic data (mass-independent and mass-dependent isotopic variations, radiogenic isotopic systems);
4. Support tools that generate flexible metadata software, open software, automated work flows, pipelines;
5. Developing identifiers to link metadata to instruments, methods, and samples (i.e., PIDINST, IGSN);
6. Support implementation of machine-readability of metadata concepts (providing APIs for testing new templates, etc.);
7. Support of focus areas 1-6: Development of FAIR data concepts and machine-readability of geochemical data.
