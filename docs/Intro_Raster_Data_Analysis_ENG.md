---
name: Introduction to handling raster time series in Julia, Python and R

description: Managing raster data with scripts is essential for Earth System Science data analysis. Here, we provide tutorials for data analysis in Python, Julia, and R to demonstrate similarities and differences in these ecosystems.

author: 
  - name: Felix Cremer
    orcidId: https://orcid.org/0000-0001-8659-4361
  - name: Hannes Dröse

  - name: Arne Osterthun
    orcidId: https://orcid.org/0000-0001-6455-9119
  - name: Yomna Eid
    orcidId: https://orcid.org/0000-0001-7713-9682
  - name: Fabian Gans
    orcidId: https://orcid.org/0000-0001-9614-0435
  - name: Sibylle Hassler
    orcidId: https://orcid.org/0000-0001-5411-8491
  - name: Edzer Pebesma
    orcidId: https://orcid.org/0000-0001-8049-7069

inLanguage: ENG

about:
  - internal nfdi4earth
  - tools and techniques
  - data model
  - workflow
  - resources

isPartOf: 
  - Collection_ProcessAnalyse.md

additionalType: article

subjectArea: 
  - unesco:concept3611
  - unesco:concept6007 
  - dfgfo:409%0A

keywords:
  - raster time series
  - Julia
  - Python
  - R

version: 0.1

license: CC-BY-4.0

---

# Introduction to handling raster time series in Julia, Python and R

This tutorial will showcase how to work with raster data efficiently.
The analysis will be shown in Julia, Python and R to showcase the
similarities and differences in handling raster data in these
ecosystems.

In this tutorial we are going to use the [COSMO REA reanalyis near surface air temperature data](https://www.wdc-climate.de/ui/entry?acronym=CR6_EU6). 
The data is an reanalysis dataset on a 6km by 6km grid.
We are going to use the monthly average values,
but the data is also avialable with an hourly or daily temporal resolution.
The data was produced in the GRIB format but was converted to NetCDF files in the [NFDI4Earth Pilot](https://onestop4all.nfdi4earth.de/result/znd-10.5281-zenodo.7360808). 

[Raster Data Analysis in Julia](Intro_Raster_Data_Analysis_ENG_Julia.md)

[Raster Data Analysis in Python](Intro_Raster_Data_Analysis_ENG_Python.md)

[Raster Data Analysis in R](Intro_Raster_Data_Analysis_ENG_R.md)

![Example for output data](img/Intro_Raster_Data_Analysis_ENG_Julia_files/figure-markdown_strict/cell-4-output-1.png)