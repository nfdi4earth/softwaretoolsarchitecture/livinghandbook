---
name: Data from governmental agencies (listed in geoportal.de)
description: A list of all data providers on a federal level which are present in the geoportal.de

author: 
  - name: Tim Schürmann
    orcidId: https://orcid.org/0009-0009-4778-5902

inLanguage: ENG


about:
  - internal nfdi4earth
  - dataset
  - resources

additionalType: article


isPartOf: 
#  - index.md

#subjectArea: 


keywords:
  - Governmental data

audience: 
  - data user

version: 0.1

license: CC-BY-4.0

---

# Data from governmental agencies (listed in geoportal.de)

The [geoportal.de]( https://geoportal.de/ ) is a governmental cooperative resource which lists an abundance of data entries from the various institutions. It aims to provide free access to geoinformation from different parts of public administration. Among them are the governmental agencies on a federal level which cover a broad range of topics.

In the portal, further filters can be applied to narrow down the search. Available filters include but are not limited to type of entry, provider, INSPIRE relevancy, open data, language. A viewer is also included if applicable.

Data from the following federal agencies can be found in the [geoportal.de]( https://geoportal.de/ )  (names and topics listed in German):



|     Name    |     Abbreviation    |     Topic    |     URL    |
|---|---|---|---|
|     Bundesamt für Bauwesen und Raumordnung    |     BBR    |     Bauwesen    |     https://www.bbr.bund.de/BBR/DE/Home/home_node.html    |
|     Bundesamt für Infrastruktur, Umweltschutz und   Dienstleistungen der Bundeswehr    |     BAIUDBW    |     Koordinierung Bundeswehr    |     https://www.bundeswehr.de/de/organisation/infrastruktur-umweltschutz-und-dienstleistungen/organisation-iud/baiudbw    |
|     Bundesamt für Kartographie und Geodäsie    |     BKG    |     Geodäsie & Geoinformationen    |     https://www.bkg.bund.de/DE/Home/home.html    |
|     Bundesamt für Naturschutz    |     BfN    |     Naturschutz    |     https://www.bfn.de/    |
|     Bundesamt für Seeschifffahrt und Hydrographie    |     BSH    |     Schiffahrt & Meeresumwelt    |     https://www.bsh.de/DE/Home/home_node.html    |
|     Bundesamt für Strahlenschutz    |     BfS    |     Strahlenschutz    |     https://www.bfs.de/DE/home/home_node.html    |
|     Bundesamt für Verbraucherschutz und   Lebensmittelsicherheit    |     BVL    |     Lebensmittelsicherheit    |     https://www.bvl.bund.de/DE/Home/home_node.html    |
|     Bundesanstalt für Geowissenschaften und Rohstoffe    |     BGR    |     Geowissenschaften & Rohstoffe    |     https://www.bgr.bund.de/DE/Home/homepage_node.html    |
|     Bundesanstalt für Gewässerkunde    |     BfG    |     Gewässersysteme    |     https://www.bafg.de/DE/0_Home/home_node.html    |
|     Bundesanstalt für Landwirtschaft und Ernährung    |     BLE    |     Landwirtschaft & Lebensmittel    |     https://www.ble.de/DE/Startseite/startseite_node.html    |
|     Bundesanstalt für Straßenwesen    |     BASt    |     Straßenwesen    |     https://www.bast.de/DE/Home/home_node.html    |
|     Bundesanstalt für Wasserbau    |     BAW    |     Wasserstraßen & Schifffahrt    |     https://www.baw.de/de/home/home.html    |
|     Bundesgesellschaft für Endlagerung mbH    |     BGE    |     Endlagerung    |     https://www.bge.de/de/    |
|     Bundesministerium der Finanzen    |     BMF    |     Finanzen    |     https://www.bundesfinanzministerium.de/Web/DE/Home/home.html    |
|     Bundesministerium für Digitales und Verkehr    |     BMDV    |     Mobilität & Digitale Infrastruktur    |     https://bmdv.bund.de/DE/Home/home.html    |
|     Bundesnetzagentur    |     BnetzA    |     Telekommunikation    |     https://www.bundesnetzagentur.de/DE/Home/home_node.html    |
|     Deutsche Flugsicherung    |     DFS    |     Flugsicherung    |     https://www.dfs.de/homepage/de/    |
|     Deutscher Wetterdienst    |     DWD    |     Klima & Wetter    |     https://www.dwd.de/DE/Home/home_node.html    |
|     Eisenbahn-Bundesamt    |     EBA    |     Schieneninfrastruktur    |     https://www.eba.bund.de/DE/home_node.html    |
|     Informationstechnikzentrum Bund    |     ITZBund    |     IT-Dienstleistungen    |     https://www.itzbund.de/DE/home/home_node.html    |
|     Julius Kühn-Institut    |     JKI    |     Pflanzenschutz    |     https://www.julius-kuehn.de/    |
|     Statistisches Bundesamt    |     Destatis    |     Statistiken    |     https://www.destatis.de/DE/Home/_inhalt.html    |
|     Umweltbundesamt    |     UBA    |     Umweltschutz    |     https://www.umweltbundesamt.de/    |
|     Wasser- und Schifffahrtsverwaltung    |     WSV    |     Wasserstraßen & Schifffahrt    |     https://www.gdws.wsv.bund.de/DE/startseite/startseite_node.html    |