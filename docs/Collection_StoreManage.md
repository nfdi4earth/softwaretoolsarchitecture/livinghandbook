---
name: Store and Manage

description: Collection of articles about research data management topics related to store, organise, and manage research data. 

author: 
  - name: NFDI4Earth Editorial Board
 #   orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

additionalType: collection

subjectArea: 
  - dfgfo:34
  - unesco:mt2.35
  - unesco:concept7396

keywords:
  - research data
  - storage
  - preservation
  - management

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Organise and Store

## What is the store and organise phase?

After the data have been acquired in the 2nd step [Observe, Collect, Create and Evaluate](Collection_ObserveCollectCreateEvaluate.md), these are transferred in this 3rd step from an analytical instrument, a sensor, or a satellite into an appropriate and sustainable storage. 

**Organise**: A clear labelling policy of data files, file directory hierarchy must be defined. Various types of collected geospatial, geochemical, and/or environmental raw data need to be systematically structured, classified and arranged. For example, collected rock samples need to be labeled with metadata that include information such as location (e.g., latitude and longitude), time of sampling, depth of extraction, or geological formation (e.g., sandstone from the Morrison Formation), data from satellites need to be organised by temporal and spatial resolution to monitor deforestation or glacier movements. 

**Store**: The acquired data should be stored in their raw format to allow checking the originally obtained data in case questions regarding the data processing arise along the subsequent data lifecycle. Electronic Lab Notebooks (ELN) are ideal storage locations for raw data. Templates with metadata fields and vocabulary suggestions can be created in an ELN to ease the process of storing data in interoperable formats from the beginning. At least one copy should be stored on a second hard-drive. If required, a storage policy might be enforced, e.g., General Data Protection Regulation (GDPR), data retention policy, and data access control policy. 

## Why is the organise and store phase important?

A sensible storage strategy is vital to not lose freshly acquired data and make it easily findable by others. Geoscientific research generates data from multiple sources like satellite images, seismic surveys, geological mapping, or ship expeditions. Effective data organization and storage are essential for collaborative projects like climate studies or earthquake monitoring, which rely on shared data across disciplines. Certain research approaches, such as 3D modelling and resource management, rely on data that has been collected, organised and stored in structured formats.


## What will be offered in this article collection?

Options on how local storage could be organised, a guide for selecting the appropriate Earth System Science system (ESS) repository, and how to store data in such, as well as regulatory compliance requirements related to data storage and organization, e.g., data protection laws, intellectual property rights, or data sharing agreements, not to forget advise on choosing a sensible licence are provided.

We encourage contributions from the community to help refine and enhance this collection and provide general or domain specific strategies for more effective and collaborative data solutions.  

For more details on how to contribute, please refer to our [template article](example_ENG.md).


<!-- The following is missing, but should be addressed:
Articles address regulatory compliance requirements related to data storage and management, such as data protection laws, intellectual property rights, and data sharing agreements.
How do you ensure data quality? This is covered in articles concerning the topics data validation, error detection, and data cleaning processes. -->
