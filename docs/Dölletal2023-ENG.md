---
name: Sustainable research software for high-quality computational research in the Earth System Sciences

description: These 15 recommendation to support sustainable research software in the Earth System Sciences were developed during a DFG-funded round-table discussion. They were complemented by best practice examples for the publication. 

author: 
  - name: Petra Döll
    orcidId: https://orcid.org/0000-0003-2238-4546
  - name: Monika Sester
  - name: Udo Feuerhake
  - name: Holger Frahm
  - name: Bernadette Fritzsch
  - name: Dominik C. Hezel
    orcidId: https://orcid.org/0000-0002-5059-2281
  - name: Boris Kaus
  - name: Olaf Kolditz
  - name: Jan Linxweiler
  - name: Hannes Müller Schmied
  - name: Emmanuel Nyenah
  - name: Bernjamin Risse
  - name: Ulrich Schielein
  - name: Tobias Schlauch
  - name: Thilo Streck
  - name: Gijs van den Oord

inLanguage: ENG

about:
  - external nfdi4earth
  - tools and techniques
  - workflow

additionalType: recommended_article

subjectArea: 
  - dfgfo:34
  - unesco:concept7775
  - unesco:mt2.35
  - unesco:concept6081
  - unesco:concept111
  - unesco:concept8017
  - unesco:concept17126
  - unesco:concept2565
  - unesco:concept10993
  - unesco:concept14843
  - unesco:concept112
  - unesco:concept4566

keywords:
  - research software
  - sustainability
  - DFG
  - white paper
  - recommendations
  - round table discussion

audience: 
  - research software engineer
  - data librarian
  - policy maker

identifier: https://doi.org/10.23689/fidgeo-5805

version: 1.0

license: CC-BY-4.0

---

# Sustainable research software for high-quality computational research in the Earth System Sciences: Recommendations for universities, funders and the scientific community in Germany

Döll, Petra; Sester, Monika; Feuerhake, Udo; Frahm, Holger; Fritzsch, Bernadette; Hezel, Dominik C.; Kaus, Boris; Kolditz, Olaf; Linxweiler, Jan; Müller Schmied, Hannes; Nyenah, Emmanuel; Risse, Bernjamin; Schielein, Ulrich; Schlauch, Tobias; Streck, Thilo; van den Oord, Gijs, 2023: Sustainable research software for high-quality computational research in the Earth System Sciences: Recommendations for universities, funders and the scientific community in Germany. FIG GEO-LEO e-docs, <https://doi.org/10.23689/fidgeo-5805>

New knowledge in the Earth System Sciences is increasingly produced by computational research. This includes the simulation of earth system processes (e.g., climate models), the design, processing and analysis of earth observation and experiments, and integrative analysis (e.g. of large amounts of data by machine learning). Efficient and reproducible high-quality computational research requires sustainable research software. Therefore, its development should be supported by universities, funders and the scientific community. Fifteen recommendations for how such support should be designed were developed in a round-table discussion with researchers, research software engineers, the Chief Information officers of two universities, and a representative of the German Research Foundation (DFG) in June 2023. Complemented by best practice examples, the recommendations were published in this white paper. 

Read the full article: [@doell_etal2023]

## References