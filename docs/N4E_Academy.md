---
name: NFDI4Earth Academy

description: The NFDI4Earth Academy is a network of early career scientists bridging Earth System and Data Sciences beyond institutional borders. Geo.X, Geoverbund ABC/J, and DAM offer an open science and learning environment covering specialized training courses and collaborations within the NFDI4Earth. 

author: 
  - name: Hildegard Gödde
  - name: Effi-Laura Drews
    orcidId: https://orcid.org/0009-0007-8878-8080
  - name: Jonas Kuppler
    orcidId: https://orcid.org/0000-0003-4409-9367
  - name: Konstantin Ntageretzis
  - name: Kristin Sauerland
    orcidId: https://orcid.org/0000-0002-0003-1259
  - name: Gauvain Wiemer
    orcidId: https://orcid.org/0000-0003-4420-5318

inLanguage: ENG

about:
  - internal nfdi4earth
  - resources


isPartOf: 
  - NFDI4Earth.md

additionalType: article

subjectArea: 
  - unesco:concept157
  - unesco:concept12756
  - unesco:mt2.10
  - unesco:concept3247
  - unesco:concept4603

keywords:
  - early career scientists
  - think tank
  - interdisciplinary research
  - data science
  - joint training

audience: 
  - data collector
  - data owner
  - data user
  - policy maker

version: 1.0

license: CC-BY-4.0

---

# NFDI4Earth Academy - Your training network to bridge Earth System and Data Science

## Overview 
As an Academy fellow, you will benefit from tailored training courses (e.g., summer schools, workshops, and seminars designed by peers and organized by us), networking beyond institutional boundaries and events to foster interdisciplinary research & projects. Further, fellows and their projects will have a high visibility in and support from the NFDI4Earth community. They will have the opportunity to participate in a broad range of external training offers, such as the Helmholtz Academy for Information and Data Science (HIDA) or DataTrain (Uni Bremen Research Alliance). With the Academy, we offer an open science and learning environment.

Academy fellows can advance their current research projects by exploring and integrating new methods and connecting with like-minded scientists in an agile, bottom-up, and peer-mentored community. Using the established structures of our three research networks ([Geo.X](https://www.geo-x.net/), [Geoverbund ABC/J](https://www.geoverbund-abcj.de/) and [Deutsche Allianz Meeresforschung](https://www.allianz-meeresforschung.de/en)), our program combines various virtual, hybrid and in-person events. By taking advantage of on-site resources, we can provide support for essential in-person events and meetings for networking. Thus, we explicitly invite all early career scientists in the German Earth System and Data Sciences community to join us.

Our first Cohort started in November 2022 and our second Cohort in June 2024.

More information can be found on our [homepage](https://www.nfdi4earth.de/2participate/academy).


## Academy Program
The main objective of the two-year Academy program is to foster networking, training, and collaborative research of doctoral and postdoctoral researchers bridging Earth System and Data Science. Our program deviates from traditional graduate school curricula in two ways: First, we focus on peer-mentoring rather than teacher-based learning. Secondly, we provide a core event structure (three per year), of which the  content will be determined by the fellows in an agile bottom-up process. This way, we ensure that the events match the fellows’ training needs. In addition to the core events, fellows will have the opportunity to organize and host additional workshops, meetings, and other events - with our support. We also explicitly focus on linking fellows to existing training opportunities that are needed or helpful. Furthermore, we ensure  strong integration and linkage to other relevant NFDI4Earth developments.

### Core events (Year 1)
The first year of the Academy will focus on research data management and Data Science skills and starts with a two-day Kick-off retreat (in-person, depending on the pandemic situation). During this retreat, fellows will get to know each other, their research projects, and their interests in Data Sciences. Further, together with the coordinators, they will develop a code of conduct and a roadmap for the Academy to create a motivational and inspiring working environment. In the span of two months during winter, there will be a bi-weekly virtual workshop series focusing on research data management, reproducibility, and Open Science. The workshops will be designed to be engaging with fruitful discussion and active participation, during which fellows describe their current practices and a pathway forward. For training in Data Science, we will host up to three in-person/hybrid spring or summer schools. The fellows determine the thematic focus of the events to fulfill their specific learning and training opportunities.

### Core events (Year 2)
In the second year, the core program focuses on collaborative and networking events. We will host “Think tank” events designed to brainstorm and develop synergies between fellows projects, potential collaborations, and new ideas. During Hackathons (up to three in year 2) fellows can work on new ideas and/or their own projects, depending on the fellows preferences. For these events, early career scientists from other NFDI consortia, such as NFDI4Biodiversity or KonsortSWD, will be invited to join and explore cross-link topics.   The final core event will be a joint Hackweek) of the 1st and 2nd cohort to ensure knowledge transfer between both. By combining elements of schools and Hackathons, these meetings foster community building, education in data science methods, and immersion in collaborative project work.

### Additional events & possibilities
Throughout the program,  fellows can join short (half or full day) informal meetings every three to four months to present and discuss their project progress with peers. Further, all fellows are invited to join NFDI4Earth events, such as plenary meetings, to present their research and be up-to-date on the developments of the NFDI4Earth. Furthermore, we provide access to a broad range of external training opportunities, such as the Helmholtz Academy for Information and Data Science (HIDA) or DataTrain (Uni Bremen Research Alliance). 
