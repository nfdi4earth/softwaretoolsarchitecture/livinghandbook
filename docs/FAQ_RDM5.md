---
name: Why does publishing data in FAIR repositories take so long compared to Zenodo?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth
  - publication
  - workflow


isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - unesco:concept3810

keywords:
  - data publication
  - Zenodo
  - repository

audience: 
  - data depositor

version: 1.0

license: CC-BY-4.0

---

# Why does publishing data in FAIR repositories take so long compared to Zenodo?

Publishing data in FAIR repositories take longer compared to Zenodo due to the different level of curation applied by the provider to the data. 

[Zenodo](https://zenodo.org/) is an open dissemination research data repository for the preservation and making available of research, educational and informational content. The uploader is exclusively responsible for the content and FAIRness of the primary data that they upload to Zenodo. In the context of FAIR, the **F**indability and the **A**ccessability are provided by Zenodo. 

Publishing in FAIR repositories such as for example [PANGAEA](https://www.pangaea.de/) or [WDCC](https://www.wdc-climate.de/ui/) include a managed high-level curation process (e.g., [Lafferty et al. 2020](https://doi.org/10.7710/2162-3309.2347)) by the Research Data Management service of the repository, to ensure the quality of metadata and primary data in the FAIR data publication process and enables therefore the **I**nteroperability and **R**eusability of the data.