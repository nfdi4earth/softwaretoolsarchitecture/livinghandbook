---
name: NFDI4Earth

description: Collection of articles related to the NFDI4Earth, its services and outcomes. 

author: 
  - name: NFDI4Earth Editorial Board
 #   orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

additionalType: collection

subjectArea: 
  - dfgfo:34
  - unesco:mt2.35
  - unesco:concept522

keywords:
  - NFDI4Earth
  - NFDI
  - Earth Sciences

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Collection: About the NFDI4Earth

![NFDI4Earth logo](img/NFDI4Earth.png "NFDI4Earth logo.")

## Basic information about NFDI4Earth

The NFDI4Earth is the [NFDI](http://www.nfdi.de) consortium for the Earth System Sciences. The National Research Data Management Infrastructure (NFDI) is an initiative in Germany aimed at establishing a comprehensive research data management infrastructure across all science domains. The NFDI contributes to the European Open Science Cloud (EOSC) by promoting FAIR (Findable, Accessible, Interoperable, Reusable) data principles. It aims to drive a cultural change in Earth System Sciences, enhancing data management practices and fostering collaboration and innovation through improved data accessibility and usability.  

66 partners are currently working in the NFDI4Earth to provide a range of service that are all accessible through the NFDI4Earth OneStop4All website.
 

## The focus of NFDI4Earth

The NFDI4Earth connects stakeholders, including universities, data services, research organizations, government agencies, with a planned extension to industries, manufacturers, publishers or funding agencies to make Earth System Science data **F**indable, **A**ccessible, **I**nteroperable, and **R**eusable – i.e., FAIR.


## What is offered in this article collection?
With its OneStop4All, the NFDI4Earth offers a user-friendly entry point that provides useful services, including a Living Handbook, User Support Network, educational resources, and an Academy for early career scientists. The Knowledge Hub serves as the technical backend. Participative opportunities are offered through pilot projects, incubator projects, and interest groups. Together we are working on a cultural shift to make all Earth System Science data FAIR, and establish shared data best practices that serve this goal.

This is the NFDI4Earth collection of Living Handbook articles centered on research data management (RDM) within Earth System Sciences (ESS). It aims to provide researchers with essential resources for effective data handling and innovation in ESS. We invite your insights and experiences on the topic of NFDI4Earth. For more details on how to contribute, please refer to our [author guidelines](LHB_author-guidelines_ENG.md).