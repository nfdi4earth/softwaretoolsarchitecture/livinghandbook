---
name: Workshop “How to create publishable netCDF data?”

description: In July 2023, NFDI4Earth members held a workshop, where the participants were trained on how to use and publish Earth System Science data in the FAIR data format netCDF.

author: 
  - name: Stefan Kern
    orcidId: https://orcid.org/0000-0001-7281-3746
  - name: Remon Sadikni
    orcidId: https://orcid.org/0009-0009-9872-021X 
  - name: Christopher Purr
    orcidId: https://orcid.org/0000-0002-9376-432X 

inLanguage: ENG



about:
  - external nfdi4earth
  - resources


isPartOf: 
  - LHB_ENG.md
  - Collection1.md
  - Collection2.md


additionalType: user_guide

subjectArea: 
  - dfgfo:313-02
  - unesco:concept160
  - unesco:mt2.35

keywords:
  - NetCDF
  - Measure 1.3
  - workshop
  - success story
  - FAIR
  - education

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data steward
  - data advocate
  - data provider

identifier: 

version: 0.1

license: CC-BY-4.0

---

# Workshop “How to create publishable netCDF data? 


_**Contributions to the community**: This workshop contributed to the promotion of FAIR data by educating students and early career scientists on the benefits of using standardised data file formats, in this case netCDF. The educational material from the workhop is also available online enabling a wider audience to benefit from this information._


In July 2023, NFDI4Earth members held a workshop in Hamburg, where students and scientists were trained on how to use and publish Earth System Science data in a FAIR data format, in this case netCDF. The workshop successfully educated the participants on how to create and manipulate netCFD files, thus promoting the use of a FAIR conform data format. The [learning material](https://edutrain.nfdi4earth.de/courses/course-v1:NFDI4Earth+20230502CP+self-paced/about) for the workshop is also available online, allowing for widespread education on this topic. 


![Workshop flyer](img/netCDF_workshop_flyer.png "Workshop flyer")`

Workshop flyer 

In this workshop, NFDI4Earth members supported local personnel (at the Integrated Climate Data Center (ICDC, CEN), University of Hamburg)  in planning and carrying out a workshop related to training students and scientists at various career levels in using/publishing ESS data in a [FAIR](https://onestop4all.nfdi4earth.de/result/f44d01c3b2ab74dbb12a) conform data format, e.g. netCDF using the [CF conventions](http://cfconventions.org/).

Publishing research results and data products in Earth System Sciences (ESS) in a [FAIR](https://onestop4all.nfdi4earth.de/result/f44d01c3b2ab74dbb12a) way is mandatory in many projects and forms one of the core topics within the NFDI4Earth. In ESS, such data products are often published and shared in the self-explanatory netCDF file format. This format enables across platform usage of the data published, and enables to store a content rich amount of metadata alongside the actual data, giving important information to make the data product [FAIR](https://onestop4all.nfdi4earth.de/result/f44d01c3b2ab74dbb12a). 

Publishing data in netCDF file format requires following a number of guidelines. These guidelines are, however, often not known well enough by those who want to publish their research results. One important element to improve this situation therefore is to teach researchers how publishing of data in netCDF file format works (and what kind of best practices should be adhered to).

Within the NDFI4Earth project, especially Measure 1.3, to develop such teaching activities is among the core aims. Following these aims, a first hands-on workshop was held in July 2023 entitled: “How to create publishable netCDF data?”. The respective educational material is published 
[here](https://edutrain.nfdi4earth.de/courses/course-v1:NFDI4Earth+20230502CP+self-paced/about); everybody can enroll. 


