---
name: Suggest your learning material 

description: NFDI4Earth aims to share information on high-quality educational materials that support FAIR and open research data management in the Earth System Sciences. Here, we provide an overview of steps to suggest your material.

author:

  - name: Farzaneh Sadeghi
    orcidId: https://orcid.org/0000-0002-7920-4289

inLanguage: ENG

about:
  - internal nfdi4earth  

isPartOf:
  - Collection_ObserveCollectCreateEvaluate.md

additionalType: article

subjectArea:

keywords:
  - metadata
  - educational material
  - training resource
  - learning resource
  - open educational resource
  - OER

version: 0.1

license: CC-BY-4.0
---

# Suggest your learning material 

NFDI4Earth is a community-driven initiative aiming to provide researchers with FAIR, coherent, and open access to relevant Earth System resources, such as services. Through its Education and Training team (EduTrain), the project is developing a wide range of training materials and services specifically tailored to the [needs of the ESS community](https://doi.org/10.5281/zenodo.7940195). 

The [NFDI4Earth Educational Portal](https://edutrain.nfdi4earth.de/) is a virtual learning environment where modular courses curated by EduTrain are published. EduTrain is committed to developing and publishing high-quality educational materials tailored to the needs of the ESS community under open licenses to promote open and reproducible research methods.

The [NFDI4Earth Catalog of Educational Resources](https://onestop4all.nfdi4earth.de/search?resourcetype=Learning+Resource&sort=) is designed to facilitate the easy discovery of high-quality educational content available online. It provides detailed metadata and direct links to educational resources in ESS, serving as a centralized search tool for educators and learners.

Sharing your expertise with the NFDI4Earth community is a powerful way to extend your influence and contribute to advancing Earth system sciences. We will credit and acknowledge your work, ensuring that your contributions are recognized and accessible to a broader audience of educators and learners. To propose your educational content, please follow these steps:

## 1. Select the educational service that suits your needs the best

* __NFDI4Earth Educational Portal__: you want your content to be published or republished in NFDI4Earth educational portal
* __NFDI4Earth Educational Catalog__: you already have published your content and want to make it visible to NFDI4Earth users 

## 2. Tell us more about your contribution by preparing the following information

* Title
* Subject area(s)
* Course description (preferably including learning objectives)
* Author(s) of the course
* Licence (for publishing courses on the NFDI4Earth educational portal, only content with open licenses is accepted)
* Prerequisites (if any)
* Difficulty level (beginner, intermediate, advanced)

## 3. Contact the NFDI4Earth EduTrain Team

* Contact the EduTrain team at [farzaneh.sadeghi@hs-bochum.de](mailto:farzaneh.sadeghi@hs-bochum.de) to share the above information along with your educational materials or links to them. We will review and prepare the materials for publication, and you will be granted access as the course owner.

![NFDI4Earth](img/NFDI4Earth.jpg)

<!--

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences).

-->