---
name: 'Lexcube: Interactive Visualization and Exploration of High-Resolution Remote Sensing and Socioeconomic Data Sets'

description: Description of the NFDI4Earth Pilot project. 

author: 
  - name: Maximilian Söchting
    orcidId: https://orcid.org/0000-0002-8761-2821
  - name: David Montero
    orcidId: https://orcid.org/0000-0002-9010-3286
  - name: Miguel Mahecha
    orcidId: https://orcid.org/0000-0003-3031-613X

inLanguage: ENG

about:
  - external nfdi4earth
  - dataset
  - tools and techniques

isPartOf: 
  - N4E_Pilots.md
  - Collection_AccessReuse.md
  - Collection_SharePublish.md

additionalType: technical_note

subjectArea: 
  - dfgfo:315-02
  - unesco:concept17105
  - unesco:concept1557
  - unesco:concept2253
  - unesco:concept5437

keywords:
  - data cube
  - lexcube
  - gridded data
  - visualization

audience: 
  - data librarian
  - data user

version: 1.0

identifier: https://doi.org/10.5281/zenodo.7822389


license: CC-BY-4.0

---

# Lexcube: Interactive Visualization and Exploration of High-Resolution Remote Sensing and Socioeconomic Data Sets

A multitude of data streams, such as gridded climate data and biophysical parameters of land and water bodies, are utilized for monitoring various subsystems of the Earth in space and time. As sensor technology advances, the spatial and temporal resolution of these data sets continue to increase, posing a challenge in obtaining global and local insights from the data. In order to explore these large socioeconomic and multivariate remote sensing data cubes effectively, we explored different visualization approaches and extended our existing client-server software architecture Lexcube ([Media 1](#med1)) for interactive exploration and visualization of data cubes. As part of this NFDI4Earth pilot project, Lexcube has been successfully released as open-access at [lexcube.org](https://www.lexcube.org/). Furthermore, new features such as animations and axis labels have been developed and a new data set from the European Centre for Medium-Range Weather Forecasts (ECMWF) has been added since the public release. [Lexcube.org](https://www.lexcube.org/) has seen over 2800 users and 163,000 API requests since its public release in May 2022, offering interactive visualizations for open-access to a widely interested audience. In the future, Lexcube will be extended with additional functionality, including integration into data science workflows like Jupyter notebooks, and the ability to visualize multiple cubes simultaneously. Additionally, we are aiming to open-source Lexcube in 2023, which will enable a broader audience to benefit from Lexcube's capabilities and potentially contribute to its future development.

<a id="med1"></a>

![A screenshot of the interface of the interactive Lexcube
visualization at lexcube.org](img/Pilot_Lexcube_fig1.png "A screenshot of the interface of the interactive Lexcube
visualization.")

## Resources
* [Project website and interactive visualization](https://www.lexcube.org/)
* Pilot report ([@maximilian_sochting_2023])

## References
