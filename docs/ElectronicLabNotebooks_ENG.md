---
name: Electronic Lab Notebooks (ELN)
# The title of the article.

description: this is an introduction about electronic lab notebooks.
# A brief summary of the article, max. 300 characters incl. spaces.

author: 
  - name: Dominik Hezel
    orcidId: https://orcid.org/0000-0002-5059-2281

inLanguage: ENG

about:
  - internal nfdi4earth
  - electronic lab and field notebooks


isPartOf: 
  - LHB_ENG.md
  - Collection_ObserveCollectCreateEvaluate.md
  - Collection_StoreManage.md

additionalType: article


subjectArea: 
  - dfgfo:408-01
  - unesco:concept520
  - unesco:concept6019

# Electronic Semiconductors, Components, Circuits, Systems; Computer applications; Computer networks


keywords:
  - Electronic Lab Notebooks
  - keyword2
# A list of terms to describe the article's topic more precisely. 

audience: 
  - data collector
  - policy maker
  - data owner
  - data provider


identifier: 
# If the article has already a unique identifier such as an DOI, provide it here as URL (e.g., https://doi.org/10.1001/12345). 

version: 0.1
# The version of the article; will be updated by the editor. 

license: CC-BY-4.0
# The license of the article. Must not be altered. 

---

# Electronic Lab Notebooks (ELN)

## What it is?
An electronic lab notebook (ELN) is a software tool to document and store all steps performed on a sample during any kind of lab work in a local or cloud-based long-term archive. As such, ELNs are often domain specific to satisfy the typical needs of e.g., an experimental marine lab, which are different from a medical lab, or a field ›lab‹ when conducting an opinion poll in the humanities.

The ELN is used in the first steps of the data life cycle, i.e., it is sensible to use an ELN from the very first planning stage, before data are transferred to a public data repository or database.


## Purpose
An ELN requires and supports a user to document all steps performed during lab work. The ELN stores all data in a standardised way, which ideally follows published or community agreed upon metadata and vocabularies, e.g., naming conventions or units. This way the data become FAIR (findable, accessible, interoperable, reusable) from the beginning and the subsequent inclusion of the gathered data in a domain-specific repositories becomes an easy process.

## Significance, Importance, possible Limitations, and solutions for these
The exponential data increase in geosciences over the past and future years require efficient data handling, which means that data need to be machine-readable and FAIR. ELNs provide an effective workflow to achieve this goal.

ELNs are ideal for teams, as any team member can share and work on the same experiment simultaneously. Data are easily found in ELNs through their integrated search functionalities. Future, or even current instruments and software can provide interfaces for direct data transfer from the instrument into the ELN, further easing the process of machine-readable and FAIR data storage.
Most ELNs have an API (application programming interface) to access data remotely or display and work with the data with custom build interfaces. An example is the web-app to interact with electron microprobe (EPMA) data of the Institute für Geowissenschaften at the GU Frankfurt (https://mag4.org, -> **All Apps** -> **epma-tools**), which is connected to the Kadi4Mat (see below) ELN of this EPMA lab. Some ELNs already include extended functionalities to work with and manipulate data.

Lab work includes the development of new methods, workflows, etc. Parts of such new developments might not immediately be possible to be documented in an existing ELN. The ELN might then be updated appropriately to include the new development. Until then, the new parts can always be included in a ›comment‹ filed, which usually any ELN offers. 

Selecting an appropriate ELN can be challenging, and a concern might be which ELN to choose from, if ELNs are not yet well established in a community. A specific concern might be to choose an ELN which will be discontinued in the future. There are, however, efforts to make ELN entries interoperable among different ELNs to ease this concern.


## Examples
 - **KADI4Mat** (https://kadi.iam.kit.edu) is developed by the Karlsruher Institute for Technology (KIT) as part of an NFDI project. It provides extensive and flexible options to facilitate various types of lab data. It is free to use, or can be installed locally.
 - **elabFTW** (https://www.elabftw.net) is also free and open source, and can be installed locally.
 - **chemmotion** (https://chemotion.net) is specific for chemical data, and less for geosciences data. It is, however, interesting regarding its functionalities, as it provides extensive data post-processing options.

