---
name: Prompt accessed GeoQAMap for Earth Data

description: A multi-agent Large Language Model powered GeoQ&A System for geospatial data retrieval and analysis.

author: 
  - name: Yu Feng
    orcidId: https://orcid.org/0000-0001-5110-5564
  - name: Puzhen Zhang 
    orcidId: https://orcid.org/0009-0004-2208-0942
  - name: Linfang Ding 
    orcidId: https://orcid.org/0000-0002-3707-5845
  - name:  Guohui Xiao
    orcidId: https://orcid.org/0000-0002-5115-4769
  - name: Liqiu Meng
    orcidId: https://orcid.org/0000-0001-8787-3418

inLanguage: ENG


about:
  - external nfdi4earth
  - tools and techniques

isPartOf: 
  - N4E_Incubators.md
  - LHB_ENG.md

subjectArea: 
  - dfgfo:3.43-02 

keywords:
  - Large Language Model
  - Human Computer Interaction
  - Geographic Question Answering

audience: 
  - data user
  - general public


identifier: 

version: 0.1

license: CC-BY-4.0

---

# Prompt accessed GeoQAMap for Earth Data

_**Contribution to the community**: A multi-agent Large Language Model powered GeoQ&A System for geospatial data retrieval and analysis. GeoQAMap lowers the barrier to entry for users unfamiliar with GIS, enabling a broader audience to access and make use of valuable geospatial data_.

In this project, we developed GeoQAMap, an innovative Q&A system designed to enable users to interact with vector map data seamlessly. It was specifically designed for users across various skill levels, from GIS and SQL experts to those less familiar with geospatial technologies. 

## Key Features and Capabilities

1.	**Spatial Analysis through Natural Language**: GeoQAMap supports a variety of fundamental spatial analysis operations that users can perform via natural language input. For example, users can conduct buffer analysis to calculate the specified area surrounding a geographic entity. Additionally, intersection analysis helps users identify overlapping areas between multiple geographic entities. These features enable users to easily explore spatial data, which can be applied to tasks such as site selection, evacuation during disasters, etc.

2.	**Multi-criteria Geographic Entity Query**: GeoQAMap not only supports simple, explicit queries but also handles complex or implicit queries involving multiple data fields of one data or across multiple data sources. The system automatically breaks down the problem into subtasks and executes them, ultimately providing integrated results. This simplifies the query process and greatly improves efficiency. 

3.	**Support for Follow-up Questions**: GeoQAMap features robust support for continuous Q&A, allowing users to follow up on initial queries with additional, related questions. The system maintains contextual understanding and provides responses to subsequent queries. Users can also generate data textual summaries or histograms based on the results, offering more detailed and clear insights into the data. 

4.	**Exceptional User Experience**: Through user study and benchmark testing, GeoQAMap has demonstrated outstanding performance in 4 key metrics out of 6, including attractiveness, efficiency, stimulation, and novelty, surpassing 75% of user interaction products in the benchmark. This result highlights the advantages of the system in terms of user experience.

This tool is expected to help the ESS community better promote their data, making it easier for people without specialized knowledge to explore and interact with it. By providing natural language chatting capabilities, GeoQAMap lowers the barrier to entry for users unfamiliar with GIS, enabling a broader audience to access and make use of valuable geospatial data ([Media 1](#fig1)).

<a id="fig1"></a>

![GeoQAMap User interface.](img/Incubator_GeoQAMap_Thumbnail.png "GeoQAMap User interface.") 

## Outcomes
- Feng, Y., Zhang, P., Ding, L., Xiao, G., & Meng, L. (2024) GeoQAS: a Multi-agent LLM Powered Geographic Question Answering System for Data Retrieval and Spatial Analysis (in preparation).
-	Feng, Y., Ding, L., & Xiao, G. (2023). Geoqamap-geographic question answering with maps leveraging LLM and open knowledge base. In 12th International Conference on Geographic Information Science (GIScience 2023). 
- [Software repository](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/prompt-accessed-geoqamap-for-earth-data)
- [Docker Container](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/prompt-accessed-geoqamap-for-earth-data/-/blob/main/Dockerfile?ref_type=heads)
- [Video on YouTube](https://www.youtube.com/watch?v=GeWgN2fMFkE)
