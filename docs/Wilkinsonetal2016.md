---
additionalType: recommended_article

description: Recommended article representing the nucleus of all recent work in research data management along FAIR guiding principles.

author:
  - name: Michael Finkel
    orcidId: https://orcid.org/0000-0002-5268-5203

inLanguage: ENG

about:
  - internal nfdi4earth  
  - fair
  - data management plans


isPartOf: 
  - recommended_articles.md
  - Collection_StoreManage.md
#   - FAIR_data
#   - Metadata_quality

subjectArea: 
  - dfgfo:409-06
  - unseco:concept17126

keywords:
  - research data management
  - FAIR principles

audience: 
  - data collector
  - data owner
  - data depositor
  - data steward
  - data user
  - data librarian
  - data curator
  - data provider
  - data advocate
  - service provider
  - general public

name: "The FAIR guiding principles for scientific data management and stewardship"

version: 1.0

license: CC-BY-4.0

---

*Recommended Original Article*

# The FAIR guiding principles for scientific data management and stewardship

**by:** Mark D. Wilkinson, Michel Dumontier, IJsbrand Jan Aalbersberg, Gabrielle Appleton, Myles Axton, Arie Baak, Niklas Blomberg, Jan-Willem Boiten, Luiz Bonino da Silva Santos, Philip E. Bourne, Jildau Bouwman, Anthony J. Brookes, Tim Clark, Mercè Crosas, Ingrid Dillo, Olivier Dumon, Scott Edmunds, Chris T. Evelo, Richard Finkers, Alejandra Gonzalez-Beltran, Alasdair J.G. Gray, Paul Groth, Carole Goble, Jeffrey S. Grethe, Jaap Heringa, Peter A.C ’t Hoen, Rob Hooft, Tobias Kuhn, Ruben Kok, Joost Kok, Scott J. Lusher, Maryann E. Martone, Albert Mons, Abel L. Packer, Bengt Persson, Philippe Rocca-Serra, Marco Roos, Rene van Schaik, Susanna-Assunta Sansone, Erik Schultes, Thierry Sengstag, Ted Slater, George Strawn, Morris A. Swertz, Mark Thompson, Johan van der Lei, Erik van Mulligen, Jan Velterop, Andra Waagmeester, Peter Wittenburg, Katherine Wolstencroft, Jun Zhao & Barend Mons

**Published in:** Scientific Data (2016), Vol. 3, 160018

**Link:** <https://doi.org/10.1038/sdata.2016.18>

**Abstract:** There is an urgent need to improve the infrastructure supporting the reuse of scholarly data. A diverse set of stakeholders—representing academia, industry, funding agencies, and scholarly publishers—have come together to design and jointly endorse a concise and measureable set of principles that we refer to as the FAIR Data Principles. The intent is that these may act as a guideline for those wishing to enhance the reusability of their data holdings. Distinct from peer initiatives that focus on the human scholar, the FAIR Principles put specific emphasis on enhancing the ability of machines to automatically find and use the data, in addition to supporting its reuse by individuals. This Comment is the first formal publication of the FAIR Principles, and includes the rationale behind them, and some exemplar implementations in the community.

(Abstract by the [publication](https://doi.org/10.1038/sdata.2016.18) author(s), licensed under [Creative Commons Attribution 4.0 License](http://creativecommons.org/licenses/by/4.0/).

## Comment
Somehow the nucleus of all recent work in research data management. The article describes the FAIR guiding principles for scientific data management, as a results of a workshop ‘Jointly Designing a Data Fairport’ held in Leiden, Netherlands, in 2014. 
