---
name: Suggest a repository

description: NFDI4Earth aims to share information on high-quality services that support FAIR and open research data management in the Earth System Sciences. Here, we provide an overview of steps to suggest your repository.

author:
  - name: Christin Henzen
    orcidId: https://orcid.org/0000-0002-5181-4368
  - name: Claudia Müller
    orcidId: https://orcid.org/0000-0002-0709-5044

inLanguage: ENG

about:
  - internal nfdi4earth  

isPartOf:
  - Collection_ObserveCollectCreateEvaluate.md

additionalType: article

subjectArea:

keywords:
  - metadata
  - services
  - repository

version: 0.1

license: CC-BY-4.0
---

# Suggest your data repository

NFDI4Earth is a community-driven initiative aiming to provide researchers with FAIR, coherent, and open access to relevant Earth System resources, such as repositories.

In NFDI4Earth, we follow a repository definition adapted from [re3data](https://www.re3data.org/suggest): A research data repository is a service and a subtype of a sustainable information infrastructure that provides long-term storage and access to research data that is the basis for a scholarly publication.

Offering high-quality community repositories is key to supporting the researchers. To suggest a repository, you can follow these steps:

## 1. Understand the NFDI4Earth goals and requirements

* __[Mission](https://www.nfdi4earth.de/about-us)__: Familiarize yourself with the mission and goals of NFDI4Earth. This will help you to ensure that your service aligns with our objectives.
* __[Software architecture documentation](https://nfdi4earth.pages.rwth-aachen.de/architecture/architecture-docs/)__: Review the software architecture documentation, particularly the requirements and architecture constraints. 

## 2. Register your repository

* __re3data registration__: Register your repository in re3data and tag it with NFDI4Earth-relevant keywords as described [here](https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/blob/main/docs/N4E_Data_Repository_Registration.md).

## 3. Tell us more about your service by preparing the following information

* __Contact__: Provide an organisational and a technical contact.
* __Technical specifications__: Provide technical details about the repository, if you like to propose your data repository as a source for harvesting.

## 4. Contact the NFDI4Earth Helpdesk

* Please provide your questions or interest and service information to the NFDI4Earth Helpdesk: [helpdesk@nfdi4earth.de](mailto:helpdesk@nfdi4earth.de). We will coordinate follow-up activities with the related NFDI4Earth working groups.

![NFDI4Earth](img/NFDI4Earth.jpg)

<!--

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences).

-->
