---
name: World Settlement Footprint (WSF)

description: Rapid urbanization is increasingly challenging in multiple domains including ecological, economic, and social issues. Detailed settlement data is, thus, essential for several multidisciplinary use cases, e.g., improved preparedness for and response to natural hazards or guiding political and economic decisions more precisely. The NFDI4Earth pilot project “World Settlement Footprint (WSF®)” provides high-resolution global datasets on settlement for the years 2015, 2019 and on settlement development for the period 1985-2015. The data is produced by the DLR team “Smart Cities and Spatial Development” and is provided as a map and for download via the EOC Geoservice. Each WSF collection can also be accessed via a STAC-based catalogue, which enables users to optimize queries and subsequent use for analysis. In addition, further reference data sets of various settlement data for Germany are presented. The data provided by the Leibniz Institute of Ecological Urban and Regional Development is available for download via the IOER Monitor.

author: 
  - name: Jan-Karl Haug
    orcidId: https://orcid.org/0009-0009-3873-8602
  - name: Hendrik Zwenzner
  - name: Thomas Esch
    orcidId: https://orcid.org/0000-0002-5868-9045
  - name: Mattia Marconcini
    orcidId: https://orcid.org/0000-0002-5042-5176
  - name: Torsten Heinen
  - name: André Twele
    orcidId: https://orcid.org/0000-0002-8035-2625
  - name: Julian Zeidler
    orcidId: https://orcid.org/0000-0001-9444-2296
  - name: Tobias Krüger
    orcidId: https://orcid.org/0000-0002-7085-8155
  - name: Sujit Sikder
    orcidId: https://orcid.org/0000-0002-0265-7394
  - name: André Hartmann
    orcidId: https://orcid.org/0000-0003-0384-8501
  - name: Martin Schorcht
    orcidId: https://orcid.org/0000-0002-9898-2975
  - name: Gotthard Meinel
    orcidId: https://orcid.org/0000-0002-9201-7664

inLanguage: ENG

about:
  - external nfdi4earth
  - dataset
  - tools and techniques

isPartOf: 
  - N4E_Pilots.md
  - Collection_SharePublish.md

additionalType: technical_note

subjectArea: 
  - dfgfo:317-02
  - dfgfo:315-02
  - unesco:concept11996
  - unesco:concept441
  - unesco:concept12004
  - unesco:concept3236
  - unesco:concept1557
  - unesco:concept10045

keywords:
  - spatio-temporal data
  - time series
  - software suite
  - settlements
  - NFDI4Earth
  - NFDI4Earth Pilot
  - DLR
  - EOC
  - IÖR
  - WSF
  - evolution
  - 3D
  - urbanization
  - global settlement extent
  - German settlement extents
  - building density
  - building height
  - building volume
  - building stock
  - ALKIS
  - land cover
  - Sentinel-1
  - Sentinel-2

audience: 
  - data user
  - policy maker

version: 1.0

identifier: https://doi.org/10.5281/zenodo.7897531

license: CC-BY-4.0

---

# World Settlement Footprint (WSF)

More than half of the world’s population is living in urban areas and the trend of rapid urbanization is ongoing. Since settlements - and urban areas in particular - represent the centers of human activity; the environmental, economic, political, societal and cultural impacts of urbanization is far-reaching. They include negative aspects like the loss of natural habitats, biodiversity and fertile soils, climate impacts, waste, pollution, natural and man-made disasters, crime, social conflicts or transportation and traffic problems, making urbanization to one of the most pressing global challenges. Accordingly, a profound understanding of the global spatial distribution and evolution of human settlements constitutes a key element in envisaging strategies to assure sustainable development of urban and rural settlements. We provide a new 10m resolution (0.32 arc sec) global map of human settlements on Earth for the years 2015 and 2019, namely the World Settlement Footprint 2015 and the World Settlement Footprint 2019 ([Media 1](#fig1)). Additionally, the WSF Evolution ([Media 2](#fig2)) and the WSF 3D ([Media 3](#fig3)) are part of the product suite. The WSF Evolution is an annual time series of the World Settlement Footprint at 30m resolution from the years 1985 to 2015. The WSF 3D depicts detailed quantification of the average height, total volume, total area and the fraction of buildings at 90m resolution at a global scale.
In addition to the global WSF products, we provide additional building data for Germany. Besides the building footprints with a 1m resolution ([Media 4](#fig4)) the datasets depict information on building height ([Media 5](#fig5)) and building volume. The datasets can be used at any scale of observation in support to all applications requiring detailed and accurate information on places with human presence (e.g., socioeconomic development, population distribution, risk assessment, etc.). In order to make the data available to a broad public, we provide a catalog service in addition to established download and visualization services. Our SpatioTemporal Asset Catalog (STAC) gives users the ability not only to search and filter data, but also to access and process it directly. This technology enables more efficient handling of data. Data transfer of large amounts of data can be prevented and storage and energy capacities can be conserved.

<a id="fig1"></a>

![](img/Pilot_WSF_fig1.png "The World Settlement Footprint (WSF) 2019 is a 10m resolution binary mask outlining the extent of human settlements globally derived by means of 2019 multitemporal Sentinel-1 (S1) and Sentinel-2 (S2) imagery.") 

<a id="fig2"></a>

![](img/Pilot_WSF_fig2.gif "The  World Settlement Footprint (WSF) Evolution is a 30m resolution dataset outlining the global settlement extent on a yearly basis from 1985 to 2015.") 

<a id="fig3"></a>

![](img/Pilot_WSF_fig3.png "WSF 3D: 3D view of WSF 3D data including information about Building Height, Building Area and Building Volume.") 

<a id="fig4"></a>

![](img/Pilot_WSF_fig4.png "Resolution binary mask outlining the extent of human settlements in Germany.") 

<a id="fig5"></a>

![](img/Pilot_WSF_fig5.png "100m resolution of building height in Germany.") 

![](img/Pilot_WSF_logo.png "WSF Logo")

## Resources

### Documentations
* WSF Pilot report ([@haug_2023])
* NFDI Success Story ["Providing high-resolution World Settlement Footprint as FAIR geospatial data"](https://www.nfdi.de/success-stories/?lang=en) 

<br>

### WSF Datasets and Services
| Collections | Products | Services  | 
|-------------|----------|------------------|
| World Settlement Footprint (WSF) 2015 - Landsat-8/Sentinel-1 - Global | World Settlement Footprint (WSF) 2015 | Geoservice (WMS): <https://geoservice.dlr.de/web/maps/eoc:wsf> <br> Geoservice (Download): <https://download.geoservice.dlr.de/WSF2015/> <br> EO Products Service (STAC): <https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/WSF_2015> |
| World Settlement Footprint (WSF) 2019 - Sentinel-1/2 - Global | World Settlement Footprint (WSF) 2019 | Geoservice (WMS): <https://geoservice.dlr.de/web/maps/eoc:wsf2019> <br> Geoservice (Download):  <https://download.geoservice.dlr.de/WSF2019/> <br> EO Products Service (STAC): <https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/WSF_2019> |
| World Settlement Footprint (WSF) Evolution - Landsat-5/7 - Global | Input Data Consistency (IDC) Score | Geoservice (WMS): <https://geoservice.dlr.de/web/maps/eoc:wsfevolution> <br> Geoservice (Download): <https://download.geoservice.dlr.de/WSF_EVO/> |
| World Settlement Footprint (WSF) Evolution - Landsat-5/7 - Global | World Settlement Footprint (WSF) Evolution | Geoservice (WMS): <https://geoservice.dlr.de/web/maps/eoc:wsfevolution> <br> Geoservice (Download): <https://download.geoservice.dlr.de/WSF_EVO/> <br> EO Products Service (STAC):  <https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/WSF_Evolution> |
| World Settlement Footprint (WSF) 3D - Global, 90m | WSF 3D - Building Area, 90m | Geoservice (WMS): <https://geoservice.dlr.de/web/maps/eoc:wsf3d> <br> Geoservice (Download):  <https://download.geoservice.dlr.de/WSF3D/files/> |
| World Settlement Footprint (WSF) 3D - Global, 90m | WSF 3D - Building Fraction, 90m | Geoservice (WMS): <https://geoservice.dlr.de/web/maps/eoc:wsf3d> <br> Geoservice (Download): <https://download.geoservice.dlr.de/WSF3D/files/> | 
| World Settlement Footprint (WSF) 3D - Global, 90m | WSF 3D - Building Height, 90m | Geoservice (WMS): <https://geoservice.dlr.de/web/maps/eoc:wsf3d> <br> Geoservice (Download): <https://download.geoservice.dlr.de/WSF3D/files/> |
| World Settlement Footprint (WSF) 3D - Global, 90m | WSF 3D - Building Volume, 90m | Geoservice (WMS): <https://geoservice.dlr.de/web/maps/eoc:wsf3d> <br> Geoservice (Download): <https://download.geoservice.dlr.de/WSF3D/files/> |
| World Settlement Footprint (WSF) 3D - Vector Tiles - Global, 90m |  | Geoservice (WMS): <http://wis.eoc.dlr.de/wsf3d/#5/50.39/11.28> |

<br>

### IOER Monitor Datasets and Related Services
| Collections | Services | 
|-------------|-------------| 
| IOER Building Footprints Complete Germany 1m | IOER Monitor: <https://monitor.ioer.de/fdzdata/1m_buildings_complete/> <br> DLR EO Products Service (STAC): <https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/IOER_BUILDINGS_COMP_GER_1M> |
| IOER Building Footprints non-residential Germany 1m | IOER Monitor: <https://monitor.ioer.de/fdzdata/1m_buildings_nonresidential/> <br> DLR EO Products Service (STAC): <https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/IOER_BUILDINGS_NONRES_GER_1M> | 
| IOER Surface Coverage - complete - Germany, 10m | IOER Monitor: https://monitor.ioer.de/fdzdata/10m_buildings_complete/ <br> DLR EO Products Service (STAC): https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/IOER_SURFACECOV_COMP_GER_10M | 
| IOER Surface Coverage - non-residential - Germany 10m | IOER Monitor: https://monitor.ioer.de/fdzdata/10m_buildings_nonresidential/ <br> DLR EO Products Service (STAC): https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/IOER_SURFACECOV_NONRES_GER_10M |  
| IOER Building Structure - Height Median - Germany, 100m | IOER Monitor: https://monitor.ioer.de/fdzdata/100m_buildings_height/ <br> DLR EO Products Service (STAC): https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/IOER_STRUCTURE_HEIGHT_GER_100M |
| IOER Building Structure - Volume - Germany, 100m | IOER Monitor: <https://monitor.ioer.de/fdzdata/100m_buildings_volume/> <br> DLR EO Products Service (STAC):  <https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/IOER_STRUCTURE_VOLUME_GER_100M> | 

## References
