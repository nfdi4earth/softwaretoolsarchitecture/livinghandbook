---
name: What is a research data management plan?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth
  - data management plans



isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - unesco:concept7387
  - unesco:concept112
  - unesco:concept522
  - unesco:concept7396
  - unesco:concept8201
  - unesco:concept15607

keywords:
  - data management
  - research project

audience: 
  - data collector
  - data owner
  - data steward

version: 1.0

license: CC-BY-4.0

---

# What is a research data management plan?

A data management plan describes the data generated during a scientific research work. A focus lies on data collection, data description and how the data was produced. Which standards were used and how the data will be handled during and after a research project and how the data should be protected. 
