---
name: What does the NFDI4Earth offer? 

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth


isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - dfgfo:34
  - unesco:mt2.35

keywords:
  - NFDI4Earth

audience: 
  - general public

version: 1.0

license: CC-BY-4.0

---

# What does the NFDI4Earth offer? 

NFDI4Earth offers interaction with specialists engaged in data management and data science at national governmental institutions. It facilitates access to information on contemporary research data management and data science as practiced at internationally renown facilities. Activities revolve around Data Repositories, Data Management Services, Metadata Standards, Data Discovery, Data Access and Sharing, Data Preservation and Community Building. 