---
name: World Heat Flow Database Project (WHFD)
# The title of the article.

description: The World Heat Flow Database Project develops a new research data infrastructure around the IHFC Global Heat Flow Database (IHFC). Heat flow is a measure of thermal energy flowing from the Earth's core to its surface. The research data infrastructure is offering a one-stop shop for comprehensive information on heat-flow related data, publications, projects, and researchers.

# A brief summary of the article, max. 300 characters incl. spaces.

author: 
  - name: Stephan Mäs
    orcidId: https://orcid.org/0000-0002-9016-1996
  - name: Nikolas Ott
    orcidId: https://orcid.org/0000-0003-1633-0058 
  - name: Sven Fuchs
    orcidId: https://orcid.org/0000-0002-2896-6662
  - name: Kirsten Elger
    orcidId: https://orcid.org/0000-0001-5140-8602
  - name: Samuel Jennings
    orcidId: https://orcid.org/0000-0003-3762-7336 
  - name: Florian Neumann
    orcidId: https://orcid.org/0000-0002-9666-5087
  - name: Ben Norden
    orcidId: https://orcid.org/0000-0003-2228-9979

# The author(s) of the article. The name must be given, the ORCID should be given. As many authors as necessary can be listed. 

inLanguage: ENG
# The language of the article. Accepted values are:  
# ENG for English
# DEU for German

isPartOf: 
  - Collection_ProcessAnalyse.md
  - Collection_AccessReuse.md
  - Collection_SharePublish.md
  - Collection_StoreManage.md

# The filenames of the NFDI4Earth Living Handbook collection(s), the article belongs to, as they appear in https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/tree/main/docs. 
# Alternatively, the titles of the collections. The editor will replace it with the collections' file names. 

additionalType: article

subjectArea: 
  - dfgfo:317-01   #Geophysics

keywords:
  - Heat-flow density
  - Thermal geophysics
  - Global heat flow database (GHFD)
  - Thermal parameter
  - Data information system
  - International Heat Flow Commission (IHFC) 
  - success stories
# A list of terms to describe the article's topic more precisely. 

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data steward
  - data advocate
  - data provider

identifier: 

version: 1.0

license: CC-BY-4.0

---

# World Heat Flow Database Project (WHFD) 

_**Contributions to the community:** The project aims to support the process understanding of the Earth’s thermal field by providing authenticated heat flow data. It contributes to FAIR disciplinary data through the creation of a harmonized heat flow data model which is openly available and extended by a new data quality scheme. Moreover, the designed web portal for heat flow data offers community-specific visualisation and analysis functions. This database serves the geoscience community._

Heat flow is a measure of thermal energy flowing from the Earth's core to its surface. Corresponding measurement data has been collected since more than 100 years. However, at the time the World Heat Flow Database Project started the data quality was heterogeneous on both national and international scale and the quality of the data collections was poor. Main reasons have been rudimentary or even missing metadata and data documentation, especially as most data derives from scholarly literature and reports that primarily didn’t aim at providing comprehensive data descriptions and metadata.

![WHFD](img/ShowCase_WHFD.png "Screenshot of the WHFD database")

The aim of the [World Heat Flow Database Project](https://gepris-extern.dfg.de/gepris/projekt/491795283?language=en) is to develop a new research data infrastructure for the Global Heat Flow Database of the IHFC ([International Heat Flow Commission](https://www.ihfc-iugg.org)) offering a “one-stop shop” for comprehensive information on heat-flow related data as well as links to publications, projects, and the researchers. This new user-oriented web platform will provide quality-proofed, up-to-date, well-documented, extended, enriched, and restructured heat-flow data to the geoscientific community. The sustainable provision of the platform and its contents will be assured by the GFZ Potsdam. The quality assurance and improvement of the historical heat flow data is done by the parallel [Global Heat Flow Data Assessment Project](https://www.ihfc-iugg.org/products/global-heat-flow-database/assessment-project), which is led by IHFC and Task Force VIII of the International Lithosphere Program.
The new technical database will take into account the criteria of FAIR and OPEN data policy and will support the interoperability with other geoscientific data services (e.g., [EPOS](https://www.epos-eu.org/)). Core elements are the provision of globally used persistent identifiers: 
1. DOI for heat-flow data newly submitted to the database.
2. ORCID IDs to uniquely identify authors.
3. International Generic Sample Numbers (IGSN) for connecting data and literature with physical samples on which data were measured. 

## Outcomes

This project is supported by [the International Heat Flow Commission (IHFC)](https://www.ihfc-iugg.org/). Models and database are openly available as follows:
- The harmonized heat flow data model: [@fuchs2021]
- The German Heat Flow Database 2022: [@fuchs2022]
- The developed data quality scheme extends the existing reference data model: [@fuchs2023]
- A new version of the global heat flow database is released every year: Information about available data can be accessed on the [OneStop4All](https://onestop4all.nfdi4earth.de/search?searchterm=%22global+heat+flow%22&sort=). The data is published in the GFZ Data Services repository, with additional details available [here](https://onestop4all.nfdi4earth.de/result/r3d-r3d100012335).
- A demo of the developed Web application is available at the project's [GitHub repository](https://worldheatflowdatabase.github.io/HeatFlowMapping/)

## References

