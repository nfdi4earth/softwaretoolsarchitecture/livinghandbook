---
name: Research Data Management (RDM) Services

description: Many institutions of higher education offer dedicated support services for Research Data Management (RDM). Here, we provide an overview of RDM services operated by members of the NFDI4Earth and beyond

author: 
  - name: Claudia Müller
    orcidId: https://orcid.org/0000-0002-0709-5044	 

inLanguage: ENG

isPartOf: 
  - Collection_PlanDesign.md

additionalType: article

subjectArea:  
  - unesco:concept3799
  - unesco:concept157
  - dfgfo:313%0A
  - unesco:concept8075
  - unesco:concept11831
  
keywords:
  - Earth System Sciences
  - metadata
  - metadata license
  - GeoDCAT
  - Climate Forecast
  - NetCDF
  - OAI-PMH 
  - CSW 
  - STAC

version: 0.1

license: CC-BY-4.0

---

**RDM Support Services**

Many institutions of higher education offer dedicated support services for Research Data Management (RDM) (see also Forschungsdaten.info). These services are often located at institutions’ libraries, and may or may not be linked to research data repositories. The following table provides examples of RDM support services operated by members of the NFDI4Earth (and beyond) and their utility according to three categories: consulting, training, and tools for research data management. The information is compiled based on details available on the websites of the respective institutions as of September 2024. When available, we also document the type of users for which the services are accessible (M: members of the institution only, G: members of German institutions only, I: German and international institutions, and “?”: information not available).

Service, is defined in line with a previous suggestion in NFDI as a technical-organisational solution. It broadly includes storage and computing services, software, processes, workflows, and the personnel support for service desks. It is not meant to explain single services, but more information can of course be found on the corresponding webpages of the services. Though some institutions list data repositories on their webpages as support services, we do not list them here, because they are collected separately (https://www.nfdi4earth.de/2interoperate/repositories-and-infrastructures). The information in the table is manually collected from the websites of the NFDI4Earth member institutions, hence the overview is not complete and constantly growing. 


|**Institution**|**Service**| **URL** |**Consulting**|**Training**|**Tool4RDM**|**Access**|
|----|----|----|----|----|----|----|
|**Alfred-Wegener-Institut Helmholtz-Zentrum für Polar- und Meeresforschung (AWI)**|O2A Registry|https://registry.o2a-data.de/|||x|M (this includes DAM members)|
|**DKRZ Deutsches Klimarechenzentrum**|Consultancy|https://www.dkrz.de/en/services/beratung-en|x|||I|
||Model Code Optimization|https://www.dkrz.de/en/services/model-code-optimization||x|x|I|
||Provision of Computing Power|https://www.dkrz.de/en/services/bereitstellung-von-rechenleistung|||x|I|
||Workshops and Training|https://www.dkrz.de/en/services/||x||?|
|**Deutscher Wetterdienst** |DWD Services|https://www.dwd.de/EN/ourservices/_functions/search/search_Formular.html |||x| I |
|**GEOMAR Helmholtz-Zentrum für Ozeanforschung Kiel**|RDM Consulting and workshops|https://www.geomar.de/en/idrz/digital-research-services/researchdatamanagement |x|x||G|
||Digital sample registration|https://biosamples.geomar.de |x||x|M|
||Data management in projects|https://portal.geomar.de |x||x|I|
||BIIGLE|https://annotate.geomar.de/login |||x|G|
|**Goethe-Universität Frankfurt**|E-LEARNING|https://www.ub.uni-frankfurt.de/forschungsdaten/e_learning_fdm.html ||x||M|
||Elektronische Laborbücher: eLabFTW|https://www.ub.uni-frankfurt.de/forschungsdaten/laborbuecher.html |||x|M|
||Goethe RDMO|https://www.ub.uni-frankfurt.de/forschungsdaten/rdmo.html |||x|M|
||Training Events (HeFDI)|https://www.ub.uni-frankfurt.de/forschungsdaten/rdmo.html ||x||I|
|**Gottfried Wilhelm Leibniz Universität Hannover**|eLabFTW|https://www.fdm.uni-hannover.de/en/tools/elabftw|||x|M|
||RDM Training|https://www.fdm.uni-hannover.de/en/rdm-training||x||I|
|**Helmholtz-Zentrum für Umweltforschung - UFZ**| BioMe - Digital Ecosystem for Time Series Data Management|https://www.ufz.de/index.php?en=50440||||
||BioMe - Citizen Science platform for Bioderversity Science|https://www.ufz.de/biome/index.php?en=49810||||
||spatial.IO - Spatial data infrastructure|https://www.ufz.de/index.php?en=50440||||
||Chromeleon - chromatography data system software|https://www.ufz.de/index.php?en=50440||||
||eLabFTW - Electronic Lab Notebook|https://www.ufz.de/index.php?en=50440|||x|M|
||KNIME - software for creating data science workflows|https://www.ufz.de/index.php?en=50440|||x|M|
||OMERO - Visualize, manage, and annotate microscope images and metadata|https://www.openmicroscopy.org/omero|||x|I|
||SMS - Sensor Management System|https://web.app.ufz.de/sms/|||x|M|
|**Helmholtz-Zentrum Hereon**|Campaign Metadata Tool|https://www.hereon.de/central_units/hcdc/tools/field_app/index.php.en |x||x|I|
|**KIT - Karlsruher Institut für Technologie**|FAIR-RS|https://www.rdm.kit.edu/1288.php |||x|M|
||Kadi4Mat|https://www.rdm.kit.edu/servicestools_tools_kadi4mat.php |||x|M|
||RDMO|https://www.rdm.kit.edu/servicestools_tools_rdmo.php |||x|M|
||RDM Navigator|https://www.rdm.kit.edu/RDM/RDM-navigator/index.php |x||x|I|
||RDMToolKIT|https://www.rdm.kit.edu/train-edu_rdmtoolkit.php |x|||M|
||Train & Edu|https://www.rdm.kit.edu/train-edu.php |x|x||?|
|**Technische Informationsbibliothek** **(TIB)**|ESS Terminology Service|https://terminology.tib.eu/ts/ontologies?collection=ESS&and=false&sortedBy=title&page=1&size=10 |||x|I|
|**TU Dresden**|RDM Services|https://tu-dresden.de/forschung-transfer/services-fuer-forschende/kontaktstelle-forschungsdaten/unser-service |x|||?|
||DMP for Grant Proposals|https://tu-dresden.de/forschung-transfer/services-fuer-forschende/kontaktstelle-forschungsdaten/unser-service/unterstuetzung-bei-der-fdm-umsetzung |x||x|G|
|**Universität Heidelberg**|RDM Services|https://www.data.uni-heidelberg.de/en/rdm-services  |x|||I|
|**Universität Leipzig**|Research Data Management Service| https://www.uni-leipzig.de/en/research/research-service/research-data-management |x|x||I|


This article is part of the NFDI4Earth Living Handbook. Do you miss an RDM service? Please add it here:  https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/edit/main/docs/RDM-Services.md?ref_type=heads.


<!--

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences). 

-->
