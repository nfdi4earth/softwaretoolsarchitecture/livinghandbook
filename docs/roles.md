---
name: Definition of Roles in Research Data Management within the NFDI4Earth

description: This is a definition of the different roles in research data management used in the NFDI4Earth. 

author: 
  - name: Michael Finkel
    orcidId: https://orcid.org/0000-0002-5268-5203

inLanguage: ENG

about:
  - internal nfdi4earth
  - assigning roles and responsibilities



isPartOf: 
  - Metadata.md
  - LHB.md

additionalType: articles

subjectArea: 
  - dfgfo:409-06
  - unesco:concept7398

keywords:
  - research data management
  - role

version: 0.1

audience:
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

license: CC-BY-4.0

---

# Definition of Roles in Research Data Management within the NFDI4Earth

In the context of the LHB we distinguish the following roles for individuals and institutions involved in research data management:

## Data collector
The person responsible for collecting, generating, re-using or contributing to research data and documents contexts and processes that relates to data handling. 

_Source: <https://geo-data-support.sites.uu.nl/data-management/data-management-roles/> (merged data creator and data contributor)_ 

## Data owner
Typically the principal researcher of the project or research group leader who is responsible for establishing who has access to the data, and reducing or mitigating the risk of any mishaps; e.g. improper storage facility, non-compliance with specific guidelines. 

_Source: <https://geo-data-support.sites.uu.nl/data-management/data-management-roles/>_ 

## Data depositor
Either the data creator or data owner and is the person who will perform the deposit of the data to a data preservation facility, like a repository or data archive. They perform activities such as uploading data after it has been generated, making it suitable for archiving, creating metadata and data documentation and submitting the data to the facility, so it can be preserved for the long-term. 

_Source: <https://geo-data-support.sites.uu.nl/data-management/data-management-roles/>_ 

## Data user
Someone who is interested in accessing and interacting with data for verification or (re)using data to evaluate, analyse, model and generally handle then to create insights and knowledge. access to research data for re-use or verification. 

_Source: <https://geo-data-support.sites.uu.nl/data-management/data-management-roles/> (modified/generalised)_ 

## Data curator
A person who ensures availability and reliability of data. They review and harmonise metadata of datasets submitted for storage and ensure that they are generally compliant with relevant policies before publication. They further monitor access to the data, support data owners in fulfilling data access requests and possess deeper technical knowledge about the backend of the repository or storage system. 

_Modified and enhanced after: <https://geo-data-support.sites.uu.nl/data-management/data-management-roles/>_ 

## Data librarian
An information professional responsible for developing technical expertise on practical solutions of data management, archiving and dissemination and on data mining and visualization. 

_Source: <http://data.loterre.fr/ark:/67375/TSO-PVGCQB0S-C>_ 

## Data steward
A supporter who advises researchers (or otherwise involved) persons on data management, privacy and information security aspects and can understand the meaning and applications of the data. They support: 

* data creators on quality assurance, storage, policy compliance
* data depositors on data selection, metadata and data documentation creation, preserving and publishing
* data custodians on data curation, access controls, FAIR data management

_Source: <https://geo-data-support.sites.uu.nl/data-management/data-management-roles/>_ 

## Data advocate 
A person promoting the proper use of research data and technology and providing assistance or guidance in doing so. Such a person could be: 

* a communication relay between various departments and their staff involved in data management within an organization or project
* Providing technical in blogs, tutorials, talks and other means to spread the knowledge about data technologies and related best practices
* Someone voluntarily providing advice to the members of the research community on good research data management and the implementation of the FAIR principles.

 _Merged from: data referent (<http://data.loterre.fr/ark:/67375/TSO-LWJF0BM2-P>), data evangelist (<http://data.loterre.fr/ark:/67375/TSO-BV1ZD5QP-R>), data champion (<https://www.data.cam.ac.uk/intro-data-champions>S)_ 

## Data provider
A person or organization that stores and provides datasets to others.

_Inspired by: <https://www.snowflake.com/guides/data-suppliers>_ 

## Service provider
An organization that provides services related to the processing and management of data, such as, high-performance computing facilities or web-based maps. 

_Own definition_ 

## Research software engineer
A person working with researchers to gain an understanding of the problems they face, and then develops, maintains and extends software to provide the answers. 

_Source: <https://ukrse.github.io/who.html>; <https://www.wikidata.org/wiki/Q106268090>_

## Policy maker
In research data management, policy makers strive to design effective, technology-neutral, forward-looking and coherent data governance policies across sectors. 

_Source: Going Digital Guide to Data Governance Policy Making in OECDiLibrary[@OECD2022]_ 

## General public
All persons that do not belong to any of the other groups. 

##  Remarks
- Researchers, supporters, and other persons involved in research data management can have multiple roles (e.g., the data creator can also be data owner and data depositor, and a data custodian can also be data steward).
- Hence, as a researcher, student, supporter, or otherwise being involved in research data management, you should select the target group that fits best to your actual matter of interest.
