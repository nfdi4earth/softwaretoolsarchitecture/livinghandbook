---
additionalType: recommended_article

description: Recommended article discussing opportunities, prospects, and concerns of ChatGPT in Hydrology and Earth Sciences.

author:
  - name: Michael Finkel
    orcidId: https://orcid.org/0000-0002-5268-5203

inLanguage: ENG

about:
  - internal nfdi4earth  



isPartOf: 
  - recommended_articles.md
#   - FAIR_data
#   - Metadata_quality

subjectArea: 
  - dfgfo:34
  - dfgfo:443-04
  - unesco:concept157
  - unseco:concept188
  - unesco:concept3052

keywords:
  - large language models
  - artificial intelligence
  - hydrology
  - earth sciences

audience: 
  - data collector
  - data owner
  - data depositor
  - data steward
  - data user
  - data librarian
  - data curator
  - data provider
  - data advocate
  - service provider
  - general public

name: "ChatGPT in Hydrology and Earth Sciences: Opportunities, Prospects, and Concerns"

version: 1.0

license: CC-BY-4.0

---

*Recommended Original Article*

# ChatGPT in Hydrology and Earth Sciences: Opportunities, Prospects, and Concerns

**by:** Ehsan Foroumandi, Hamid Moradkhani, Xavier Sanchez-Vila, Kamini Singha, Andrea Castelletti & Georgia Destouni

**Published in:** Water Resources Research (2023), Volume 59, Issue 10, October 2023, e2023WR036288

**Link:** <https://doi.org/10.1029/2023WR036288>

**Abstract:** The emergence of large language models (LLMs), such as ChatGPT, has garnered significant attention, particularly in academic and scientific circles. Researchers, scientists, and instructors hold varying perspectives on the advantages and disadvantages of using ChatGPT for research and teaching purposes. ChatGPT will be used by many scientists going forward for creating content and driving scientific progress. This commentary offers a brief explanation of the fundamental principles behind ChatGPT and how it can be applied in the fields of hydrology and other Earth sciences. The article examines the primary applications of this open artificial intelligence tool within these fields, specifically its ability to assist with writing and coding tasks, and highlights both the advantages and concerns associated with using such a model. Moreover, the study brings up some other limitations of the model, and the dangers of potential miss-uses. Finally, we suggest that the academic community adapts its regulations and policies to harness the potential benefits of LLMs while mitigating its pitfalls, including establishing a structure for utilizing LLMs and presenting clear regulations for their implementation. We also outline some specific steps on how to accomplish this structure.

(Original abstract published in <https://doi.org/10.1029/2023WR036288>)

## Comment
This is a very helpful article that
<br />
**(i)** gives some some key definitions for understanding ChatGPT.
<br />
**(ii)** adresses the implications and limitations of large language models in the Earth Sciences.
<br />
**(iii)** gives five key recommendations for regulating ChatGPT for Earth science academia.

