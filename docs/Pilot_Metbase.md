---
name: Developing Tools and FAIR Principles for the MetBase Database

description: Description of the MetBase NFDI4Earth Pilot project, including information about Astromat, visualisation and teaching tools and an electronic lab notebook for electron microprobe, funded also by HeFDI.

author: 
  - name: Dominik C. Hezel
    orcidId: https://orcid.org/0000-0002-5059-2281
  - name: Premkumar Elangovan
  - name: Mara L. Hochstein
    orcidId: https://orcid.org/0000-0002-6752-965X
  - name: Horst R. Marschall
    orcidId: https://orcid.org/0000-0002-0609-682X

inLanguage: ENG


about:
  - external nfdi4earth
  - tools and techniques
  - dataset 


isPartOf: 
  - N4E_Pilots.md
  - Collection_ProcessAnalyse.md
  - Collection_ObserveCollectCreateEvaluate.md

additionalType: technical_note

subjectArea: 
  - dfgfo:316-01
  - dfgfo:311-01
  - unesco:concept3228
  - unesco:concept501

keywords:
  - Database
  - Meteorites
  - Astromaterials
  - "NFDI4Earth Pilot"

audience: 
  - data user
  - data librarian

version: 1.0

identifier: https://doi.org/10.5281/zenodo.8297837


license: CC-BY-4.0

---

# Building the world’s largest Meteorite Database (plus additional web applications)


### Merging MetBase into Astromat

**MetBase** has been the world’s largest database for chemical and isotopic data, which has been accessible through a user-friendly, modern web interface. MetBase was founded in 1995 and underwent multiple technical updates until 2025, when after its last update to make MetBase compliant with the FAIR principles, MetBase was merged into the Astromat database. [**Astromat**](https://www.astromat.org) is NASA’s primary archive for laboratory analyses of astromaterial samples, and funded by NASA to provide services for the preservation and open access of data from astromaterials, including meteorites, in alignment with the FAIR principles. This merger required multiple alignments in the database structure, the content and organization of data and metadata, the terminology used, as well as the incorporation of new data. All this took almost 1 year to complete. Now, the cosmochemical community has access to the largest compilation of cosmochemical analytical data by far: over 2 million analytical data points. The merger of these two databases is published in [Hezel DC, Lehnert KA, Elangovan P, Ji P, Mays J, Koblitz J (2025) The MetBase database has been merged into Astromat. Meteoritics & Planetary Science 60:143-148.](https://doi.org/10.1111/maps.14293).

### Visualisation and Teaching Tools

MetBase was not only a database, but also included comprehensive visualisation tools, as well as a number of teaching courses. The [visualisation tool](#fig1) and the courses are now developed at the Goethe University Frankfurt. The visualisation tool is completely redesigned and allows the visualisation of multiple databases, currently with data from Astromat, Georoc, and AusGeochem. The course material, including hundreds of videos, quizzes, and material for download such as plots, sketches, etc. is also continually developed and as well freely accessible.

### An Electronic Lab Notebook (ELN) with a Web application for Data Processing and Access

This project further triggered the development of a workflow to store and access electron microprobe data (e.g., from meteorites), which are now stored in the [Kadi4Mat electronic lab notebook (ELN)](https://kadi4mat.iam.kit.edu). Users access their data through a web-interface with comprehensive pre-processing, visualisation, etc. tools, from which the data are then as well downloaded. These new [EPMA Data Tools](#fig2) in combination with the ELN make raw data from the instrument FAIR and available in a long-term archive. This ELN is further developed as part of a grant from the Hessische Forschungsdaten Initiative (HeFDI).

All new tools now developed at the Goethe University Frankfurt are available through [mag4](https://www.mag4.org).

[Hezel DC, Lehnert KA, Elangovan P, Ji P, Mays J, Koblitz J (2025) The MetBase database has been merged into Astromat. Meteoritics & Planetary Science 60:143-148.](https://doi.org/10.1111/maps.14293)

<a id="fig1"></a>

![Screenshot of the Data Access Visualiser web app to access and visualise data from various databases.](img/Pilot_Metbase_fig1.png "Screenshot of the Data Access Visualiser web app to access and visualise data from various databases.") 

<a id="fig2"></a> 

![Landing page of the EPMA Data Tools web app.](img/Pilot_Metbase_fig2.png "Landing page of the EPMA Data Tools web app.")

### Acknowledgements

This work has been funded by the German Research Foundation (NFDI4Earth, DFG project no. 460036893, <https://www.nfdi4earth.de/>), and the Hessische Forschungsdateninfrastruktur (HeFDI) through the PI Dominik Hezel.
