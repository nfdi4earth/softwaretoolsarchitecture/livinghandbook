---
name: Dataset Search in the OneStop4All

description: NFDI4Earth aims to share information on high-quality datasets that support FAIR and open research data management practices in the Earth System Sciences. Here, we provide information about the dataset search.

author:

  - name: Christin Henzen
    orcidId: https://orcid.org/0000-0002-5181-4368

inLanguage: ENG

about:
  - internal nfdi4earth  

isPartOf:
  - Collection_ObserveCollectCreateEvaluate.md

additionalType: article

subjectArea:

keywords:
  - dataset 
  - data discovery

version: 0.1

license: CC-BY-4.0
---

# Dataset Search in the OneStop4All

The OneStop4All offers dataset search using distributed data sources, such as repositories and catalogs. This means that the OneStop4All acts as a single entry point but does not manage datasets and metadata independently.<br>

Dataset search features will be implemented incrementally until the end of 2025.  

As of the current version, the dataset search includes the following data sources:

* DLR Earth Observation Center, see: https://geoservice.dlr.de/eoc/ogc/stac/v1/collections
* GDI-DE Geodatenkatalog, see: https://gdk.gdi-de.org/gdi-de/srv/eng/catalog.search#/home
* GFZ Data Services, see: https://dataservices-cms.gfz-potsdam.de
* Helmholtz Earth and Environment DataHub, see: https://earth-data.de 
* IOW Metadata Catalog, see: https://iowmeta.io-warnemuende.de/geonetwork
* KIT-Bibliothek - KITopen Repository, see: https://www.bibliothek.kit.edu/kitopen.php 
* Karlsruhe Institute of Technology - Data Catalog, see: https://cat4kit.atmohub.kit.edu 
* Marine Dateninfrastruktur Deutschland, see: https://mdi-de.org  
* PANGAEA, see: https://pangaea.de
* RADAR, see: https://www.radar-service.eu/radar/en  
* TERENO, see: http://teodoor.icg.kfa-juelich.de/geonetwork
* UFZ Metadata Catalog, see: https://geonetwork.ufz.de/geonetwork 
* World Data Center for Climate, see: https://www.wdc-climate.de  

Please note that the Helmholtz Earth and Environment DataHub and GDI-DE are both data sources and harvester nodes. As harvester nodes, they harvest metadata from further data sources. More information is available at: https://earth-data.de/providers and https://www.geoportal.de/Datenanbieter.

<!--

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences).

-->

