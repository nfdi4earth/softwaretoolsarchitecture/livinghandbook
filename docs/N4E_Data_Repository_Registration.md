---
name: Register a data repository in re3data

description:

author:
  - name: Claudia Müller
    orcidId: https://orcid.org/0000-0002-0709-5044
  - name: Jonas Grieb
    orcidId: https://orcid.org/0000-0002-8876-1722
  - name: Christin Henzen
    orcidId: https://orcid.org/0000-0002-5181-4368

inLanguage: ENG

about:
  - internal nfdi4earth
  - sharing data
  

isPartOf:
  - Collection_ObserveCollectCreateEvaluate.md

additionalType: article

subjectArea:

keywords:
  - metadata
  - data repositories

version: 0.1

license: CC-BY-4.0
---

# Register a data repository

NFDI4Earth provides an overview of NFDI4Earth-operated and Earth-System-Sciences-relevant data repositories. Information on the data repositories will be harvested and managed in our [Knowledge Hub](https://knowledgehub.nfdi4earth.de/) and made available through our [OneStop4All](https://onestop4all.nfdi4earth.de/).
NFDI4Earth collaborates closely with the REGISTRY OF OPEN RESEARCH DATA REPOSITORIES ([re3data](https://www.re3data.org/)) and uses it as the data source for repository information.

## How to register a data repository in re3data

re3data provides a web form for suggesting data repositories. We recommend to use the form and provide the requested information: [https://www.re3data.org/suggest](https://www.re3data.org/suggest).

Please reach out to our NFDI4Earth Helpdesk [helpdesk@nfdi4earth.de](mailto:helpdesk@nfdi4earth.de) to support the suggestion process.

## How to make the data repository entry available for NFDI4Earth

Although many data repositories are relevant for Earth System Sciences or multidisciplinary use cases, NFDI4Earth filters existing re3data entries. Thus, you have to tag the suggested data repository with

- the subject Geosciences or a related sub-discipline (see [DFG subject classification](https://www.dfg.de/en/research-funding/proposal-funding-process/interdisciplinarity/subject-area-structure)) and
- the country Germany
- the ROR of your institution (department).

Registering the data repository is a preliminary but separate step for harvesting dataset metadata.

<!--

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences).

-->
