---
name: The NFDI4Earth Living Handbook
description: Presenting scope and outline of the NFDI4Earth Living Handbook.

author: 
  - name: Thomas Rose
    orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

about:
  - internal nfdi4earth

isPartOf: NFDI4Earth.md

additionalType: collection

subjectArea: 
  - unesco:mt2.35
  - unesco:concept8079
  - unesco:concept503

keywords:
  - NFDI4Earth
  - Living Handbook

audience: 
  - general public

version: 1.0

license: CC-BY-4.0

---

# Welcome to the NFDI4Earth Living Handbook

The NFDI4Earth Living Handbook provides (i) an encyclopaedia-style documentation for all aspects related to the NFDI4Earth, its services and outcomes in a human readable form, and (ii) aims to structure and harmonise all information related to data management and data science approaches in the Earth System Sciences (ESS) in a community-driven effort. 

The Living Handbook is compiled in a community-driven process, meaning that the individual members of the ESS community provide the knowledge through articles and modify the articles to add further information or to keep them up to date. The [NFDI4Earth Living Handbook Editorial Board](https://nfdi4earth.de/2interoperate/living-handbook-editorial-board) supports the community in this effort, improves the editorial workflow and can set impulses in the development of the content. At the same time, the Living Handbook is the archive of the User Support Network. 

## Structure
The NFDI4Earth Living Handbook is structured along the data life cycle with the main topics 

* Plan and Design: Plan processes and data management from onboarding to project closure. 
* Observe, collect, create and evaluate: Organisation and procedures for the creation and collection of datasets (laboratory, field, and modelling data). 
* Process and analyse: Processing and analysing data 
* Store and Manage: Preserving and managing your data on a project level
* Archive: Making data fit for long-term storage, including the selection (or disposal) of data as preparation for their preservation
* Share and Publish: Establishing and supporting the reach and impact of your research data
* Access and reuse: Ensuring the broad utility of your research data efforts for other researchers

with an additional category for articles covering the NFDI4Earth and its outcomes. 

Within these main topics, smaller topics can be defined, the so-called [collections](LHB_collections.md). Because articles may relate to more than one of these smaller topics, they can be assigned to multiple collections. Moreover, collections can be part of collections and articles can belong to more than one collection, creating a network of topically-related articles. 

## Contributing
The NFDI4Earth Living Handbook is compiled by volunteers from the ESS community and neighbouring fields. They are creating and updating its content with support by the [NFDI4Earth Living Handbook Editorial Board](https://nfdi4earth.de/2interoperate/living-handbook-editorial-board), which provides tools to make this process as inclusive and smooth as possible while ensuring a high quality of the content. To learn more about how to contribute to the NFDI4Earth Living Handbook, have a look into its [documentation](LHB_ENG.md) or get in touch with its [Editorial Board](mailto:nfdi4earth-livinghandbook@tu-dresden.de). 
