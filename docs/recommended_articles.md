---
name: Recommended articles 

description: A list of peer-reviewed publications on research data management and data science in the Earth System Sciences recommended by the NFDI4Earth Living Handbook Editorial Board because of their high-quality. 

author: 
  - name: Michael Finkel
    orcidId: https://orcid.org/0000-0002-5268-5203

inLanguage: ENG

additionalType: collection

subjectArea: 
  - dfgfo:34
  - unesco:mt2.35
  - concept3810
  - concept3117
  - concept8079
  - concept4224
  - concept103

keywords:
  - publications
  - peer-review
  - literature

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Recommended articles

These peer-reviewed publications are recommended by the Edutitorial Board because of their impact on research data management and data science practices in the Earth System Sciences or as high-quality examples for such practices and case-studies. 
