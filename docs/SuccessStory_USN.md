---
name: Jemal gets help from the NFDI4Earth User Support Network


description: Jemal, a PhD student, investigates the relation between sea surface temperature and fish stock in the Mediterranean. He contacts the User Support Network, which supports him in finding a suitable dataset, suggests tools to handle and to analyse the data. 


author: 
  - name: Hela Mertens
    orcidId: https://orcid.org/0000-0002-4526-2472
  - name: Ines Langer
    orcidId: https://orcid.org/0000-0003-0670-4597

inLanguage: ENG

about:
  - internal nfdi4earth

  
isPartOf: 
  - Collection_PlanDesign.md



subjectArea: 
  - dfgfo:313 #(Atmospheric Science, Oceanography and Climate Research)
  - dfgfo:21 #(Biology)
  - dfgfo:23 #(Agriculture, Forestry and Veterinary Medicine)


keywords:
  - temperature
  - sea surface temperature
  - fishery
  - Mediterrenean Sea
  - water temperature
  - fish stock
  - model data 
  - data search


audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public




version: 1.0
 

license: CC-BY-4.0


---

# Jemal gets help from the NFDI4Earth User Support Network 


_**Contributions to the community**: The User Story demonstrates various NFDI4Earth services and their utilisation by providing concrete examples. Special focus is placed on the functionality of the User Support Network, in this case providing support for finding a specific dataset and pointing towards related educational materials. The User Story raises the reader's awareness of the benefits of FAIR data and Open Science._
 
Jemal is a PhD student in a research institute in the North of Germany. He is working in a big European Project, which investigates the influence of climate change on species in the Mediterranean. In his doctoral thesis, he focusses on the European anchovy (Engraulis encrasicolus) and its changing occurrence in recent decades. Jemal would like to see to what extent the change in water temperature could play a role. A project partner points out to him that there should be long-term water surface temperature data for the Mediterranean, but he is not an expert either and recommends that Jemal should contact the NFDI4Earth User Support Network, with which he himself has already had a good experiences. Jemal then immediately contacts the User Support Network via the [NFDI4Earth portal](https://onestop4all.nfdi4earth.de/). 

![Proto-Persona Jemal](img/USN-proto-persona_Jemal.png "Proto-Persona Jemal")` 

He initially receives an automatic reply, but within a day he receives a specific reply from a person in the network asking him for a few details about the data he is looking for, such as the target area, time period, minimum temporal resolution and special requirements, e.g. whether it should be observational data from satellites or data from models. He is given brief information on the advantages and disadvantages of both. Jemal communicates his data requirements to his USN contact, who also involves other experts in answering the request.

![USN ticket form](img/USN-UserSupport_Form.png "USN ticket form")`

The USN recommends the sea surface temperature data set from the Integrated Climate Data Center (ICDC) which Jemal can access via an [entry on the NFDI4Earth portal](https://onestop4all.nfdi4earth.de/result/3d302787648e97edbff8). The data is available in NetCDF format. Jemal has worked with NetCDF data in the past, but it was a long time ago. The contact at the USN recommends to visit a [course](https://edutrain.nfdi4earth.de/courses/course-v1:NFDI4Earth+20230502CP+self-paced/) from their education and training materials to refresh his knowledge. Jemal is inspired by the information from the USN and his visit to the NFDI4Earth portal and is working on new ideas that include more data sets and more complex analyses.
