---
name: NFDI4Earth Incubator Lab

description: This article provides information about the NFDI4Earth Incubator Lab and a list of all articles related to them. 

author: 
  - name: Steffen Busch
    orcidId: https://orcid.org/0000-0002-5607-9040

inLanguage: ENG


about:
  - internal nfdi4earth
  - resources

isPartOf: 
  - NFDI4Earth.md

additionalType: collection

subjectArea: 
  - dfgfo:34
  - unesco:mt2.35
  - unesco:concept522
  - unesco:concept7387
  - unesco:concept2214

keywords:
  - NFDI4Earth

audience: 
  - data user
  - data collector
  - data steward
  - data librarian
  - service provider

version: 1.0

license: CC-BY-4.0

---

# The NFDI4Earth Incubator Lab

The [Incubator Lab](https://nfdi4earth.de/2participate/incubator-lab) fosters novel data science developments for ESS in dedicated focused projects by funding of short projects (up to 6 month). Exploring innovative and blue sky developments is explicitly encouraged even if it leads to a negative result in the course of the project. The objective is to steer the *exploration of new, potentially relevant building blocks* to be included in NFDI4Earth and related NFDIs. Examples are tools for  automatic metadata extraction and annotation, semantic mapping and harmonization, machine learning, data fusion, visualization, and interaction. The Incubator Lab also serves as a forum where novel requirements can be formulated and trends presented in terms of a user consultation process. 

## Calls
The first call was published in April 2022. There were 23 submissions from 7 different research areas and 4 funded projects ([Media 1](#med1)). 

<a id="med1"></a>

![Pie chart with the distribution of the submitted proposals for the NFDI4Earth Incubator Lab, nine projects were coming from the research areas of Atmospheric Science, Oceanography and Climate Research and four from Geography. Three proposals were coming from the researhc areas of Geology/Palaeontology, Mineralogy/Petrology/Geochemistry, and Geodesy/Remore Sensint/Geoinformatics, each. One proposal each were submitted from the fields of Visualisation and Biodiversity.](img/N4E_Incubator_media1.png "The distribution of the proposals submitted to the first call of the NFDI4Earth Incubator Lab according to their research areas.")

&nbsp;

The second call was published in June 2023. There were 11 submissions from 4 different research areas and 5 funded projects ([Media 2](#med2)).

<a id="med2"></a>

![This figure has to two subfigures: A map of Germany with the cities of the insitutions with funded proposals from the first and second call to the NFDI4Earth Incubator Lab highlighted. Funded were proposals from Munich, Hanover, Geesthacht, and Hamburg in the first call, and Munich, Hannover, Braunschweig, Potsdam and Leipzig in the second call. The second figure is a pie chart, depicting the distribution of the funded proposals according to the research areas. Four belong to the research areas of Geophysics/Geodesy, three to Water Research, two to Atmospheric Science, Oceanography and Climate Research, and two to Mineraloggy, Petrology and Geochemistry.](img/N4E_Incubator_media2.png "The distribution of the proposals submitted to the NFDI4Earth Incubator Lab according to their geographic distribution and research areas.")

&nbsp;

## First Cohort

