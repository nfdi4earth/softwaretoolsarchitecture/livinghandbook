---
name: German Marine Seismic Data Access

description: Reflection seismic data are the paramount source of information for the subsurface structure as they provide the highest resolution of any geophysical technique. As such, they have been used for various academic and commercial purposes. The NFDI4Earth Pilot project German Marine Seismic Data Access addressed the publication and rescue of marine seismic data by developing a common strategy for future retrieval and archival, fostering collaborations across research institutions and universities, and continuing the efforts in follow-up projects aligned with Deutsche Allianz Meeresforschung (DAM) and Helmholtz Metadata Collaboration (HMC).

author:
  - name: Janine Berndt
    orcidId: https://orcid.org/0000-0003-3720-3568
  - name: Christian Hübscher
    orcidId: https://orcid.org/0000-0001-7380-2344
  - name: Hanno Keil
    orcidId: https://orcid.org/0000-0003-3072-6753
  - name: Andreas Lehmann
    orcidId: https://orcid.org/0000-0001-5618-6105
  - name: Christian Berndt
    orcidId: https://orcid.org/0000-0001-5055-0180

inLanguage: ENG


about:
  - external nfdi4earth
  - dataset
  - storage
  - tools and techniques


isPartOf:
  - N4E_Pilots.md
  - Collection_AccessReuse.md
  - Collection_StoreManage.md

additionalType: technical_note

subjectArea:
  - dfgfo:315-01
  - unesco:concept8605
  - unesco:concept7438
  - unesco:concept11412
  - unesco:concept160
  - unesco:concept2214
  - unesco:concept7387

keywords:
  - data access
  - data infrastructure
  - marine seismology

audience:
  - data user
  - data librarian
  - service provider
  - data curator

version: 1.0

identifier: https://doi.org/10.5281/zenodo.7875451


license: CC-BY-4.0
---

# German Marine Seismic Data Access

**Reflection seismic data** are critical for understanding subsurface structures due to their high spatial and temporal resolution. These data have been collected over the past 60 years for both academic and commercial purposes, especially in the identification of hydrocarbon reservoirs. However, the large size of seismic datasets (ranging from hundreds of megabytes to terabytes) has presented significant data management challenges over the years, particularly due to varied acquisition systems and data processing methods.
<br>
The **German Marine Seismic Data Access** pilot project, initiated by the working group **Marine Geophysics AGMAR** of the **Forschungskollegium Physik des Erdkörpers (FKPE)**, aimed to address these challenges by developing a unified data infrastructure and rescue strategy for legacy seismic data. This project contributes to the broader effort of NFDI4Earth in establishing a distributed infrastructure for data curation through harmonized data workflows with links to international data repositories.
<br>

## Objectives

1. **Develop a Strategy for Marine Seismic Data Archaeology**: Create a system to make existing seismic data accessible for future use.
2. **Establish a Common Data Standard**: Develop a standard for data archival and metadata management that can be used across German marine geophysical institutions.
3. **Create a Rescue Strategy for Legacy Data**: Implement solutions for transferring data from outdated storage formats (e.g., 9’’ track tapes) to modern systems like hard disks.
4. **Collaborate with International Initiatives**: Align with similar efforts in the U.S. and Italy, ensuring interoperability and compliance with FAIR principles.

## Key Features

- **Seismic Data Archaeology**: A key feature of the project was to investigate the current state of seismic data storage and management across German institutions. The project worked with multiple organizations, including GEOMAR, the University of Kiel, the University of Hamburg, and the Alfred Wegener Institute (AWI), to assess the state of data and develop a strategy for long-term data rescue and management.
- **Data Access and Retrieval**: Institutions like GEOMAR have already archived significant amounts of seismic data, and the pilot project worked on transferring data from legacy media like tapes to hard disks. In addition, data were stored at institutions like University of Bremen, with lists of available data already accessible online.
- **Aligning with International Efforts**: The project studied international data management initiatives such as the MGDS in the U.S. and Disko2 in Norway to explore best practices for seismic data management. Collaboration with these initiatives helped define the path forward for German seismic data management efforts.

## Outcomes

The German Marine Seismic Data Access project produced the following outcomes:

- **Development of a Common Strategy**: The project made significant progress in identifying the current state of seismic data storage and the challenges associated with rescuing legacy data. A clear strategy for future retrieval and archival of seismic data was established.
- **Cross-Institution Collaboration**: Collaborations between institutions like GEOMAR, University of Kiel, University of Hamburg, and AWI resulted in a collective understanding of data management needs and the resources required for future work.
- **Proposal for Further Funding**: The project flagged the need for further resources to fund the retrieval of legacy data, especially the copying of 9’’ tapes at GEOMAR. Proposals for continued funding were submitted, including one to the Helmholtz Metadata Initiative.

## Challenges and Gaps

While significant progress was made, there were several challenges:

- **Legacy Data Storage**: Many institutions still hold seismic data on outdated media like tapes, requiring complex and resource-intensive efforts to transfer this data to modern storage systems.
- **Lack of Metadata**: Many legacy datasets lack complete metadata, making it difficult to establish consistency across databases and limiting the usability of some data.
- **Funding Limitations**: Although the project identified a clear need for resources, securing funding to implement the full data retrieval and archival strategy remains an ongoing challenge.

The German Marine Seismic Data Access project continued with support from NFDI4Earth and DataHub, ensuring its success.

## Achievements and Progress

### 1. Development of a Metadata Description for 2D Multichannel Seismic Raw Data

The German marine seismic community, PANGAEA, and data managers from various institutions and universities collaboratively established a standardized metadata vocabulary for 2D multichannel seismic raw data. This vocabulary aligns with the FAIR principles (Findable, Accessible, Interoperable, and Reusable). The metadata description will be reviewed on a yearly base as it is considered a living document ([https://doi.org/10.5281/zenodo.14754825](https://doi.org/10.5281/zenodo.14754825)).

### 2. Implementation and Integration of Metadata Standards for Future German Vessel Cruises

- **Adoption Across Research Vessels**: The newly defined metadata standard is now being implemented across major German research vessels.
- **Streamlined Data Transfer**: A seamless process has been introduced to transfer raw experiment data from vessels to the PANGAEA data repository in partnership with the German Marine Alliance (DAM). This transparent workflow simplifies compliance with data management requirements for scientists. Metadata are directly integrated into platforms such as GEOMAR’s Ocean Science Information System (OSIS).
- **Expansion to Additional Seismic Data**: The approach is being extended to other types of seismic data through the HMC project MetaSeis, which focuses on metadata concepts for Ocean Bottom Seismometer (OBS) and 3D seismic data within the German research community.

Specific progress includes:

- **Archived Data**: MCS raw data for cruises SO294 and SO299/2 have been archived, complete with required technical reports, and visualized on [marine-data.de](https://marine-data.de).
- **Metadata Submission**: Institutions such as the University of Kiel, the Federal Institute for Geosciences and Natural Resources (BGR), the University of Hamburg and GEOMAR have submitted seismic data and metadata, with publication scheduled for 2025 following quality control and curation.
- **Future Expeditions**: Several upcoming expeditions have already adopted the new workflow for publishing raw data. During the initial implementation phase, GEOMAR will oversee quality control and curation. Long-term, this process will be decentralized to individual institutions once routing and scripting are fully established.

### 3. Development of a Dedicated Seismic Data Viewer

A beta version of the [Marine Seismic Compilation Viewer](https://marine-data.de/viewers/cf518de6-7885-4a6f-b8dc-ecae2171ca15) has been introduced. Developed under the German Marine Research Alliance (DAM) core area for Data Management and Digitalization and in collaboration with the Helmholtz DataHub Initiative for Earth and Environment, the viewer provides:

- Access to marine seismic metadata and datasets collected by researchers.
- Integration of selected international datasets.

### 4. HMC project MetaSeis

We have successfully secured additional funding through the HMC project MetaSeis (Metadata Concept for OBS (Ocean Bottom Seismometer) and 3D Seismic Data for the German Community). More information is available [here](https://helmholtz-metadaten.de/de/inf-projects/metaseis).
<br>
MetaSeis is a collaborative effort between AWI and GEOMAR, launched in May 2024 and set to conclude in October 2025. The project focuses on creating a unified data infrastructure to support the future archival of:

- 3D seismic reflection data, and
- Active Ocean Bottom Seismometer (OBS) data from recent and upcoming research cruises.

Key objectives include:

- Adopting and Extending Standards: Building on existing metadata standards and interoperable vocabularies within the seismic domain to ensure compatibility and alignment with the FAIR principles (Findable, Accessible, Interoperable, and Reusable).
- Metadata Quality and Validation: Implementing quality assurance and validation checks for seismic metadata to maintain accuracy and reliability.
- Integration of Future and Legacy Data: Establishing workflows to seamlessly archive both future datasets and legacy data. 
<br>

Within MetaSeis, metadata standards are being developed for:

- 3D Multichannel Seismic Raw Data: Extending the existing metadata description developed for 2D multichannel seismic raw data.
- Active Ocean Bottom Seismic Raw Data: Metadata tailored to OBS data generated from active seismic sources.
- Legacy OBS Data: General metadata standards for active source OBS data to facilitate archival and accessibility

## Community Support and Infrastructure

To support the research community:

- A **WIKI at PANGAEA** is being developed, featuring **Standard Operating Procedures (SOPs)** for seismic data submission. This will enable researchers to upload their data efficiently and consistently.
- The **AG Seismic Working Group** has been established under the German Marine Research Alliance (DAM) core area for Data Management and Digitalization. This group meets regularly to address challenges in developing standards and procedures for different seismic data types

## Future Directions

The project and its initiators still plan to continue their efforts to rescue legacy seismic data and align data management practices across German institutions. The following steps are proposed:

1. **Expand Collaboration and Funding**: Secure funding to continue the data retrieval work, including the transfer of data from tapes to hard disk.
2. **Implement International Best Practices**: Continue to align with international initiatives like MGDS and Disko2, ensuring that German seismic data follow FAIR principles and are accessible through platforms like PANGAEA and marine-data.de.
3. **Develop Metadata Standards**: Work on establishing a flexible metadata standard that can accommodate both raw and processed seismic data, ensuring that future data is fully FAIR-compliant and reusable.

The German Marine Seismic Data Access project was, and continues to be, a significant step toward rescuing and managing seismic data in Germany, laying the foundation for future research and collaboration in marine geophysics. Furthermore, we have been approached by international institutions to present our metadata vocabularies, underscoring their relevance and potential for broader adoption.

![Seismic data and metadata visualized in the marine-data.de Portal.](img/Pilot_GSMD_Marine_Data_Viewer_Metadata.png
 "Seismic data and metadata visualized in the marine-data.de Portal.")

![Seismic data visualized in the marine-data.de Portal.](img/Pilot_GSMD_Marine_Data_Viewer_Seismic_Data.png
 "Seismic data visualized in the marine-data.de Portal.")

## Resources

- Roadmap / project report ([@berndt_2023])

## References
