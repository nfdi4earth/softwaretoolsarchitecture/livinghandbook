---
additionalType: recommended_article

description: Recommended article about quality control in large databases managing hydrological data.

author:
  - name: Michael Finkel
    orcidId: https://orcid.org/0000-0002-5268-5203

name: "The Global Streamflow Indices and Metadata Archive (GSIM) - Part 2: Quality control, time-series indices and homogeneity assessment"

isPartOf: 
  - recommended_articles.md
  - Collection_ProcessAnalyse.md

version: 1.0

inLanguage: ENG

about:
  - external nfdi4earth
  - dataset
  - metadata
  - data management plans


keywords:
  - research data management
  - hydrology
  - field observation
  - water resources

audience:
  - data collector
  - data depositor
  - data steward
  - data user
  - data curator
  - data provider
  - research software engineer

subjectArea: 
  - dfgfo:318-01
  - unesco:concept6517
  - unesco:concept192
  - unesco:concept188

license: CC-BY-4.0

---

*Recommended Original Article*

# The Global Streamflow Indices and Metadata Archive (GSIM) - Part 2: Quality control, time-series indices and homogeneity assessment

**by:** Lukas Gudmundsson, Hong Xuan Do, Michael Leonard, and Seth Westra

**Published in:** Earth System Science Data (2018), Vol. 10[2], 787-804

**Link:** <https://doi.org/10.5194/essd-10-787-2018>

**Abstract:** This is Part 2 of a two-paper series presenting the Global Streamflow Indices and Metadata Archive (GSIM), which is a collection of daily streamflow observations at more than 30 000 stations around the world. While Part 1 (Do et al., 2018a) describes the data collection process as well as the generation of auxiliary catchment data (e.g. catchment boundary, land cover, mean climate), Part 2 introduces a set of quality controlled time-series indices representing (i) the water balance, (ii) the seasonal cycle, (iii) low flows and (iv) floods. To this end we first consider the quality of individual daily records using a combination of quality flags from data providers and automated screening methods. Subsequently, streamflow time-series indices are computed for yearly, seasonal and monthly resolution. The paper provides a generalized assessment of the homogeneity of all generated streamflow time-series indices, which can be used to select time series that are suitable for a specific task. The newly generated global set of streamflow time-series indices is made freely available with an digital object identifier at [PANGAEA](https://doi.pangaea.de/10.1594/PANGAEA.887470) and is expected to foster global freshwater research, by acting as a ground truth for model validation or as a basis for assessing the role of human impacts on the terrestrial water cycle. It is hoped that a renewed interest in streamflow data at the global scale will foster efforts in the systematic assessment of data quality and provide momentum to overcome administrative barriers that lead to inconsistencies in global collections of relevant hydrological observations.

(Abstract by the [publication](https://doi.org/10.5194/essd-10-787-2018) author(s), licensed under [Creative Commons Attribution 4.0 License](http://creativecommons.org/licenses/by/4.0/).

## Comment
This is part 2 of a very interesting work showing how quality control aspects can be integrated in large databases that are fed from various sources.
