---
name: Access and Reuse

description: Collection of articles about research data management topics related to making your research data accessible and ensuring their broad utility to other researchers. 

author: 
  - name: NFDI4Earth Editorial Board
    # orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

additionalType: collection

subjectArea: 
  - dfgfo:34
  - unesco:mt2.35
  - unesco:concept7396
  - unesco:concept17128
  - unesco:concept1129
  - unesco:concept1147

keywords:
  - research data management
  - reusability
  - accessibility

audience: 
  - data collector
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Access and reuse

## What is the access and reuse phase?

After data have been [shared, managed and published](Collection_SharePublish.md), researchers will likely be interested to access and reuse these data. As the 8th phase of the data lifecycle, data access and reuse are closely linked, but have different focuses.

**Access**: Data access involves mechanisms and processes that allow users to find, obtain, and utilise datasets. Many geoscience datasets are stored in public repositories and databases, such as [PANGAEA](https://pangaea.de/), [EarthChem](https://www.earthchem.org), or [GEOROC](https://georoc.mpch-mainz.gwdg.de/georoc/). These platforms provide free access to a wide range of geoscientific data. Users can also discover geoscientific datasets through search engines, metadata catalogues, or specialised databases like the NFDI4Earth [OneStop4All](https://onestop4all.nfdi4earth.de/).  

**Reuse**: Data reuse refers to the practice of utilising previously collected and archived geoscientific data in current or planned projects. By reusing existing datasets, scientists can build on past work, enhance the efficiency of their research, and gain deeper insights into Earth processes and environmental changes. For example, satellite imagery and remote sensing data from missions like Landsat can be reused to study howland use or deforestation changed over time.

## Why is the access and reuse phase important?

Access to and reuse of data are crucial to not duplicate the same effort, try and replicate a specific result,  integrate existing data in the big picture of how the Earth works, or build a comprehensive model for climate prediction. Large datasets are required for AI models, and are the basis for large language models to learn about ESS. Access to existing data is pivotal for such models.

## What will be offered in this article collection?

This collection provides best practices for data access and reuse in the ESS, including setting up effective access mechanisms and managing permissions. We plan to include practical examples and guidelines for the ESS to support researchers in optimizing the workflow and utility of their data. 

We encourage contributions from the community to help refine and enhance this collection and provide challenges and experience in data access and reuse for more effective and collaborative data solutions.  

For more details on how to contribute, please refer to our [template article](example_ENG.md).