---
name: What license should I use for publishing my data?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth
  - publication
  - licensing options
  - sharing data


isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - unesco:concept3810
  - unesco:concept5326
  - unesco:concept5313

keywords:
  - license
  - data publication

audience: 
  - data depositor
  - data steward

version: 1.0

license: CC-BY-4.0

---

# What license should I use for publishing my data?

Data licences clarify the terms of use of your published data. To this end they provide guidance on how others can use, share and build upon your data. This involves legal aspects and employer's typically have respective guidelines and policies. In addition there are excellent online resources such as [CESSDA Training Team (2017 – 2022). CESSDA Data Management Expert Guide. Bergen, Norway: CESSDA ERIC](https://dmeg.cessda.eu/) with its Chapter [Licensing your data](https://dmeg.cessda.eu/Data-Management-Expert-Guide/6.-Archive-Publish/Publishing-with-CESSDA-archives/Licensing-your-data)
