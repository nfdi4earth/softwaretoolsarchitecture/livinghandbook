---
name: How can I contribute to the NFDI4Earth?

author: 
  - name: User Support Network

inLanguage: ENG

about:
  - internal nfdi4earth


isPartOf: 
  - FAQ.md 

additionalType: FAQ

subjectArea: 
  - unesco:concept3117 
  - dfgfo:34
  - unesco:mt2.35

keywords:
  - NFDI4Earth

audience: 
  - general public

version: 1.0

license: CC-BY-4.0

---

# How can I contribute to the NFDI4Earth?

As an open consortium we are always happy to receive new ideas and contributions from all aspects of research data management. There are various ways how you can contribute to the NFDI4Earth. 

| Contribution/Involvement | Contact |
|--------------------------|---------|
| Becoming a member of the consortium | [Coordination team](mailto:nfdi4earth-coordination@tu-dresden.de) |
| Contributing to the [Living Handbook](LHB_ENG.md) | [Living Handbook Editorial Board](mailto:nfdi4earth-livinghandbook@tu-dresden.de) |
| Joining the User Support Network | [Klaus Getzlaff](mailto:kgetzlaff@geomar.de) |
| Publish educational material | [Farzaneh Sadeghi](mailto:farzaneh.sadeghi@hs-bochum.de) |
| Joining a NFDI4Earth Interest groups | [see list](https://www.nfdi4earth.de/2participate/get-involved-by-interest-groups) |
| Applying for a [NFDI4Earth Pilot](N4E_Pilots.md) | [Kolja Nenoff](mailto:kolja.nenoff@uni-leipzig.de) |
| Applying for a NFDI4Earth Incubator | [Udo Feuerhake](mailto:udo.feuerhake@ikg.uni-hannover.de) |
| Applying for a NFDI4Earth Educational Pilot | [Farzaneh Sadeghi](mailto:farzaneh.sadeghi@hs-bochum.de) |
| Joining the NFDI4Earth Academy | [Jonas Kuppler](mailto:jonas.kuppler@gfz-potsdam.de) |
