---
name: Explore the Academy and connect with peers

description: Learn more about how to join the NFDI4Earth Academy and their open events.  

author: 
  - name: Hildegard Gödde
  - name: Effi-Laura Drews
    orcidId: https://orcid.org/0009-0007-8878-8080
  - name: Jonas Kuppler
    orcidId: https://orcid.org/0000-0003-4409-9367
  - name: Konstantin Ntageretzis
  - name: Kristin Sauerland
    orcidId: https://orcid.org/0000-0002-0003-1259
  - name: Gauvain Wiemer
    orcidId: https://orcid.org/0000-0003-4420-5318

inLanguage: ENG

about:
  - internal nfdi4earth

additionalType: call_participate

subjectArea: 
  - unesco:concept157
  - unesco:concept12756
  - unesco:mt2.10
  - unesco:concept3247
  - unesco:concept4603

keywords:
  - early career scientists
  - think tank
  - interdisciplinary research
  - data science
  - joint training

audience: 
  - data collector
  - data owner
  - data user
  - policy maker

version: 1.0

license: CC-BY-4.0

---

# Explore the Academy and connect with peers

## What is the NFDI4Earth Academy?

The [NFDI4Earth Academy](N4E_Academy.md) is a network of doctoral and post-doctoral scientists, interested in bridging Earth System and Data Sciences beyond institutional borders. The Academy covers specialized training courses and facilitates collaborations within the NFDI4Earth community with access to all NFDI4Earth innovations and services. With the two-year Academy program, we offer an open science and learning environment. Academy Fellows can advance their current research projects by exploring and integrating new methods and connecting with like-minded scientists in an agile, bottom-up, and peer-mentored community. Using the established structures of our three research networks ([Geo.X](https://www.geo-x.net), [Deutsche Allianz Meeresforschung](https://www.allianz-meeresforschung.de/), and [Geoverbund ABC/J](https://www.geoverbund-abcj.de/)), our program combines various virtual, hybrid, and in-person events. Further information can be found on the [NFDI4Earth Academy homepage](https://www.nfdi4earth.de/2participate/academy/). 

## How to participate in the NFDI4Earth Academy?
One way to participate in the Academy is by becoming an Academy Fellow. Open calls for Fellows are advertised widely within the community and on the [NFDI4Earth Academy homepage](https://www.nfdi4earth.de/2participate/academy). Another way to participate in the Academy is by joining open events. While some events of the Academy’s program are exclusively for Fellows, several events are open to everybody interested. In particular, the Academy offers regular short Coffee Lectures on various Data Science and Data Management topics, and co-organises bigger events, such as the [DataXplorer Hackathon 2023](https://www.nfdi4earth.de/?view=article&id=365&catid=9) that arose from a cooperation with other NFDI consortia. 

All open events and calls are advertised via the [NFDI4Earth Academy Homepage](https://www.nfdi4earth.de/2participate/academy) and the [NFDI4Earth Newsletter](https://www.nfdi4earth.de/home/newsletter). 
