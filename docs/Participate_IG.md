---
name: Join or propose a NFDI4Earth Interest Group

description: Learn more about how to participate in the NFDI4Earth through its interest groups. 

author: 
  - name: Thomas Rose
    orcidId: http://orcid.org/0000-0002-8186-3566
  - name: Daniel Nüst
    orcidId: https://orcid.org/0000-0002-0024-5046

inLanguage: ENG

about:
  - internal nfdi4earth


additionalType: call_participate

subjectArea: 
  - dfgfo:3
  - unesco:mt2.35
  - unesco:concept11653

keywords:
  - NFDI4Earth

audience: 
  - general public
  - policy maker
  - data steward
  - data owner
  - data user

version: 1.0

license: CC-BY-4.0

---

# Join or propose a NFDI4Earth Interest Group

## About

An Interest Group (IG) is a temporary or permanent forum for a community or stakeholder group to collaborate on a specific topic.
IGs can identify, discuss, and communicate requirements for NFDI4Earth, jointly advance NFDI4Earth-related concepts, technologies and processes, help to implement NFDI4Earth principles, or contribute to dissemination of specific NFDI4Earth offers.
All members of the ESS community may join the IGs.

IGs support the NFDI4Earth vision, connect with other initiatives and organisations, and complement the work of the NFDI4Earth Task Areas.
IGs are entitled to submit proposals to the NFDI4Earth Steering Group, in order to address particular requests concerning the NFDI4Earth development (e.g., for support of specific domain specific requirements or development of specific standards or their adoption).

The NFDI4Earth supports its IGs by providing communication facilities (virtual meetings, mailing list, web space, etc.) to facilitate their daily work and to ensure proper communication.
It further exposes the IGs' work in the community through the invitation to participate in events, such as the annual plenary meeting, or the NFDI4Earth messaging channels like the newsletter or on social media.
Additional support can be made available upon request to [Daniel Nüst](mailto:daniel.nuest@tu-dresden.de).

## Current NFDI4Earth Interest Groups

You are kindly invited to join any of the below IGs.
Please visit the IG website to learn more about the meeting schedule and communication channels.

* [Geology & Geophysics](IG_I3G.md)
* [High-performance Computing in Earth System Sciences](IG-HPC.md)
* [Machine Learning](https://www.nfdi4earth.de/2participate/get-involved-by-interest-groups/ig-machine-learning)
* [Research Software](https://www.nfdi4earth.de/2participate/get-involved-by-interest-groups/ig-research-software)
* [Metadata Standards for Geochemical Data](https://www.nfdi4earth.de/2participate/get-involved-by-interest-groups/ig-metadata-standards-for-geochemical-data)
* [Long-term Storage and Archiving](IG-LTA.md)
* [FAIRESST](https://nfdi4earth.de/2participate/get-involved-by-interest-groups/ig-fairesst)

## How to set up a NFDI4Earth IG? 

An Interest Group can be initiated by at least 6 persons from at least three different organisations.
Therefore, external contributors are highly welcome!
Following an open community process, the IGs shall be open for new members, which commit themselves to the IGs' objectives.

Please [submit your application](https://www.geo-x.net/nfdi4earth-interest-groups/) by describing your

* Motivation
* Scope
* Initial work plan
* Mode of work
* Initial members
* Lifetime of the IG (permanent, temporary)
* Expected results (paper, event, course materials, draft specification, case studies, and flexible projects etc.)

Your application will be evaluated and decided upon by the NFDI4Earth Steering Group. As soon as your IG is confirmed, you would be asked to elect a representative spokesperson. The [NFDI4Earth coordination office](mailto:nfdi4earth-coordination@tu-dresden.de) would like to help you to get started.
