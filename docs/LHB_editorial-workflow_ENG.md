---
name: Editorial Workflow
description: Specification of the editorial workflow in the Living Handbook.

author: 
  - name: Thomas Rose
    orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

about:
  - internal nfdi4earth
  - workflow

isPartOf: LHB_ENG.md

additionalType: article

subjectArea: 
  - unesco:concept3810
  - unesco:concept326
  - unesco:concept4224

keywords:
  - Living Handbook 
  - NFDI4Earth

audience: 
  - general public

version: 1.0

license: CC-BY-4.0

---

# Editorial Workflow

This document provides the detailed editorial workflow for the NFDI4Earth Living Handbook ([Media 1](#fig1)). 

## Submission

Articles can currently only be submitted to the Living Handbook by mail to <incoming+nfdi4earth-livinghandbook-livinghandbook-79252-issue-@mail.git.rwth-aachen.de>. This will automatically create an issue in the [NFDI4Earth Living Handbook GitLab repository](https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook). 

## From submission to publication

### Initial checks

An editor handling the article will be assigned to the respective issue. They will handle the article and all correspondence with the author related to this submission. The editor adds the label “new article” to the issue (labels are used throughout the procedure to track the progress) and asks the managing editor to check if the author has accepted the [authorship agreement](LHB_author-agreement_ENG.md). If they have not yet, the managing editor will ask the submitting author to do so by [inviting him to subscribe the respective mailing list](https://mailman.zih.tu-dresden.de/groups/admin/nfdi4earth-lhb-contributors/members/add) which is our GDPR-compliant approach of storing and managing this information. 

Next, the editor will perform the following, initial checks:

The submitted article

- fits the scope of the Living Handbook
- has a clear connection to ESS, NFDI4Earth or RDM
- adheres to the [author guidelines](LHB_author-guidelines_ENG.md)
- is complete (e.g., all image files are included, links are working)
- the author agreement is signed, and license information for any re-used material is provided
- comes with correct metadata
- is appropriate in its style and quality, and sufficiently backed up by references

After the article passed these checks, it moves to [Final checks and publication](#final-checks-and-publication). The editor decides if the article should be checked by an external person based on a [list of criteria](LHB_expert checks_criteria_ENG.md). The editor sends the author his or her justified decision, and changes the article label to “final check” or “review”, respectively.

In case the checks are not successful, the editor provides appropriate feedback to the author, and asks to provide the missing or corrected information and documents. An article cannot be accepted for further processing before all checks are satisfied.

### External checks (optional)

If required, the editor contacts a suitable external expert to check the article. The NFDI4Earth Living Handbook Editorial Board will keep a record of persons willing to provide their feedback on an ongoing basis to faciliate the search. The check should be completed within 3 weeks. An editor can assign an article to a new external expert, if the review process is not completed within 6 weeks. Once an external expert is assigned, they will receive a copy of the article and submit their feedback through the mail address specific for the article-related issue. This will immediately notify the author, who then has the possibility to update the article within 4 weeks. 

After the author submitted the updated version -- or declared not wishing to do any changes --, the editor assigned to the article will check if the feedback of the external expert was sufficiently taken into account. Unless explicitly requested, the article will not be send again to the external expert. If the editor still sees the need for improvement, he will provide appropriate feedback to the author and request to submit an updated version of the article. 

After the revised article is prepared, it moves to [Final checks and publication](#final-checks-and-publication). 

### Final checks and publication

Once the article is ready to be included in the Living Handbook either as preprint or as checked version, the editor creates a new branch from the issue for the article and places in the new branch all files associated with the article in the respective folders. If the article was not submitted in Markdown, the editor will translate it into Markdown. The editor then prepares a publication-ready version of the article and runs the following final checks:

- If it the articles is a preprint: Inclusion of the “under review”-badge;  Removal of the “under review”-badge, if it is the updated version.  
- Are the metadata correctly entered?
- Inclusion of any additional markdown code (e.g., cross-references)
- Is everything correctly rendered?
- Is the bibliographic information complete?
- Do all links have a valid target/are working?
- Is information from the NFDI4Earth Knowledge Hub successfully retrieved (if any)?

When all checks are successful, the editor increases the version number. Preprints will have version number 0.1 by default, allowing to increase it on the second digit in case more than one revision cycle is needed. All other articles will be initially published with version number 1.0. Version numbers increase if the article is updated, depending on the extent of modifications (see [Article updates](#article-updates)). 

If the publication-ready version is prepared, the editor sends the author the link to the file for approval of the article. 

After the author approved the layout of the article, the editor creates the merge request to include the article into the Living Handbook. The managing editor or, if not available, a person appointed by them, checks and accepts the merge request. The author is notified that the article is now online (as preprint). If the article is published without external checks or as checked version, the editor closes the issue. If the article is published as preprint, the issue is kept open until the checked version is published online. 

## Article updates

If a person wants to update an article, they should download the respective Markdown file from the [GitLab repository](https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/tree/main/docs), make the modifications they wish to make and submit the modified version as regular submission (see above). The editor handling the updated version checks if the updates are sensible. If they are substantial, he might send them to an external expert for checking. Otherwise, the updates will be included. The editor increases the version number and adds the person providing the update either as author or contributor. These adjustment will depend on the extent of modifications (see table). If no contributors are listed yet, the following line will be added at the end of the article: "The following persons contributed to this article:". Contributors are listed with the most recent contributor in front and the first one at the end. Their ORCID ID should be provided where available. 

| Modification extent | Procedure |
|--------|-----------|
| slight/moderate | The version number increases by one on the second digit. <br> The person providing the modifications will be listed as contributor at the end of the article. |
| severe/comprehensive | The version number increases by one on the first digit. The second digit is set to 0. <br> The editorial board will decide whether and how the authorship will be updated to appropriately achknowledge the modifications. The current author of the article should be contacted and asked for feedback. Their feedback shall be included if they respond within two weeks or until the respective meeting of the editorial board, whatever takes longer. |

Subsequently, the updated article proceeds to [Final checks and publication](#final-checks-and-publication). 

## Suggesting a new collection

A new collection can be suggested by anyone via email through the address provided above. The suggestion must include the abstract/description of the collection as well as a suggested list of articles to demonstrate and justify the need for a new collection.

The issue created through the request will be labelled “collection” and an editor assigned to handle the request. The editorial board discusses the suggestion, and may invite the suggesting person to this discussion. Following a positive vote, the collection article is created by the handling editor and published according to the workflows in [Final checks and publication](#final-checks-and-publication). The handling editor will update the metadata of the articles that became part of the new collection. Afterwards, the handling editor informs the submitter of the suggestion that the new collection is implemented and closes the issue. 

<a id="fig1"></a>

![Simplified schema of the editorial workflow described in the text.](img/editorial_workflow.svg "Simplified schema of the NFDI4Earth Living Handbook editorial workflow. The bold arrows indicate the expected major workflow, i.e. submission and publication without the need for an external check of the submitted article. Notes: * Decision is made according to the list of criteria linked in the text.") 
