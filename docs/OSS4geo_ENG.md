---
name: OSS4GEO Overview of Open Source Software for geospatial data analysis
description: Description of the OSS4GEO project

author: 
  - name: Felix Cremer
    orcidId: https://orcid.org/0000-0001-8659-4361

inLanguage: ENG

about:
  - external nfdi4earth
  - tools and techniques


---

# OSS4Geo

[OSS4GEO](https://www.oss4geo.org/) is an initiative to provide an overview of open source software for the analysis and exploitation of geospatial data. OSS4GEO aims to become a curated metadata catalogue of software from the community for the community. 
One can explore the current state of catalogue here <https://www.oss4geo.org/>. This gives an overview of all collected geospatial analysis tools as a dependency graph which are colored by the type of software.


The entries can be filtered by setting different options on the right of the site.
And clicking on an entry will provide more information regarding the software.
Missing entries can be added to the catalogue by members of the community after requesting a contributer account.
To learn more about the project go to the project website <https://project.oss4geo.org/>.

## Contribution

Since this is a community driven tool, it needs to be filled and corrected by the geospatial community. 
To add new software entries or to change the metadata of existing entries one first need to [request a contributor account](https://project.oss4geo.org/contribute/).
Then one can suggest new repositories by providing a link to the source repository and indicating the category of the software. 
The metadata will then be captured automatically and the entry will be added to the metadata catalogue after a check by a curator. 
The project has been initially funded by ESA and is now governed as a community project with this [governance structure](https://drive.google.com/file/d/1Ivmu5Kf1TLjiBPmsvF68xF2G2Us1ynZd/view).

