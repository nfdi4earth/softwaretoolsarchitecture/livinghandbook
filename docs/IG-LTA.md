---
name: Interest Group Long-term Storage and Archiving (IG LTA)

description: This article gives an overview about the activities, format and goals of the NFDI4Earth Interest Group Long-term Storage and Archiving (IG LTA) as well as about the intended outcomes of the IG´s work.

author: 
  - name: Peter Valena
    orcidId: https://orcid.org/0000-0003-2673-2974
  - name: Tim Schürmann
    orcidId: https://orcid.org/0009-0009-4778-5902

inLanguage: ENG

about:
  - internal nfdi4earth
  - resources
  - storage


isPartOf: 
  - N4E_IG.md
  - Collection_Archive.md

additionalType: article

subjectArea: 
  - unesco:concept492
  - unesco:concept2780
  - unesco:mt2.35
  - unesco:concept11831
  - unesco:concept7291



keywords:
  - NFDI4Earth
  - interest group
  - LTA
  - long-term archiving
  - long-term preservation

audience: 
  - data collector
  - policy maker
  - data owner
  - data depositor
  - data user
  - data curator
  - data librarian
  - data steward
  - data advocate
  - data provider
  - service provider
  - research software engineer
  - policy maker
  - general public

version: 1.0

license: CC-BY-4.0

---

# Interest Group Long-term Storage and Archiving (IG LTA)

The Interest Group Long-term Storage and Archiving (IG LTA) facilitates a community forum for exchange and discussions on all relevant aspects of long term preservation of digital Earth System Science (ESS) data. The challenges of long-term preservation of digital ESS data are manifold, affecting researchers producing, collecting, publishing and/or reusing data as well as institutions retaining data alike. Therefore, the IG LTA combines different stakeholders in ESS, such as researchers and data stewards as well as institutional players such as repository and infrastructure providers, archives, governmental agencies, research institutes and universities. Participation in the IG LTA is open to all interested individuals from within and outside of the NFDI4Earth ([Media 1](#fig1)).

<a id="fig1"></a>

![Chart with the stakeholders, interactions, and activities of the IG LTA. On the left side the stakeholders with whom the IG LTA interacts, on the left side the activities carried out by the IG, including interaction with the NFDI4Earth-Measure 2.4.](img/Chart_IG_LTA.png "Stakeholders, interactions and activities of the IG LTA.")

&nbsp; 

Research data and especially ESS data is often highly relevant for many different scientific approaches and disciplines long after its creation. Securing the long-term availability, interpretability and reusability of ESS data in the long-term perspective is therefore of utmost importance for the ESS community, the NFDI4Earth, but also the NFDI as a whole. A successful long-term preservation of digital data depends on the intended timeframe that the data should be kept available, interpretable and reusable. Thus, the IG plans to cover the distinction between long-term storage of digital data (normally around 10 years, e.g. DFG-Guidelines for Safeguarding Good Research Practice) and long-term archiving of digital data (potentially for an indefinite timeframe) and the respective challenges and strategies, addressing the FAIRness of data throughout its data life cycle in the long-term perspective. Based on existing services, infrastructures and collaborations within the ESS community, the IG aims to contribute to the further development of a decentralized but interconnected archiving landscape for ESS data. The IG will focus on topics such as data formats and metadata standards suitable for long-term archiving, questions of appraisal, as well as different strategies for long-term preservation (e.g., bitstream preservation, format migration, emulation). 

The IG will also serve as a forum for community feedback and input on different materials and guidelines, which are being developed in the NFDI4Earth-Measure "Data in Long-Term Storage" (M2.4), such as guidelines for the preparation of data for archiving, guidelines for the appraisal of ESS data for long-term preservation and archiving or guidelines for repository providers. 

The primary goal of the IG is to facilitate the exchange and discussion on all aspects of long-term preservation of ESS data, taking into account the perspectives of different stakeholders in the ESS-community. Ideally, the IG will achieve a common consensus and understanding of the challenges and strategies needed for the long-term preservation of ESS data. Additionally, the IG can function as a forum to initiate and/or contribute to pilot projects and use cases to showcase exemplary preparation of specific data-types for long-term preservation as well as give impulses on requirements for long-term preservation of ESS data back to the ESS community and research data management (RDM) education. The IG also aims to strengthen the national and international interconnectedness regarding long-term preservation of ESS data. 

The IG holds regular virtual meetings every second Friday every second (uneven) month at 8:30. A shared platform for agenda, meeting minutes, documents and outcomes as well as a [mailing list](https://mailman.zih.tu-dresden.de/groups/listinfo/nfdi4earth-ig-lta) for communication is available via TU Dresden. For further information and participation in the IG LTA visit [our website](https://www.nfdi4earth.de/2participate/get-involved-by-interest-groups/ig-lta).

