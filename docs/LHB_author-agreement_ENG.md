---
name: Author agreement
description: Author agreement for contributions to the NFDI4Earth Living Handbook

author: 
  - name: Thomas Rose
    orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: ENG

about:
  - internal nfdi4earth
  - data policies and compliance



isPartOf: LHB_ENG.md

additionalType: user_guide

subjectArea: 
  - unesco:concept326

keywords:
  - Living Handbook 
  - NFDI4Earth

audience: 
  - general public

version: 1.0

license: CC-BY-4.0

---

# Author Agreement

I hereby agree that the work submitted will be published under the [CC-BY 4.0 International license](https://creativecommons.org/licenses/by/4.0/) (<https://creativecommons.org/licenses/by/4.0/>) in all its original parts in the NFDI4Earth Living Handbook operated by Dresden University of Technology and Goethe-University Frankfurt. Before publication of the work, the latter may arrange for an external check of the work. I retain the full copyright of my contributions.

I warrant that the work is original and I am the author of the work. 

To the extent the work incorporates text passages, figures, data or other material from other authors or from own work already published elsewhere, I confirm that all such re-used material is published under permissive licences, that grant the right for such re-use in a work to be published under CC-BY 4.0 International license. In addition, I declare that the original material is incorporated in accordance with the rules outset in these licences (check for licence compatibility can be done, e.g., with the [JLA Compatibility Checker](https://joinup.ec.europa.eu/collection/eupl/solution/joinup-licensing-assistant/jla-compatibility-checker): <https://joinup.ec.europa.eu/collection/eupl/solution/joinup-licensing-assistant/jla-compatibility-checker>. If unsure, please contact the NFDI4Earth Living Handbook Editorial Board via nfdi4earth-livinghandbook@tu-dresden.de). 

I hereby declare that the present work originates from the author(s) indicated and no other sources and resources were used, except for the ones that are expressly indicated. All text passages and ideas quoted or borrowed from other sources of any kind have been clearly identified as such. 

If the work or any part therein was produced through an AI-based or AI-assisted technology, such as ChatGPT, a statement is included at the end of the work that identifies what was produced with which technology/product, and stating that all content supplied by AI-based or AI-assisted technology was checked and verified by the submitting author(s).

I am aware of the fact that publication of the work in the NFDI4Earth Living Handbook operated by Dresden University of Technology and Goethe-University Frankfurt does not in any way affect my legal responsibility as author of the work. 

I acknowledge this agreement, also on behalf of any co-authors, by subscribing to and confirming my membership to the mailing list nfdi4earth-lhb-contributors@groups.tu-dresden.de. I am aware that I may unsubscribe from this list by myself at any time and that I will not be able to make any new contributions to the NFDI4Earth Living Handbook if I do so. Further, I acknowledge that unsubscribing from the mailing list does not invalidate the binding effect of this agreement unless I retract my contributions. I can retract all my contributions to the NFDI4Earth Living Handbook by contacting the Editorial Board via nfdi4earth-livinghandbook@tu-dresden.de. 
