---
name: How to Apply for High Performance Computing resources within NFDI4Earth

description: The article describes the requirements and the application process on the application for High-Performance Computing (HPC) resources within NFDI4Earth. 

author: 
  - name: Ralf Klammer
    orcidId: https://orcid.org/0000-0003-4816-6440

inLanguage: ENG


about:
  - internal nfdi4earth
  - tools and techniques
  - resources
  - data service


isPartOf: 
  - Collection_ProcessAnalyse.md

additionalType: article

subjectArea: 
  - dfgfo:34
  - unesco:concept7387
  - unesco:mt2.35
  - unesco:concept81
  - unesco:concept8075
  - unesco:concept17017
  - unesco:concept6081
  - unesco:concept157

  
keywords:
  - High performance computing
  - HPC
  - processing

version: 0.1

license: CC-BY-4.0

---

# How to apply for a HPC project

The Center for Information Services and High-Performance Computing (ZIH) at the Technical University of Dresden is an NFDI4Earth provider for technical infrastructure and qualified support in collaborative services and virtual research environments, e.g., high-performance computing. The TU Dresden resources are available to active NFDI4Earth members and participants from funded measures such as pilot projects and incubators. 
Here, we provide information about the application process and available options.

## General Information

The general HPC resource is available for highly parallel simulations while the expansion “High-Performance Computing - Data Analytics” (HPC-DA) is available for projects in the field of data analytics (big data and machine learning).

The project duration is one year and can be extended.

The use of the resources should be planned to be used evenly over the entire duration of the project. Furthermore, single “jobs” can never exceed 7 days.

The possibility of so-called “Trial Projects” deserves special mention. Trial projects run through a simplified application process and only need an abstract instead of an extensive project description. They can use up to 3500 CPU hours per month respectively 250 GPU hours per month.

To use HPC resources within courses you can find separate admission requirements.

## Requirements

1. The applicant institution must:
    - belong to a publicly funded institution
    - represent an NFDI4Earth partner
2. The applicant must agree with the terms of use for the HPC systems of the ZIH.
3. The relationship to the TU Dresden, as a partner institution of the NFDI4Earth, must be clarified within the abstract of the project application. We can provide a standard text for this.

## Application Process

1. Read the guidelines with respect to requirements and terms of use
2. Contact our [NFDI4Earth Helpdesk](mailto:helpdesk@nfdi4earth.de) and send the following information
    - Applicant: name, institution, email,
    - Project: title, period, project manager, and used software
    - Short project description; full or abstract (“trial project”)
    - Required resources: machines, CPU time, memory
3. We will document the project and its current status within the NFDI4Earth

## Recommendations

We kindly ask you to refer to the

1. support within the application process by representatives of the NFDI4Earth consortium
2. use of ZIH’s HPC resources within the acknowledgment of derived publications and presentations.

## Links

- [ZIH Terms of Use](https://doc.zih.tu-dresden.de/application/terms_of_use/)
- [HPC courses](https://tu-dresden.de/zih/hochleistungsrechnen/zugang/hpclehre)

<!--

If you contribute to the article, please don't forget to add you as author and to sign the author agreeement (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines_ENG/#author-agreement-and-licences). 

-->
