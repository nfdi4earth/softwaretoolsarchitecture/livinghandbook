<!-- 

created in shell with command "Path to pandoc" -f markdown -t commonmark-yaml_metadata_block --standalone -o TBEP_pandoc_commonmark.md TBEP_pandoc_raw.md

I did not found a way yet to preserve the YAML header during conversion
-->

# I am looking for data

## Resources for finding available ESS data

Access to high-quality data is crucial for research. Therefore, we offer
a wide range of resources and services to facilitate the search for
suitable data. Currently, it is not yet possible to find data directly
through our portal, but we are working on it! However, our guided search
will help you to find the repository or archive that might have the data
you are looking for. Are you already familiar with standards, licences
and repositories? Then start with the complete list of available data
sources.

A number of questions arise around data search and use. What is a
repository? Who owns the data and how are you actually allowed to use
the data? How is the quality of data assessed or evaluated? There are
many data formats in earth system sciences. Each discipline may have its
own standards and requirements. Maybe you want to use the data in the
cloud? There are various tools that support this.

## Read the articles and information we recommend on these and other topics.

<div class="cols" style="display: flex; background-color: #d3d3d3;">

<div class="column">

[![Who owns the
data?](img/who_owns_the_data.png "Who owns the data? Data ownership and licenses")](WhoOwnsTheData_ENG.md)

</div>

<div class="column">

[![What are repositories (in
ESS)?](img/what_are_repositories.png "What are repositories (in ESS)?")](WhatAreRepositories_ENG.md)

</div>

<div class="column">

<!--Unclear why the following links direct the reader to _under_construction.md. There is no information given about the respective topics.-->

[![Data quality in Earth System
Science?](img/data_quality_in_ess.png "Data quality in Earth System Science")](_under_construction.md)

</div>

<div class="column">

[![Data formats in Earth System
Science](img/data_formats_in_ess.png "Data formats in Earth System Science")](_under_construction.md)

</div>

<div class="column">

[![Tools for cloud
solutions](img/tools_for_cloud_solutions.png "Tools for cloud solutions")](_under_construction.md)

</div>

</div>
