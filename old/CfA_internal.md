---
title: Call for Articles
summary: Internal Call for Articles for the Living Handbook
authors: 
  - http://orcid.org/0000-0002-8186-3566

language: en

target_group: 
  - researchers
  - infrastructure providers

collection: 
  - Living Handbook
  - NFDI4Earth

updated: 2023-04-04

---

# Call for Articles

The NFDI4Earth Living Handbook provides (i) an encyclopaedia-style documentation for all aspects related to the NFDI4Earth, its services and outcomes in a human readable form, and (ii) aims to structure and harmonise all information related to data management and data science approaches in the Earth System Sciences in a community-driven effort. 

To kick-off this endeavour, the Living Handbook requires a particular attractive set of initial articles about the NFDI4Earth and its activities, as well as topics related to data search and data publication (incl. repositories in Earth System Sciences). A wide variety of content, article types and authors are further necessary to streamline the editorial workflow, implement and test the necessary functionalities, and optimise the display of its content. 

We invite all members of the NFDI4Earth consortium to submit articles (in English or German) to the Living Handbook **until 31st May 2023**. Articles covering all topics related to the NFDI4Earth and services and methods to improve Data Science approaches in the Earth System Sciences are welcome, particularly those covering topics related to the NFDI4Earth,  data search and data publications (incl. repositories). They can be supported by all types of content, e.g., figures, interactive graphs, or Jupyter Notebooks. Details on the submission process and guidance for authors are provided in the [editorial workflow](https://nfdi4earth.pages.rwth-aachen.de/softwaretoolsarchitecture/livinghandbook/#LHB_editorial-workflow/) and [author guidelines](https://nfdi4earth.pages.rwth-aachen.de/softwaretoolsarchitecture/livinghandbook/#LHB_author-guidelines/), respectively. 

Please enter the topic(s) you intent to write about [in this list](https://sharepoint.tu-dresden.de/projects/nfdi4earth/TA_Synergies/LHB%20Group/LHB-Articles.xlsx?d=w012d4acb147b4656b0e44b399b1a3d4d) to help us avoid double submissions. 

For any questions or assistance, please contact [Thomas Rose](mailto:t.rose@em.uni-frankfurt.de). 

We are looking forward reading your articles! 
